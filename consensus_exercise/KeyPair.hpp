#ifndef KEYPAIR_H
#define KEYPAIR_H
#include "convert.hpp"
#include <cstdint>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstddef>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <cryptopp/sha3.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>

using namespace CryptoPP;
using namespace std;

extern "C"{
     #include "Signatures/api.h"
    }

class KeyPair{
    private:
// The public key of this keypair.
        char publicKey_[PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_PUBLICKEYBYTES];
// The private key of this keypair.
        char privateKey_[PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_SECRETKEYBYTES];
// The password to access the private key, setting admin as a default.
        std::string passwd_ = "admin";
    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the keypair.
*
* Arguments:   None
*
* No Return value
**************************************************/
        KeyPair(){
                publicKey_;
                privateKey_;
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the keypair.
*
* Arguments:   -string publicKey:the public key
*               of the keypair
*              -string privateKey: the private key
*               of the keypair
*              -string passwd: the password to later
*               access the private key
*
* No Return value
**************************************************/
        KeyPair(std::string publicKey, std::string privateKey, std::string passwd){
                std::string publickey = hexToASCII(publicKey);
                std::string privatekey = hexToASCII(privateKey);
                for (int i=0; i < publickey.size(); i++){
                        publicKey_[i] = publickey[i];
                }
                for (int i=0; i < privatekey.size(); i++){
                        privateKey_[i] = privatekey[i];
                }
                passwd_ = passwd;
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the keypair.
*
* Arguments:   -string publicKey:the public key
*               of the keypair
*              -string privateKey: the private key
*               of the keypair
*
* No Return value
**************************************************/
        KeyPair(std::string publicKey, std::string privateKey){
                std::string publickey = hexToASCII(publicKey);
                std::string privatekey = hexToASCII(privateKey);
                for (int i=0; i < publickey.size(); i++){
                        publicKey_[i] = publickey[i];
                }
                for (int i=0; i < privatekey.size(); i++){
                        privateKey_[i] = privatekey[i];
                }
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the keypair.
*
* Arguments:   -string passwd:the chosen password to
*               to access the private key
*
* No Return value
**************************************************/
        KeyPair(std::string passwd)
        : passwd_(passwd)
        {}; 

/*************************************************
* Name:        getPublicKeyAddress
*
* Description: Get the address where the public key
*              is saved.
*
* Arguments:   None
*
* Returns &publicKey_[0]
**************************************************/
        inline char* getPublicKeyAddress()  {return &publicKey_[0];}

/*************************************************
* Name:        getSecretKeyAddress
*
* Description: Get the address where the private key
*              is saved.
*
* Arguments:   -string passwd: The password in order
*               to access the private key
*
* Returns &privateKey_[0] (success)
**************************************************/
        inline char* getSecretKeyAddress(string passwd)  {
                if (checkPasswd(passwd) == true){
                        return &privateKey_[0];
                }
                return NULL;
        }

/*************************************************
* Name:        getPublicKey
*
* Description: Get the public key in hex string form.
*
* Arguments:   None
*
* Returns publickey
**************************************************/
        inline std::string getPublicKey() {
                std::string publicKey(publicKey_, PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_PUBLICKEYBYTES);
                std::string publickey = ASCIIToHex(publicKey);
                
                return publickey;
        }

/*************************************************
* Name:        getHashedPasswd
*
* Description: Get a hash of the password to compare
*              the given password to the actual password.
*
* Arguments:   None
*
* Returns hashedPasswd
**************************************************/
        inline std::string getHashedPasswd(){
                SHA3_256 hash;
                std::string hashedPasswd;
                hash.Update((const CryptoPP::byte*)passwd_.data(),passwd_.size());
                hashedPasswd.resize(hash.DigestSize());
                hash.Final((CryptoPP::byte*) &hashedPasswd[0]);
                return hashedPasswd;
        };      
        
/*************************************************
* Name:        generateKeyPair
*
* Description: Generates a public and a private key.
*
* Arguments:   -string passwd: the passwd needed to
*               to access the private key
*
* No Return value
**************************************************/
        inline void generateKeyPair(std::string passwd){
               char* pK = getPublicKeyAddress();
                uint8_t* pk = (uint8_t*) pK;
                if (checkPasswd(passwd) == true){
                        uint8_t* sk = (uint8_t*) getSecretKeyAddress(passwd);
                        PQCLEAN_DILITHIUM5AES_CLEAN_crypto_sign_keypair(pk, sk);
                } 
        }

/*************************************************
* Name:        createSignature
*
* Description: Compute the signature.
*
* Arguments:   -string Message: the message on
*               which the signature is based
*              -string passwd: the passwd needed to
*               to access the private key
*
* Returns outputSig
**************************************************/
        inline std::string createSignature(std::string Message, std::string passwd){
                char signature[PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_BYTES];
                uint8_t* sig = (uint8_t*) &signature[0];
                size_t sigLength = PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_BYTES;
                size_t* siglen = &sigLength;
                std::string message = hexToASCII(Message);
                uint8_t* m = (uint8_t*) message.data();
                size_t mlen = message.size();
                if (checkPasswd(passwd) == true){
                        const uint8_t *sk = (uint8_t*) getSecretKeyAddress(passwd);
                        PQCLEAN_DILITHIUM5AES_CLEAN_crypto_sign_signature(sig, siglen,m,mlen,sk);
                }   
                string outputSig(signature, PQCLEAN_DILITHIUM5AES_CLEAN_CRYPTO_BYTES);
                std::string outputsig = ASCIIToHex(outputSig);
                return outputsig;
        }

/*************************************************
* Name:        checkPasswd
*
* Description: Checks whether the given password is
*              equal to the password to access the 
*              secret key.
*
* Arguments:   - string passwd: the password that needs 
*                to be verified
*
* Returns true if the pasword is correct, else returns false
**************************************************/
        inline bool checkPasswd(std::string passwd){
                SHA3_256 hash;
                std::string hashedPW;
                hash.Update((const CryptoPP::byte*)passwd.data(),passwd.size());
                hashedPW.resize(hash.DigestSize());
                hash.Final((CryptoPP::byte*) &hashedPW[0]);
                if (hashedPW == KeyPair::getHashedPasswd()){
                        return true;
                };
                return false;   
        }
};

/*************************************************
* Name:        verifySignature
*
* Description: Verifies the signature.
*
* Arguments:   -string signatureToVerify: the signature we want to verify
*              -string Message: the message on which the signature
*               was supposedly set
*              -string publicKey: the public key of the
*               person who supposedly signed the message
*
* Returns true if the signature on the message is valid,
* else returns false
**************************************************/
inline bool verifySignature(std::string signatureToVerify,std::string Message, std::string publickey){
        std::string publicKey = hexToASCII(publickey);
        std::string sigA = hexToASCII(signatureToVerify);
        uint8_t* sig = (uint8_t*) sigA.data();
        size_t siglen = sigA.size();
        std::string message = hexToASCII(Message);
        uint8_t* m = (uint8_t*) message.data();
        size_t mlen = message.size();
        uint8_t *pk = (uint8_t*) publicKey.data();
        if (PQCLEAN_DILITHIUM5AES_CLEAN_crypto_sign_verify(sig, siglen,m,mlen,pk)== -1){
                return false;
        }
        return true;
}



#endif