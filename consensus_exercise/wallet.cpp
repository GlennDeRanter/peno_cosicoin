#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

#include <unistd.h>
#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <cmath>
#include <vector>
#include <map>
#include <toml.hpp>
#include "Transaction.hpp"
#include "wallet.hpp"
#include "IO.hpp"
#include "KeyPair.hpp"
#include <time.h> 
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using consensus::Consensus;
using consensus::EpochInit;
using consensus::SendTx;
using consensus::Ack;
using consensus::WalletIdentification;
using consensus::InputResponse;

/*************************************************
* Name:        sendTransaction
*
* Description: Function to let the wallet send a transaction.
*
* Arguments:   -Transaction transaction: the transaction we want
*               the wallet to send
*
* Returns a boolean that indicates whether the wallet was able to
* send the transaction, and gives the id of the validator to which the
* transaction was sent if it was succesful
**************************************************/
    std::pair<bool,std::string> Wallet::sendTransaction(Transaction& transaction) {
        // Sending the transaction to all validators
        SendTx init;

        // Setting the sender signature and the transaction id
        init.set_wallet_id(getWalletID());
        init.set_sender_sig(transaction.getSenderSig());
        init.set_tx_id(transaction.IdToString());
        for (auto& input: transaction.getInputs()) {
            consensus::SendTx::Input* in = init.add_inputs();
            in->set_previous_tx_id(input.IDToString());
            in->set_output_index(input.getOutputIndex());
        }

        // All outputs from the transaction
        for (auto& output: transaction.getOutputs()) {
            consensus::SendTx::Output* out = init.add_outputs();
            out->set_value(output.getValue());
            out->set_receiver_pk(output.getReceiverPk());
        } 
        // The actual  RPC to all validators.
        int numberOfReceivers = 0;
        for (auto& validator: getValidatorChannels()) {
            Ack Acknowledge; 
            ClientContext context;
            Status status = validator->CommitTransaction(&context, init, &Acknowledge);
            if (status.ok()) {
                numberOfReceivers += 1;
            }
        }

        if (numberOfReceivers == 0) {
            return std::pair<bool,std::string>(false, "[WALLET " + getWalletID() + "]: Failed to send the transaction '" + transaction.IdToString() + "' to any of the validators.");
        } else {
            return std::pair<bool,std::string>(true, "[WALLET " + getWalletID() + "]: Succeeded to send the transaction '" + transaction.IdToString() + "' to " + std::to_string(numberOfReceivers) + " validators.");
        }
    }

/*************************************************
* Name:        synchronize
*
* Description: Function to synchronize the wallet 
*              with the validators (updates UTXO 
*              and balance).
*
* Arguments:   None
*
* Returns a boolean that indicates whether the wallet was able to
* update his balance, returns also a reason why it failed to synchronize
* or the new balance.
**************************************************/
    std::pair<bool,std::string> Wallet::synchronize() {
        // Request the current balance
        WalletIdentification request;
        // Setting the wallet id and the public key
        request.set_wallet_id(getWalletID());
        request.set_public_key(getKeyPair().getPublicKey());
        // Request the balance to a random validator
        unsigned int randNum = rand()%(getValidatorChannels().size());
        // The actual RPC command
        InputResponse response;
        ClientContext context;
        Status status = getValidatorChannels()[randNum]->Synchronize(&context, request, &response);
        if (status.ok()) {
            int validatorId = response.validator_id();
            if (response.valid()) {
                if (response.record_size() != 1) {
                    return std::pair<bool,std::string>(false, "[WALLET " + getWalletID() + "]: Updating the balance failed, since validator " + std::to_string(validatorId) + " sent more than one UTXO, the old balance is: " + std::to_string(getBalance()) + ".");
                }
                getUTXO().clear();
                const consensus::InputResponse::Record& r = response.record(0);
                for (int i = 0; i < r.previous_tx_id_size(); i++) {
                    std::string previousTxId = r.previous_tx_id(i);
                    unsigned int outputIndex = r.output_index(i);
                    unsigned int value = r.number_of_coins(i);
                    previousTransaction pt(previousTxId, outputIndex);
                    utxo_.insert(std::pair<previousTransaction, unsigned int>(pt, value));
                }
            } 
            else {
                balance_ = 0;
                return std::pair<bool,std::string>(true, "[WALLET " + getWalletID() + "]: Updating the balance succeeded, but validator " + std::to_string(validatorId) + " did not find the public key in the UTXO, the new balance is: " + std::to_string(getBalance()) + ".");
            }
        } 
        else {
            balance_ = 0;
            return std::pair<bool,std::string>(false, "[WALLET " + getWalletID() + "]: Updating the balance failed, since validator " + std::to_string(randNum) + " did not respond, the old balance is: " + std::to_string(getBalance()) + ".");
        }
        updateBalance();
        return std::pair<bool,std::string>(true, "[WALLET " + getWalletID() + "]: Updating the balance succeeded, the new balance is: " + std::to_string(getBalance()) + ".");
    }

/*************************************************
* Name:        checkTransaction
*
* Description: Function that checks a transaction
*              given the receivers and corresponding
*              values.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               a list of all the receivers, containing for each receiver
*               their public key and the amount we wish to transfer to them
*
* Returns a boolean that indicates whether the wallet was able to
* validate the transaction and if not returns a reason why not
**************************************************/
    std::pair<bool,std::string> Wallet::checkTransaction(std::vector<std::pair<std::string, unsigned int>>& receivers) {
        synchronize();
        unsigned int intermediateAmount = 0;
        // Checking overspending
        for (const auto& receiver: receivers) { 
            unsigned int value = std::get<unsigned int>(receiver);
            intermediateAmount += value; 
        }
        if (intermediateAmount > getBalance()) {
            return std::pair<bool,std::string>(false, "The transaction spends " + std::to_string(intermediateAmount-getBalance()) + " more coins than the available number of coins: " + std::to_string(getBalance()));
        }
        return std::pair<bool,std::string>(true, "");
    }

/*************************************************
* Name:        makeTransaction
*
* Description: Function that makes a transaction
*              given the receivers and corresponding
*              values.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               a list of all the receivers, containing for each receiver
*               their public key and the amount we wish to transfer to them
*              -std::string transactionId: the id we want to give to the
*               transaction
*
* Returns a boolean that indicates whether the wallet was able to
* sign the transaction and if not the reason why not and also the built
* transaction
**************************************************/
    std::pair<std::pair<bool,std::string>,Transaction> Wallet::makeTransaction(std::vector<std::pair<std::string, unsigned int>>& receivers, std::string& transactionId) {
        std::pair<bool,std::string> result1 = checkTransaction(receivers);
        if (std::get<bool>(result1)) {
            auto [outputs, spendAmount] = createOutputs(receivers);
            // Inputs
            unsigned int returnedCoins;
            unsigned int totalInputValue = 0;
            std::vector<Input> inputs = {};
            for (const auto& input: getUTXO()) {
                std::string pt = std::get<std::string>(input.first);
                std::vector<unsigned char> txId(pt.begin(), pt.end());
                unsigned int outputOffset = std::get<unsigned int>(input.first);

                Input in(txId, outputOffset);
                inputs.push_back(in);
                getUTXO().erase(std::pair<std::string, unsigned int>(pt, outputOffset));
                unsigned int totalInputValue = input.second;
                if (totalInputValue >= spendAmount) {
                    returnedCoins = totalInputValue - spendAmount;
                    break;
                }
            }
            unsigned int txId;
            std:string transactionId;
            if (transactionId == "") {
                srand(time(NULL));
                txId = rand() + stoi(getWalletID());
                transactionId = std::to_string(txId);
            } 
            std::vector<unsigned char> id(transactionId.begin(), transactionId.end());
            auto [result2, transaction] = completeTransaction(returnedCoins, inputs, outputs, id);
            if (std::get<bool>(result2)) {
                return std::pair<std::pair<bool,std::string>,Transaction>(std::pair(true, ""), transaction);
            } else {
                return std::pair<std::pair<bool,std::string>,Transaction>(std::pair(false, std::get<std::string>(result2)), transaction);
            }
        } else {
            Transaction transaction({}, {}, "", {});
            return std::pair<std::pair<bool,std::string>,Transaction>(std::pair(false, std::get<std::string>(result1)), transaction);;
        }
    }


/*************************************************
* Name:        signTransaction
*
* Description: Function that lets the wallet sign a
*              transaction.
*
* Arguments:   -Transaction transaction: the transaction
*               the wallet needs to sign
*
* Returns a boolean saying whether the transaction was signed
* correctly and returns a string saying you failed to give the
* correct password if not
**************************************************/
    std::pair<bool,std::string> Wallet::signTransaction(Transaction& transaction) {
        std::pair<bool,std::string> result = passwordPrompt();
        if (std::get<bool>(result)) {
            // Correct password
            std::string password = std::get<std::string>(result);
            std::string message = transaction.hashTransaction();
            std::string signedMessage = getKeyPair().createSignature(message, password);
            // Set the signature in the transaction
            transaction.setSenderSig(signedMessage);
        
            return std::pair<bool,std::string>(true, "Transaction with transaction id " + transaction.IdToString() + " is correcly signed.");
        } else {
            // Incorrect password (3 attempts)
            return std::pair<bool,std::string>(false, "Failed 3 password attempts!");
        }
    }


/*************************************************
* Name:        passwordPrompt
*
* Description: Function that asks for the password
*              and gives you three guesses to enter
*              the correct password.
*
* Arguments:   None
*
* Returns a boolean saying whether the given password was
* correct and if not the amount of attempts remaining.
**************************************************/
    std::pair<bool,std::string> Wallet::passwordPrompt() {
        int attempts = 0;
        std::string password;
        // Ask for the password
        std::cout << "" << std::endl;
        std::cout << "---------PASSWORD---------" << std::endl;
        while (attempts < 3) {
            std::string password = "";
            attempts += 1;
            while (password == "") {
                std::cout << "Password: ";
                getline(std::cin, password);
            }
            // Check the password
            if (getKeyPair().checkPasswd(password)) {
                std::cout << "Correct password." << std::endl;
                return std::pair<bool,std::string>(true, password);
            } else {
                if (3-attempts == 1) {
                    std::cout << "Incorrect password, 1 remaining attempt!" << std::endl;
                } else {
                    std::cout << "Incorrect password, " << (3-attempts) << " remaining attempts!" << std::endl;
                }
            }
        }
        return std::pair<bool,std::string>(false, "");
    }


/*************************************************
* Name:        getKnownWallets
*
* Description: Function to display all past used receivers this wallet
*              knows and ask which known wallet(s) to use as receiver.
*
* Arguments:   None
*
* Returns the indexes of the receivers to which 
*         the wallet wants to send in a vector
**************************************************/
    std::vector<int> Wallet::getKnownWallets() {
            int numberOfWallets = getWallets().size();
            std::vector<int> receivers;
            std::string receiver = "-";
            for (int i=0; i<getWallets().size(); ++i) {
                std::string walletPublicKey = (getWallets()[i]);
                std::cout << (i+1) << ". " << walletPublicKey.substr(0, 100) << std::endl;
            }
            std::cout << "" << std::endl;
            while (receiver == "-" && getWallets().empty() == false) {
                std::cout << "Add receiver on index (leave blank to exit): ";
                getline(std::cin, receiver);
                if (receiver == "") {
                   break;
                }
                if (std::get<bool>(validStringOfIntegers(receiver)) == false) {
                    std::cout << std::get<std::string>(validStringOfIntegers(receiver)) << std::endl;
                } else {
                    if (0 < stoi(receiver) && stoi(receiver) <= numberOfWallets) {
                        // Add the receiver to the known receivers if not already in the known receivers
                        if (std::find(receivers.begin(), receivers.end(), stoi(receiver)) == receivers.end()) {
                            // Not found
                            receivers.push_back(stoi(receiver));
                        } else {
                                std::cout << "Not possible to add the receiver twice!" << std::endl;
                        }                 
                    } else {
                        std::cout << "The index should be in range!" << std::endl;
                    }
                }
                receiver = "-";
            }
            std::cout << "" << std::endl;
            return receivers;
        }


/*************************************************
* Name:        run
*
* Description: Function that actually runs an 
*              interactive wallet.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
    void Wallet::run() {
        if (getWalletID() == "1" || getWalletID() == "2" || getWalletID() == "3") {
            runDefaultWallet();
        // Customized wallet
        } else {
            while (true) {
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::string function = "";
                std::vector<std::pair<std::string, unsigned int>> receivers;

                while (function != "1" && function != "2" && function != "3" && function != "4") {
                    std::cout << "Send transaction/Synchronize/Configuration/Quit (1/2/3/4): ";
                    getline(std::cin, function);
                }
                std::cout << "" << std::endl;
                if (function == "1") {
                    sendTransactionInterfaceCustom(receivers);
                } else if (function == "2") {
                    walletSynchronizationInterface();
                } else if (function == "3") {
                    walletConfigurationInterface();
                } else {
                    return;
                }
            }
        }
    }

/*************************************************
* Name:        walletSynchronizationInterface
*
* Description: Function to do the synchronization
*              function of the wallet interface.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
void Wallet::walletSynchronizationInterface(){
    std::cout << std::get<std::string>(synchronize()) << std::endl;
    std::cout << "------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
}

/*************************************************
* Name:        walletConfigurationInterface
*
* Description: Function to do the configuration
*              function of the wallet interface.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
void Wallet::walletConfigurationInterface(){
    std::cout << "---------WALLET INFORMATION---------" << std::endl;
    std::cout << "Wallet id: " << getWalletID() << std::endl;
    std::cout << "Public key: " << getKeyPair().getPublicKey() << std::endl;
    std::cout << "------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
}

int main(int argc, char const *argv[]) {
  if (argc > 1) {
      std::string id = argv[1];
      std::string password = "";
      // Ask for a new password
      while (password == "") {
          std::cout << "Type in your new password: ";
          getline(std::cin, password);
      }

      if (id == "dev") {
          Wallet dev("0", password, true);
          dev.run();
          return 0;
      } else if (id == "Milan") {
          Wallet Milan("1", password, true);
          Milan.run();
          return 0;
      } else if (id == "Linde") {
          Wallet Linde("2", password, true);
          Linde.run();
          return 0;
      } else if (id == "Glenn") {
          Wallet Glenn("3", password, true);
          Glenn.run();
          return 0;
      } else {
          Wallet newWallet(id, password);
          newWallet.run();
          return 0;
      }
  } else {
    return 0;
  }
}