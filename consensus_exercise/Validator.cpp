#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

#include <iostream>
#include <memory>
#include <string>
#include <list>
#include "toml.hpp"
#include "Block.hpp"
#include "BlockChain.hpp"
#include "convert.hpp"
#include <assert.h>
#include "Validator.hpp"
#include "Transaction.hpp" //milan
#include "IO.hpp" //milan
#include "UTXO.hpp"
#include "KeyPair.hpp"
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <mutex>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using consensus::Ack;
using consensus::Ballot;
using consensus::BallotPayload;
using consensus::VoteBallot;
using consensus::VotePayload;
using consensus::EpochInit;
using consensus::Consensus;
using consensus::SendTx; //milan
using consensus::WalletIdentification; //milan
using consensus::InputResponse; //milan
using consensus::Echo; //milan

        
///////////////////////////////////////////////////////////////////////////////////////////// INTERACTION WITH OTHER VALIDATORS /////////////////////////////////////////////////////////////////////////////////////////////
    
/*************************************************
* Name:        sendVote
*
* Description: Function that does the client side of the rpc 
*              command vote.
*
* Arguments:   -const long int user: id of the validator who wishes to
*               send the vote
*              -const VotePayload payload: the block
*               we want to vote on
*              -const std::string signature: the signature of the 
*               validator on the block
*
* Returns a string that tells us whether or not the vote was successful
* and if not why not
**************************************************/
    std::string Validator::sendVote(const long int user, const VotePayload payload, const std::string signature){
        //construct the ballot with all the necessary information
        VoteBallot vote;
        vote.mutable_payload() -> CopyFrom(payload);
        vote.set_name(user);
        vote.set_signature(signature);
        // create the ack en context for the rpc
        Ack Acknowledge;
        ClientContext context;
        // The actual  RPC.
        Status status = getChannel()->Vote(&context, vote, &Acknowledge);
        // Act upon its status.
        if (status.ok()) {
            return "[VALIDATOR " + to_string(getUserID()) + "] --> The sended vote is acknowledged.";
        } else {
            std::cout <<"vote: " << status.error_code() << ": " << status.error_message()
                        << std::endl;
            return "[VALIDATOR " + to_string(getUserID()) + "] --> RPC failed: no acknowledge received for the sended vote.";
        }
    }

/*************************************************
* Name:        sendProposal
*
* Description: Function that does the client side of the rpc 
*              command propose.
*
* Arguments:   -const long int id: id of the validator who wishes to
*               send the vote
*              -const VotePayload payload: the block
*               we want to vote on
*              -const std::string signature: the signature of the 
*               validator on the block
*
* Returns a string that tells us whether or not the proposal was successful
* and if not why not
**************************************************/
    std::string Validator::sendProposal(const long int id, const BallotPayload payload, const std::string signature) {
        //construct the ballot proposal with the input of this function
        Ballot proposal;
        proposal.mutable_payload() -> CopyFrom(payload);
        proposal.set_name(id);
        proposal.set_signature(signature);
        //create the ack and the context for the client
        Ack Acknowledge;
        ClientContext context;
        // The actual  RPC.
        Status status = getChannel()->Propose(&context, proposal, &Acknowledge);
        // Act upon its status.
        if (status.ok()) {
            return "[VALIDATOR " + to_string(getUserID()) + "] --> The sended proposal is acknowledged.";
        } else {
            std::cout << "propose:" << status.error_code() << ": " << status.error_message()
                        << std::endl;
            return "[VALIDATOR " + to_string(getUserID()) + "] --> RPC failed: no acknowledge received for the sended proposal.";
        }
    }

/*************************************************
* Name:        sendImplicitEcho
*
* Description: Function that does the client side of the rpc 
*              command implicit echo.
*
* Arguments:   -const long int receiverID: the id of the receiving validator
*              -const long int senderID: the id of the sending validator
*              -const std::string echoType: the type of echo we want to do
*               (reaction on either a vote or a proposal)
*              -const BallotPayload payload: the block of which we need to send
*               the echo of
*              -const std::string merkelrootID:The merkelroot of the transactions 
*               in the block
*              -const std::string signature: signature that is in the message (vote or
*               proposal) we want to echo
*
* Returns a string that tells us whether or not the echo was successful
* and if not why not
**************************************************/
    std::string Validator::sendImplicitEcho(const long int receiverID, const long int senderID, const std::string echoType, const BallotPayload payload, const std::string merkelrootID, const std::string signature) {
        //construct the Echo echoMessage with the input of this function
        Echo echoMessage;
        echoMessage.mutable_payload() -> CopyFrom(payload);
        echoMessage.set_receiverid(receiverID);
        echoMessage.set_senderid(senderID);
        echoMessage.set_typeecho(echoType);
        echoMessage.set_merkelrootid(merkelrootID);
        echoMessage.set_signature(signature);
        //create the ack and the context for the client
        Ack Acknowledge;
        ClientContext context;
        // The actual  RPC.
        Status status = getChannel()->ImplicitEcho(&context, echoMessage, &Acknowledge);
        // Act upon its status.
        if (status.ok()) {
            return "[VALIDATOR " + to_string(getUserID()) + "] --> The sended implicit echo is acknowledged.";
        } else {
            std::cout << "echo: " << echoType << status.error_code() << ": " << status.error_message()
                        << std::endl;
            return "[VALIDATOR " + to_string(getUserID()) + "] --> RPC failed: no acknowledge received for the sended implicit echo.";
        }
    }

/*************************************************
* Name:        Propose
*
* Description: Function that does the server side of the Propose rpc command.
*              Here we program how a validator reacts when it receives a proposal.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const Ballot* proposal: all the information needed for the 
*               proposal
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the proposal was proposed successful
**************************************************/
    Status Validator::Propose(ServerContext* context, const Ballot* proposal, Ack* acknowledge) {
        //Save the payload of the proposal locally
        BallotPayload payload = proposal -> payload(); 
        std::vector<Transaction> transactionsProposedBlock = moveTransactionsFromPayload(payload);   
        //print received proposal
        printReceivedProposal(proposal, transactionsProposedBlock);
        std::unique_lock<std::mutex> lock1(m,std::defer_lock);
        lock1.lock();
        Block receivedBlock = Block(payload.parent_hash(), payload.epoch_number(), transactionsProposedBlock);
        setBlockProposal(receivedBlock);
        Block proposalBlocks = getBlockProposal();  
        proposalBlocks.addSignatureToBlock(findPublicKey(proposal -> name()), proposal -> signature(), getNmbOfReceivingValidators());
        setBlockProposal(proposalBlocks);
        addBlockToProposedBlock(proposalBlocks);
        //Create variables to locate the biggest chain the Validator has seen and how long it is
        int locationBiggestChain;
        std::pair<BlockChain, std::vector<int>> result = retrieveLocationLongestBlockChains(payload);
        BlockChain chainProposal = std::get<BlockChain>(result);
        std::vector<int> chainsOfSameLength = std::get<std::vector<int>>(result);
        setReceivedProposal(true);
        //runs the while loop until every longest chain is checked if it is the proposedchain
        while (!chainsOfSameLength.empty()){
            locationBiggestChain = chainsOfSameLength.back();
            verifyProposedChain(locationBiggestChain, chainProposal);
            //Checks whether the proposed chain is  the longest chain seen and adds the signature of the leader  
            //TODO change available public keys
            chainsOfSameLength.pop_back();
        }
        lock1.unlock();
        for (auto& client: getPoolOfReceivingValidators()){
            if (client.getUserID() != proposal -> name()){
                client.sendImplicitEcho(proposal -> name(), getUserID() , "proposal", payload, getBlockProposal().getHeader().getMerkelRoot(), proposal -> signature());
            }
        }      
        return Status::OK;
    }

/*************************************************
* Name:        Vote
*
* Description: Function that does the server side of the vote rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const VoteBallot* vote: all the information needed for the 
*               vote
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the vote was sent successful
**************************************************/
    Status Validator::Vote(ServerContext* context, const VoteBallot* vote, Ack* acknowledge) {
        std::cout << "[VALIDATOR " << getUserID() << "] --> Received a vote from validator " << vote->name()  << " for epoch " << (vote -> payload()).epoch_number() << "." << std::endl;
        //A while so the vote is only received after the Validator has also received a proposal
        //otherwise it would see the wrong proposedblock and chain
        while((!getReceivedProposal()) && (!getIsLeader())) {
            struct timespec timeout;
            timeout.tv_sec = 1;
            timeout.tv_nsec = 1;
            nanosleep(&timeout, NULL);            
        }
        //save the payload of the vote locally
        VotePayload payload = vote -> payload();
        
        std::unique_lock<std::mutex> lock1(m, std::defer_lock);
        lock1.lock();
        Block votedBlock = addReceivedVoteSignature(payload.merkelrootid(), vote -> name(), vote -> signature());
        //check whether the voted block is the same as the proposed block and if it is then we add locally the signature of the recieved voter on the block
        lock1.unlock();
        //we check also if the block is notarised and if it can be notarised than we add it to the proposed chain
        BallotPayload echoPayload;
        for (auto& client: getPoolOfReceivingValidators()){
            if (client.getUserID() != vote -> name()){
                client.sendImplicitEcho(vote -> name(), getUserID() , "vote", echoPayload, votedBlock.getHeader().getMerkelRoot(), vote -> signature());
            } 
        }
        return Status::OK;
    }

/*************************************************
* Name:        ImplicitEcho
*
* Description: Function that does the server side of the implicit echo rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const Echo* message: all the information needed for the 
*               implicit echo (either for the echo of a proposal or a vote)
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the echo was sent successful
**************************************************/
    Status Validator::ImplicitEcho(ServerContext* context, const Echo* message, Ack* acknowledge) {
        std::cout << "[VALIDATOR " << getUserID() << "] --> Received an echo from validator " << message -> senderid() << " mentioning a received " << message -> typeecho() << " from validator " << message -> receiverid() << "." << std::endl;
        if ((message -> typeecho() == "vote") && (!getAlreadyAdded())) {
            std::unique_lock<std::mutex> lock1(m, std::defer_lock);
            lock1.lock();
            addReceivedVoteSignature(message ->merkelrootid(), message->receiverid(), message -> signature());
            lock1.unlock();
        }
        if ((message -> typeecho() == "proposal") && (!getAlreadyAdded())){
            std::unique_lock<std::mutex> lock1(m, std::defer_lock);
            lock1.lock();
            bool alreadyAddedToProposedBlocks = false;
            for(int i = 0; i < getProposedBlocks().size(); ++i){
                Block proposeBlock = getProposedBlocks()[i];
                std::string merkelroot = message -> merkelrootid();
                if (merkelroot == proposeBlock.getHeader().getMerkelRoot()){
                    eraseBlockOutProposedBlocks(i);
                    proposeBlock.addSignatureToBlock(findPublicKey(message -> receiverid()), message-> signature(), getNmbOfReceivingValidators());
                    alreadyAddedToProposedBlocks = true;
                    addBlockToProposedBlock(proposeBlock);
                }
            }
            if (!alreadyAddedToProposedBlocks){
                BallotPayload payload = message -> payload();
                std::vector<Transaction> transactionsProposedBlock = moveTransactionsFromPayload(payload);                
                Block blockPropose = Block(payload.parent_hash(), payload.epoch_number(), transactionsProposedBlock);
                blockPropose.addSignatureToBlock(findPublicKey(message -> receiverid()), message -> signature(), getNmbOfReceivingValidators());
                addBlockToProposedBlock(blockPropose);
                for (long unsigned int i =0; i < getBlockChainVector().size(); ++i){
                    BlockChain chain = getBlockChainVector()[i];
                    std::string hashChain = chain.hashPrefix();
                    //if the hash of the latest block equals the parenthash from the proposal than we know this is the chain the leader wants to extend
                    if (hashChain == payload.parent_hash()){
                        addProposedChain(chain);
                    }
                }
            }
        } 
        return Status::OK;
    }


///////////////////////////////////////////////////////////////////////////////////////////// INTERACTION WITH THE HEARTBEAT /////////////////////////////////////////////////////////////////////////////////////////////
    
/*************************************************
* Name:        InitEpoch
*
* Description: Function that does the reaction of the validator to
*              the message of the heartbeat server that a new epoch
*              is going to start.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const EpochInit* epoch: a message containing the epoch number
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the initialisation of the epoch was sent successful
**************************************************/
    Status Validator::InitEpoch(ServerContext* context, const EpochInit* epoch, Ack* acknowledge) {
        std::cout << "" << std::endl; 
        std::cout << "" << std::endl;
        if (epoch->epoch_number()-1 != getEpochNumber()){
            std::cout << "[VALIDATOR " << getUserID() << "] --> WARNING, epoch wasn't updated for " << epoch->epoch_number() - epoch_ -1 << "epochs!" << std::endl; 
        }
        //reset all the booleans at the beginning of each epoch
        setReceivedProposal(false);
        setAlreadyAdded(false);
        setVoteCanBeSend(false);
        setIsLeader(false);
        setEpochNumber(epoch->epoch_number());
        cleanProposedBlocks();
        cleanProposedChains();
        setAddedBlockToChainThisEpoch(false);
        std::cout << "[VALIDATOR " << getUserID() << "] --> Epoch " << epoch->epoch_number() << " is initiated." << std::endl;
        return Status::OK;
    }

/*************************************************
* Name:        StartEpoch
*
* Description: Function that makes sure the validator starts the epoch.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const EpochInit* epoch: a message containing the epoch number
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the start of the epoch was sent successful
**************************************************/
    Status Validator::StartEpoch(ServerContext* context, const EpochInit* epoch, Ack* acknowledge) {
        //Find out the new leader for the started epoch.
        setVoteCanBeSend(false);
        updateLeader();
        std::cout << "[VALIDATOR " << getUserID() << "] --> New leader of epoch " << epoch->epoch_number() << " is validator " << getLeaderID() << "." << std::endl;
        if (getUserID() == getLeaderID()){
            std::cout << "[VALIDATOR " << getUserID() << "] --> I am the new leader of epoch " << epoch->epoch_number() << "." << std::endl;
            setIsLeader(true);
        }
        setEpochIsStarted(true);
        std::cout << "[VALIDATOR " << getUserID() << "] --> Epoch " << epoch->epoch_number() << " is started." << std::endl;
        return Status::OK;
    }  


///////////////////////////////////////////////////////////////////////////////////////////// INTERACTION WITH WALLETS /////////////////////////////////////////////////////////////////////////////////////////////
    
/*************************************************
* Name:        CommitTransaction
*
* Description: Function that does the server side of the transaction
*              rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const SendTx* transaction: a message containing the transaction
                the wallet wants to send to the validator
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
    Status Validator::CommitTransaction(ServerContext* context, const SendTx* transaction, Ack* acknowledge) { // milan
        // Reassemble the transaction
        auto [receivedTransaction,tx, walletId, txIdOfTransaction ] = reassembleTransaction(transaction);
        bool valid = false;
        std::vector<int> largestChainPositions = findLargestChainPositions();
        for (int i = 0; i < largestChainPositions.size(); ++i) {
            int index = largestChainPositions[i];
            UTXO utxo = getBlockChainVector()[index].getUTXO();
            // Check if the transaction is valid regarding one UTXO coming from the one of the longest chains
            std::vector<Transaction> payloadToCheck = getPendingTransactions();
            payloadToCheck.push_back(receivedTransaction);
            Block blockToCheck("", 1, payloadToCheck);
            if (std::get<bool>(utxo.verify(blockToCheck))) {
                valid = true;
                goto End1;
            } else {
                std::cout << "DEBUG INFORMATION: Error in received transaction: " << std::get<std::string>(utxo.verify(blockToCheck)) << std::endl;
            }
        }
        End1:
        //Checking if the transaction is already in the pending transactions
        bool duplicate = false;
        for (auto& pendingTransaction: getPendingTransactions()) {
            if (txIdOfTransaction == pendingTransaction.getId()) {
                duplicate = true;
                goto End2;
            }
        }
        End2:
        commitTransactionStatusUpdate(walletId, receivedTransaction, tx, duplicate, valid);
        return Status::OK;
    }

/*************************************************
* Name:        Synchronize
*
* Description: Function that does the server side of the synchronization
*              rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const WalletIdentification* request: a message containing the id
*               and public key of the wallet that wishes to synchronize
*              -InputResponse* response: a message containing the UTXO, whether
*               it is valid and from which validator it comes
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
    Status Validator::Synchronize(ServerContext* context, const WalletIdentification* request, InputResponse* response) {
        std::string walletId = request->wallet_id();
        std::string publicKey = request->public_key();
        std::cout << "[VALIDATOR " << getUserID() << "]: Received a balance request from wallet: " << walletId << std::endl;
        // Search for the longest (notarized) chains seen by the validator
        int largestChainLength = 0;
        std::vector<int> largestChainPositions;
        for (long unsigned int i = 0; i < getBlockChainVector().size(); ++i){
           BlockChain chain = getBlockChainVector()[i];
           if (chain.getBlocks().back().isNotarized()) {
               if (chain.getChainLength() > largestChainLength) {
                   largestChainLength = chain.getChainLength();
                   largestChainPositions.clear();
                   largestChainPositions.push_back(i);
               } else if (chain.getChainLength() == largestChainLength) {
                   largestChainPositions.push_back(i);
               }
           }
        }
        findWalletRecord(walletId, publicKey, response, largestChainPositions);
        return Status::OK;
    }

/*************************************************
* Name:        findWalletRecord
*
* Description: Function that searches for the record of the
*              wallet (public key) in the UTXO of the longest
*              chain(s).
*
* Arguments:   -std::string walletId: the id of the wallet we want to 
*               find the record of
*              -std::string publicKey: the public key of the wallet we want
*               to find the record of
*              -InputResponse* response: a message containing the UTXO, whether
*               it is valid and from which validator it comes
*              -std::vector<int> largestChainPositions: the positions of the largest
*               blockchains in the vector with all the blockchain 
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
    void Validator::findWalletRecord(std::string walletId, std::string publicKey, InputResponse* response, std::vector<int> largestChainPositions){
        response->set_validator_id(getUserID());
        bool valid = false;
        for (int i = 0; i < largestChainPositions.size(); ++i) {
            int index = largestChainPositions[i];
            UTXO utxo = getBlockChainVector()[index].getUTXO();
            // Find the record in the UTXO of the particular chain
            std::pair<bool,std::map<std::pair<std::string,unsigned int>, unsigned int>> result = utxo.getRecord(publicKey);
            // Check if record is found and add in this case
            if (std::get<bool>(result)) { 
                consensus::InputResponse::Record* in = response->add_record();
                for (auto& input: result.second) { 
                    // Add one input
                    valid = true;
                    in->add_previous_tx_id(std::get<std::string>(input.first));
                    in->add_output_index(std::get<unsigned int>(input.first));
                    in->add_number_of_coins(input.second);
                }
            }
        }
        response->set_valid(valid);

        if (valid) {
            std::cout << "[VALIDATOR " << getUserID() << "]: Sent " << response->record_size() << " UTXO(s) to wallet: " << walletId << "." << std::endl;
        } else {
            std::cout << "[VALIDATOR " << getUserID() << "]: Sent no UTXOs to wallet: " << walletId << "." << std::endl;
        }
}

///////////////////////////////////////////////////////////////////////////////////////////// CLIENT SIDE OF A NODE /////////////////////////////////////////////////////////////////////////////////////////////
    
/*************************************************
* Name:        clientActions
*
* Description: Function that runs after the validator has started a server
*              and does all the client side of the validator.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
    void Validator::clientActions() {
        // right now the server never exits, but we could also make it so that we exit the while loop at some point
        while (true){
            if (getEpochIsStarted()) {
                if (getIsLeader()) {
		    setVoteCanBeSend(false);
                    leaderActions()
;
                } 
                else {
                    if (getVoteCanBeSend()) {
                        VotePayload payloadVote;
                        payloadVote.set_epoch_number(getBlockProposal().getEpoch());
                        payloadVote.set_merkelrootid(getBlockProposal().getHeader().getMerkelRoot());
                        payloadVote.set_parent_hash(getBlockProposal().getHashedPrefix());
                        for (auto& client: getPoolOfReceivingValidators()){
                            client.sendVote(userID_, payloadVote, getVoteSignature());
                        }
                        setVoteCanBeSend(false);
                        setEpochIsStarted(false);
                        std::cout << "[VALIDATOR " << getUserID() << "] --> Voted on proposal from validator " << getLeaderID() << " with epoch number " << getEpochNumber() << "." << std::endl;
                    }
                }
            }
            if (getAddProposedBlockToChain()) {
                setAlreadyAdded(true);
                addProposedBlockToBlockChainActions();
            }
    
        }
    }    


///////////////////////////////////////////////////////////////////////////////////////////// SERVER SIDE OF A NODE /////////////////////////////////////////////////////////////////////////////////////////////
    
/*************************************************
* Name:        RunServer
*
* Description: Function that initialises the server side
*              of the validator.
*
* Arguments:   -std::string server_address: the local host
*               address of the validator
*              -std::string networkAddress: the ip address
*               of the validator
*              -Validator &service: the validator to which this
*               server belongs
*
* Returns server
**************************************************/
    std::unique_ptr<Server> RunServer(std::string server_address, std::string networkAddress, Validator &service) {
        grpc::EnableDefaultHealthCheckService(true);
        ServerBuilder builder;
        // Listen on the given address without any authentication mechanism.
        builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
        // Register "service" as the instance through which we'll communicate with
        // clients. In this case it corresponds to an *synchronous* service.
        builder.RegisterService(&service);
        // Finally assemble the server.
        std::unique_ptr<Server> server(builder.BuildAndStart());
        std::cout << "Validator listening on " << networkAddress << "." << std::endl;
        std::cout << "" << std::endl;
        return server;
    }


///////////////////////////////////////////////////////////////////////////////////////////// GENERAL FUNCTIONS /////////////////////////////////////////////////////////////////////////////////////////////
   
/*************************************************
* Name:        updateLeader
*
* Description: Function to find the next leader.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
    void Validator::updateLeader(){
        SHA3_256 hash;
        string hashedLeader;
        std::string newLeaderHash;
        srand(getEpochNumber());
        int beginNumber = getLeaderID() + rand();
        hash.Update((const CryptoPP::byte*)to_string(beginNumber).data(),to_string(beginNumber).size());
        hashedLeader.resize(hash.DigestSize());
        hash.Final((CryptoPP::byte*) &hashedLeader[0]);
        newLeaderHash = ASCIIToDec(hashedLeader).substr(5,6);
        setLeaderID(std::stoll(newLeaderHash) % getNmbOfReceivingValidators() + 1);
        
    }



//this is the main function
int main(int argc, char** argv) {
    std::string networkAddress;
    std::string localhostAddress;
    localhostAddress = argv[1];
    if (argc == 3){
        networkAddress = replaceLocalAddressTonetworkAddress(localhostAddress, argv[2]);
    }else{
        networkAddress = localhostAddress;
    }
    const toml::value data = toml::parse("setup.toml");
    const auto& table = toml::find(data, "nodes");
    std::string userId;
    std::string publicKey;
    std::string privateKey;
    for(const auto& v : table.as_array()){
        const auto nodeAddress = toml::find<std::string>(v, "ip_address");
        if (nodeAddress == networkAddress){
            userId = toml::find<std::string>(v, "id");
            std::string adjustedNodeAddress = replaceValidatorAdressForDirectoryName(networkAddress);
            publicKey = pushFileIntoString(adjustedNodeAddress, "/publicKey", userId);
            privateKey = pushFileIntoString(adjustedNodeAddress, "/privateKey", userId);
            break;
        }
    }
    Validator thisNode(std::stoi(userId), grpc::CreateChannel(
        localhostAddress, grpc::InsecureChannelCredentials()), networkAddress , publicKey,privateKey);
    std::unique_ptr<Server> server = RunServer(localhostAddress, networkAddress, thisNode);
    thisNode.clientActions();
    return 0; 
}
