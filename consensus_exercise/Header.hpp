#ifndef HEADER_H
#define HEADER_H

#include <vector>
#include <string>


class Header{
    private:
// The epoch number in which the block to which this header belongs was created.
        int epoch_;
// The vector with all the signatures of validators that have signed the block.
        std::vector<std::string> validatorSigs_;
// The hash of the last block in the blockchain to which we wish to append this block.
        std::string hashedPrefix_;
// The Merkel root tree containing the transactions belonging to this block.
        std::string merkelRoot_;
// A sommation of all the transactions belonging to this block.
        std::vector<std::vector<unsigned char>> transactionIds_;

    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the transaction and takes no arguments.
*
* Arguments:   None
*
* No Return value
**************************************************/
        Header(){
            epoch_=0;
            hashedPrefix_ = "";
            merkelRoot_ = "";
            validatorSigs_ = {};
            transactionIds_ = {};
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the header for a block.
*
* Arguments:   -const int epoch: the epoch number in which 
*               the block to which this header belongs was created
*              -const std::string hashedPrefix: the hash of the last
*               block in the blockchain we want to add the block to
*              -const vector<string>& validatorSigs: the vector
*               with all the signatures of validators that have signed
*               the block
*              -const std::string senderSig: the signature of the 
*               person who will pay the amount specified in the outputs
*              -const std::vector<unsigned char>& txId: the ID tag of 
*               the transaction 
*
* No Return value
**************************************************/
        Header(const int epoch, const std::vector<std::string>& validatorSigs, const std::string hashedPrefix, const std::string merkelRoot, const std::vector<std::vector<unsigned char>> &txIds)
        :epoch_(epoch)
        ,hashedPrefix_(hashedPrefix)
        ,merkelRoot_(merkelRoot)
        ,validatorSigs_(validatorSigs)
        ,transactionIds_(txIds)
        {}

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old header
               in a new header.
*
* Arguments:   -const Header& header: the header
*               we want to copy
*
* No Return value
**************************************************/
        Header(Header const& header)
        :epoch_(header.epoch_)
        ,hashedPrefix_(header.hashedPrefix_)
        ,merkelRoot_(header.merkelRoot_)
        ,validatorSigs_(header.validatorSigs_)
        ,transactionIds_(header.transactionIds_)
        {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the header is equal
*              to a given header and otherwise 
*              makes sure they are.
*
* Arguments:   -const Header& header: the header
*               we want to match to
*
* Returns *this (success)
**************************************************/
        Header& operator=(Header const& header){
                if(!(*this == header)){
                        for (int i=0; i<header.validatorSigs_.size();++i){
                                if (validatorSigs_.size() <= i){
                                        validatorSigs_.push_back(header.validatorSigs_[i]);
                                } 
                                else{
                                        validatorSigs_[i] = header.validatorSigs_[i];
                                } 
                        }
                        epoch_ = header.epoch_;
                        hashedPrefix_ = header.hashedPrefix_;
                        merkelRoot_ = header.merkelRoot_;
                        transactionIds_ = header.transactionIds_;
                }
                return *this;
        }

/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the header matches a 
*              given header.
*
* Arguments:   Input const& header: headerobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
        bool operator==(Header const& header){
                bool equal = true;
                if (header.validatorSigs_.size() != validatorSigs_.size()){
                        equal = false;
                }
                else {
                        for (int i=0; i < validatorSigs_.size(); ++i){
                                if (validatorSigs_[i] != header.validatorSigs_[i]){ 
                                        equal = false;
                                }   
                        }
                }
                if (header.transactionIds_.size() != transactionIds_.size()){
                        equal = false;
                }
                else{
                        for (int i=0; i < transactionIds_.size(); ++i ){
                                if ((transactionIds_[i]).size() != (header.transactionIds_[i]).size()){
                                        equal = false;
                                }
                                else {
                                        for (int j=0; j < transactionIds_[i].size(); ++j){
                                                if (transactionIds_[i][j] != header.transactionIds_[i][j]){
                                                        equal = false;
                                                }
                                        }
                                }
                        }
                }
                if (epoch_ != header.epoch_ || merkelRoot_ != header.merkelRoot_ || hashedPrefix_ != header.hashedPrefix_ || equal == false){
                        return false;
                }
                else {
                        return true;
                }
        }

/*************************************************
* Name:        getId
*
* Description: Get the epoch number in which the block
*              to which this header belongs was created.
*
* Arguments:   None
*
* Returns epoch_
**************************************************/
        inline int getId() const {return epoch_;}
    
/*************************************************
* Name:        getValidSigs
*
* Description: Get the signatures of the validators
*              who have verified this block
*
* Arguments:   None
*
* Returns validatorSigs_
**************************************************/
        inline std::vector<std::string> getValidSigs() const {return validatorSigs_;}

/*************************************************
* Name:        getHashedPrefix
*
* Description: Get the hash of the previous block.
*
* Arguments:   None
*
* Returns hashedPrefix_
**************************************************/
        inline std::string getHashedPrefix() const {return hashedPrefix_;}

/*************************************************
* Name:        getMerkelRoot
*
* Description: Get the Merkel root tree.
*
* Arguments:   None
*
* Returns merkelRoot_
**************************************************/
        inline std::string getMerkelRoot() const {return merkelRoot_;}

/*************************************************
* Name:        getTxIds
*
* Description: Get the ids of the transactions in
*              the block corresponding to this header.
*
* Arguments:   None
*
* Returns transactionIds_
**************************************************/
        inline std::vector<std::vector<unsigned char>> getTxIds() const {return transactionIds_;}

/*************************************************
* Name:        addSignature
*
* Description: Add a validator signature to the block.
*
* Arguments:   -string signature: the
*               signature of a validator
*
* Returns nothing
**************************************************/
        inline void addSignature(std::string signature){
            validatorSigs_.push_back(signature);
        }

/*************************************************
* Name:        getSize
*
* Description: Estimates the size of the header.
*
* Arguments:   None
*
* Returns size
**************************************************/
        inline int getSize(){
            int size= 0;
            size = size + sizeof(epoch_);
            size = size + sizeof(std::vector<std::string>) + (sizeof(std::string) * validatorSigs_.size());
            size = size + sizeof(hashedPrefix_);
            size = size + sizeof(merkelRoot_);
            size = size + sizeof(std::vector<std::vector<unsigned char>>) + sizeof(std::vector<unsigned char>) * transactionIds_.size();
            for (int i=0; i < transactionIds_.size(); ++i){
                size = size + sizeof(std::vector<unsigned char>)+ sizeof(unsigned char ) * transactionIds_[i].size();
            }
            return size;
            
        }

};
#endif