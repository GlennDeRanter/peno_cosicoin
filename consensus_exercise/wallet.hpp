
#ifndef WALLET_H
#define WALLET_H

#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <vector>
#include <map>
#include <toml.hpp>
#include "Transaction.hpp"
#include "IO.hpp"
#include "KeyPair.hpp"
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using consensus::Consensus;
using consensus::EpochInit;
using consensus::SendTx;
using consensus::Ack;
using consensus::WalletIdentification;
using consensus::InputResponse;


class Wallet {
    public:
// Type definition for the channel used in the grpc.
        typedef std::shared_ptr<Consensus::Stub> ChannelTyp;
// Type definition for the previous transaction to which is referred, this contains a transaction id and an output index.
        typedef std::pair<std::string, unsigned int> previousTransaction;
// Type definition for the UTXO (I assume this will later be removed as this is now a class)
        typedef std::map<previousTransaction, unsigned int> UTXO;

    private:
// A sommation of all the validators.
        std::vector<ChannelTyp> validatorChannels_;
// The id of the wallet.
        std::string walletID_;
// The keypair of the wallet.
        KeyPair keyPair_; //new
// The balance of the wallet.
        unsigned int balance_;
// The list of unspent transactions the wallet is currently synchronised with.
        UTXO utxo_;
// The list of all wallets known by this wallet.
        std::vector<std::string> wallets_;

    public:

/*************************************************
* Name:        Constructor
*
* Description: Constructs the wallet.
*
* Arguments:   -string id: the id we want the wallet
*               to have
*              -string password: password the wallet
*               needs to access his private key.
*              -bool def: to differentiate between this
*               constructor and the next
*
* No Return value
**************************************************/
        Wallet(std::string ID, std::string password, bool def)
            : walletID_(ID)
            , balance_(0) 
            , utxo_({})
            , wallets_()
            {   
                // Put the public keys of other default wallets from the TOML file into the vector wallets_
                const toml::value data1 = toml::parse("defaultWallets.toml");
                const auto& table1 = toml::find(data1, "wallets");
                for(const auto& v : table1.as_array() ) {
                    const auto walletId = toml::find<std::string>(v, "wallet_id");
                    const auto walletPublicKey = toml::find<std::string>(v, "public_key");
                    const auto walletPrivateKey = toml::find<std::string>(v, "private_key");
                    if (getWalletID() == walletId) {
                        keyPair_ = KeyPair(walletPublicKey, walletPrivateKey, password);
                        wallets_.push_back(walletPublicKey);
                    } else {
                        //std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
                        wallets_.push_back(walletPublicKey);
                    }
                }
                // Developers
                if (getWalletID() == "0") {
                   const toml::value data2 = toml::parse("developers.toml");
                   const auto& table2 = toml::find(data2, "developers");
                   const auto key1 = toml::find<std::string>(table2, "public_key");
                   const auto key2 = toml::find<std::string>(table2, "private_key");
                   std::string developersPublicKey = key1;
                   std::string developersPrivateKey = key2;
                   keyPair_ = KeyPair(developersPublicKey, developersPrivateKey, password);
                }
                // Put all node addresses from the TOML file into the vector validatorChannels_
                const toml::value data3 = toml::parse("setup.toml"); // CHANGE IF NOT LOCAL
                const auto& table3 = toml::find(data3, "nodes");
                for(const auto& v : table3.as_array() ) {
                    const auto validatorAddress = toml::find<std::string>(v, "ip_address");
                    ChannelTyp validator = Consensus::NewStub(grpc::CreateChannel(validatorAddress, grpc::InsecureChannelCredentials()));
                    validatorChannels_.push_back(validator);
                }
                // Retrieve the balance
                synchronize();
            }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the wallet.
*
* Arguments:   -string id: the id we want the wallet
*               to have
*              -string password: password the wallet
*               needs to access his private key.
*
* No Return value
**************************************************/
        Wallet(std::string ID, std::string password)
            : walletID_(ID)
            , keyPair_(password)
            , balance_(0) 
            , utxo_({})
            , wallets_()
            {   
                keyPair_.generateKeyPair(password);
                // Put all node addresses from the TOML file into the vector "node"
                const toml::value data = toml::parse("setup.toml"); // CHANGE IF NOT LOCAL
                const auto& table = toml::find(data, "nodes");
                for(const auto& v : table.as_array() ) {
                    const auto validatorAddress = toml::find<std::string>(v, "ip_address");
                    ChannelTyp validator = Consensus::NewStub(grpc::CreateChannel(validatorAddress, grpc::InsecureChannelCredentials()));
                    validatorChannels_.push_back(validator);
                }
                // Retrieve the balance
                synchronize();
            }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old wallet
               in a new wallet.
*
* Arguments:   -Wallet const& wallet: the
*               wallet we want to copy
*
* No Return value
**************************************************/
        Wallet(Wallet const& wallet)
            : walletID_(wallet.getWalletID())
            , keyPair_(wallet.getKeyPair())
            , validatorChannels_(wallet.getValidatorChannels())
            , balance_(wallet.getBalance())
            , utxo_(wallet.getUTXO())
            , wallets_(wallet.getWallets())
            {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the wallet is equal
*              to a given wallet and otherwise 
*              makes sure they are.
*
* Arguments:   -Wallet const& wallet: the
*               wallet we want to match to
*
* Returns *this (success)
**************************************************/
        Wallet& operator=(Wallet const& wallet){
            if (walletID_ != wallet.getWalletID() || balance_ != wallet.getBalance()) {  
                walletID_ = wallet.getWalletID();
                keyPair_ = wallet.getKeyPair();
                validatorChannels_ = wallet.getValidatorChannels();
                balance_ = wallet.getBalance();
                utxo_ = wallet.getUTXO();
                wallets_ = wallet.getWallets();
            }
            return *this;
        }

/*************************************************
* Name:        sendTransaction
*
* Description: Function to let the wallet send a transaction.
*
* Arguments:   -Transaction transaction: the transaction we want
*               the wallet to send
*
* Returns a boolean that indicates whether the wallet was able to
* send the transaction, and gives the id of the validator to which the
* transaction was sent if it was succesful
**************************************************/
        std::pair<bool,std::string> sendTransaction(Transaction& transaction);

/*************************************************
* Name:        synchronize
*
* Description: Function to synchronize the wallet 
*              with the validators (updates UTXO 
*              and balance).
*
* Arguments:   None
*
* Returns a boolean that indicates whether the wallet was able to
* update his balance, returns also a reason why it failed to synchronize
* or the new balance.
**************************************************/
        std::pair<bool,std::string> synchronize();

/*************************************************
* Name:        checkTransaction
*
* Description: Function that checks a transaction
*              given the receivers and corresponding
*              values.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               a list of all the receivers, containing for each receiver
*               their public key and the amount we wish to transfer to them
*
* Returns a boolean that indicates whether the wallet was able to
* validate the transaction and if not returns a reason why not
**************************************************/
        std::pair<bool,std::string> checkTransaction(std::vector<std::pair<std::string, unsigned int>>& receivers);

/*************************************************
* Name:        makeTransaction
*
* Description: Function that makes a transaction
*              given the receivers and corresponding
*              values.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               a list of all the receivers, containing for each receiver
*               their public key and the amount we wish to transfer to them
*              -std::string transactionId: the id we want to give to the
*               transaction
*
* Returns a boolean that indicates whether the wallet was able to
* sign the transaction and if not the reason why not and also the built
* transaction
**************************************************/
        std::pair<std::pair<bool,std::string>,Transaction> makeTransaction(std::vector<std::pair<std::string, unsigned int>>& receivers, std::string& transactionId);

/*************************************************
* Name:        signTransaction
*
* Description: Function that lets the wallet sign a
*              transaction.
*
* Arguments:   -Transaction transaction: the transaction
*               the wallet needs to sign
*
* Returns a boolean saying whether the transaction was signed
* correctly and returns a string saying you failed to give the
* correct password if not
**************************************************/
        std::pair<bool,std::string> signTransaction(Transaction& transaction);

/*************************************************
* Name:        passwordPrompt
*
* Description: Function that asks for the password
*              and gives you three guesses to enter
*              the correct password.
*
* Arguments:   None
*
* Returns a boolean saying whether the given password was
* correct and if not the amount of attempts remaining.
**************************************************/
        std::pair<bool,std::string> passwordPrompt();

/*************************************************
* Name:        updateBalance
*
* Description: Function to calculate the balance.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void updateBalance() {
            unsigned int newBalance = 0;
            for (const auto& output: getUTXO()) {
                newBalance += output.second;
            }
            balance_ = newBalance;
        }

/*************************************************
* Name:        getValidatorChannels
*
* Description: Function to get the validators.
*
* Arguments:   None
*
* Returns validatorChannels_
**************************************************/
        inline std::vector<ChannelTyp> getValidatorChannels() const {return validatorChannels_;}

/*************************************************
* Name:        getWalletID
*
* Description: Function to get the wallet's ID.
*
* Arguments:   None
*
* Returns walletID_
**************************************************/
        inline std::string getWalletID() const {return walletID_;}

/*************************************************
* Name:        getKeyPair
*
* Description: Function to get the keypair.
*
* Arguments:   None
*
* Returns keyPair_
**************************************************/
        inline KeyPair getKeyPair() const {return keyPair_;}

/*************************************************
* Name:        getBalance
*
* Description: Function to get the balance.
*
* Arguments:   None
*
* Returns balance_
**************************************************/
        inline unsigned int getBalance() const {return balance_;} 

/*************************************************
* Name:        getUTXO
*
* Description: Function to get the UTXO.
*
* Arguments:   None
*
* Returns utxo_
**************************************************/
        inline UTXO getUTXO() const {return utxo_;}    

/*************************************************
* Name:        getWallets
*
* Description: Function to get all the wallets this wallet
*              knows.
*
* Arguments:   None
*
* Returns wallets_
**************************************************/
        inline std::vector<std::string> getWallets() const {return wallets_;}

/*************************************************
* Name:        getKnownWallets
*
* Description: Function to display all past used receivers this wallet
*              knows and ask which known wallet(s) to use as receiver.
*
* Arguments:   None
*
* Returns the indexes of the receivers to which 
*         the wallet wants to send in a vector
**************************************************/
        std::vector<int> getKnownWallets();

/*************************************************
* Name:        validPublicKey
*
* Description: Function to check whether the given
*              public key is valid
*
* Arguments:   - The the public key to check
*
* Returns a boolean indicating if the public key is 
*         valid and the reason if not
**************************************************/
        inline std::pair<bool,std::string> validPublicKey(std::string publicKey) {
              if (publicKey.length() != 5184) {
                 return std::pair<bool,std::string>(false, "The public key of the receiver should be 5184 hexadecimal characters long!");
              }
              if (publicKey.find_first_not_of("0123456789abcdefABCDEF", 2) != std::string::npos) {
                 return std::pair<bool,std::string>(false, "The public key of the receiver should contain only hexadecimal characters!");
              }
              return std::pair<bool,std::string>(true, "");
        }


/*************************************************
* Name:        validStringOfIntegers
*
* Description: Function to check whether the given
*              is valid to convert to (strict pos.) 
*              int
*
* Arguments:   - std::string str: the string to check
*
* Returns a boolean indicating if the string is 
*         valid and the reason if not
**************************************************/
        inline std::pair<bool,std::string> validStringOfIntegers(std::string str) {
              if (str == "0") {
                 return std::pair<bool,std::string>(false, "The input should be larger than zero!");
              }
              if (str.length() < 1) {
                 return std::pair<bool,std::string>(false, "The input is empty!");
              }
              if (str.find_first_not_of("0123456789") != std::string::npos) {
                 return std::pair<bool,std::string>(false, "The input isn't a positive integer!");
              }
              return std::pair<bool,std::string>(true, "");
        }

/*************************************************
* Name:        sendingTransaction
*
* Description: Function to send the transaction to the wallet
*              and print information of the transaction to the 
*              wallet user.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               pairs of the public keys of the people we want to send to and
*               the amount of coins we want to send them
*              -std::string transactionId: the id of the transaction we want to build
*              -std::string numberOfReceivers: the number of people we want to send something
*              -std::string amountLinde: the amount of coins we want to send linde
*              -std::string amountGlenn: the amount of coins we want to send Glenn
*              -std::string amountMilan: the amount of coins we want to send Milan
*
* Returns nothing
**************************************************/
        inline void sendingTransaction(std::vector<std::pair<std::string, unsigned int>>& receivers, std::string transactionId, std::string numberOfReceivers, std::string receiver, std::string amountLinde, std::string amountGlenn, std::string amountMilan){
                std::pair<std::pair<bool,std::string>,Transaction> result = makeTransaction(receivers, transactionId);
	        if (std::get<bool>(result.first)) {
		        std::pair<bool,std::string> sendRes = sendTransaction(std::get<Transaction>(result));
		        std::cout << std::get<std::string>(sendRes) << std::endl;
		        if (std::get<bool>(sendRes)) {
			        if ((numberOfReceivers =="2" && getWalletID() !="1") || (numberOfReceivers == "1" && receiver == "1")){
			                std::cout << "                 -->" << "To Milan: " << amountMilan << " coins" << std::endl;	
			        }
			        if ((numberOfReceivers =="2" && getWalletID() !="2") || (numberOfReceivers == "1" && receiver == "2")){
			                std::cout << "                 -->" << "To Linde: " << amountLinde << " coins" << std::endl;	
			        }
			        if ((numberOfReceivers =="2" && getWalletID() !="3") || (numberOfReceivers == "1" && receiver == "3")){
			                std::cout << "                 -->" << "To Glenn: " << amountGlenn << " coins" << std::endl;	
			        }
		        }
	        }
                else {
                        std::cout << "[WALLET " << getWalletID() << "] --> " << std::get<std::string>(result.first) << std::endl;
                }
        }

/*************************************************
* Name:        createTransactionIdAndCommitTransaction
*
* Description: Function to create the transaction and show the
*              commit interface to the user.
*
* Arguments:   -None
*
* Returns {transactionId, send}
**************************************************/
        inline std::vector<std::string> createTransactionIdAndCommitTransaction(){
		std::cout << "" << std::endl;
                std::cout << "---------TRANSACTION ID---------" << std::endl;
                std::string transactionId = "-";
                while (transactionId == "-") {
                        std::cout << "Transaction id (possible to leave blank): ";
                        getline(std::cin, transactionId);
                        if (std::get<bool>(validStringOfIntegers(transactionId)) == false && transactionId != "") {
                            std::cout << std::get<std::string>(validStringOfIntegers(transactionId)) << std::endl;
                            transactionId = "-";
                        }
                    }
                    
                    std::cout << "" << std::endl;
                    std::cout << "---------COMMIT---------" << std::endl;
                    std::string send = "";
                    while (send != "y" && send != "a") {
                        std::cout << "Commit the transaction (y/n/a): ";
                        getline(std::cin, send);
                    }
               return {transactionId, send};
	}

/*************************************************
* Name:        receiveAmount
*
* Description: Function to interface with the user about
*              which amount he wants to send to a certain
*              wallet.
*
* Arguments:   -std::string amount: the amount we want to fill in
*              -std::vector<std::pair<std::string, unsigned int>> receivers:
*               the vector of receivers we want to fill in
*              -std::string receiver: the person we want to send coins to
*
* Returns amount
**************************************************/
        inline std::string receiveAmount(std::string& amount, std::vector<std::pair<std::string, unsigned int>>& receivers, std::string receiver){
		std::vector<std::string> names = {"Milan", "Linde", "Glenn"};
                //names.erase(names.begin() + stoi(getWalletID()) - 1); 
                while (amount == "") {
                        std::cout << "Coins to wallet " << names[stoi(receiver)-1] << ": ";
                        getline(std::cin, amount);
                        if (std::get<bool>(validStringOfIntegers(amount)) == false) {
                                std::cout << std::get<std::string>(validStringOfIntegers(amount)) << std::endl;
                                amount = "";
                        }
                }
                std::string publicKey = (getWallets()[stoi(receiver)-1]);
                std::pair<std::string, unsigned int> output(publicKey, stoi(amount));
                receivers.push_back(output);
                return amount;
	}

/*************************************************
* Name:        createOutputs
*
* Description: Function to create the outputs
*              of the transaction.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>>& receivers: 
*               a sommation of the public keys of the wallets we want to send
*               coins to and the corresponding amount of coins
* Returns {outputs, spendAmount}
**************************************************/
        inline auto createOutputs(std::vector<std::pair<std::string, unsigned int>>& receivers){
                std::vector<Output> outputs;
                unsigned int spendAmount = 0;
                for (const auto& receiver: receivers) {
                        std::string receiverPk = std::get<std::string>(receiver);
                        unsigned int value = std::get<unsigned int>(receiver);
                        std::cout << "RECEIVER: " << value << std::endl;
                        spendAmount += value;

                        Output out(value, receiverPk);
                        outputs.push_back(out);
                }
                struct retVals{
                        std::vector<Output> outputs;
                        unsigned int spendAmount;
                };
                return retVals {outputs, spendAmount};
        }

/*************************************************
* Name:        completeTransaction
*
* Description: Function to finish the transaction outputs
*              if the complete input was not spend yet
*              and signs it.
*
* Arguments:   -unsigned int returnedCoins: the amount of
*               unspend input coins
*              -std::vector<Input> inputs: the inputs that
*               belong to the transaction
*              -std::vector<Output> outputs: the outputs that
*               belong to the vector
*              -std::vector<unsigned char> id: the id of the
*               transaction
* Returns {result2, transaction}
**************************************************/
        inline auto completeTransaction(unsigned int& returnedCoins, std::vector<Input>& inputs, std::vector<Output>& outputs, std::vector<unsigned char>& id){
                std::pair<bool, std::string> result2;
                struct retVals{
                        std::pair<bool,std::string> result2;
                        Transaction transaction;
                };
                if (returnedCoins == 0) {
                        Transaction transaction(inputs, outputs, "", id);
                        std::pair<bool,std::string> result2 = signTransaction(transaction); 
                        return retVals {result2, transaction}; 
                } else {
                        Output out(returnedCoins, getKeyPair().getPublicKey());
                        outputs.push_back(out);
                        Transaction transaction(inputs, outputs, "", id);
                        std::pair<bool,std::string> result2 = signTransaction(transaction);
                        return retVals {result2, transaction}; 
                }                               
        }

/*************************************************
* Name:        handleKnownReceivers
*
* Description: Function to handle the interface to
*              send coins to wallets the given wallet
*              already knows.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               The public key and the corresponding amounts of receivers 
*               we want to send to
* Returns receivers
**************************************************/
        inline std::vector<std::pair<std::string, unsigned int>> handleKnownReceivers(std::vector<std::pair<std::string, unsigned int>>& receivers){
                std::cout << "" << std::endl;
                std::cout << "---------KNOWN RECEIVERS---------" << std::endl;
                // Add the known receiver(s)
                std::vector<int> knownReceivers = getKnownWallets();
                for (auto& index: knownReceivers) {
                        std::string amount = "";
                        std::string receiverPublicKey = getWallets()[index-1];
                        while (amount == "") {
                            std::cout << "Coins to '" << receiverPublicKey.substr(0, 100) << "': ";
                            getline(std::cin, amount);
                            if (std::get<bool>(validStringOfIntegers(amount)) == false) {
                                std::cout << std::get<std::string>(validStringOfIntegers(amount)) << std::endl;
                                amount = "";
                            }
                        }
                        // Add the receiver
                        std::pair<std::string, unsigned int> newReceiver = std::pair<std::string, unsigned int>(receiverPublicKey, stoi(amount));
                        receivers.push_back(newReceiver);
                }
                return receivers;
        }

/*************************************************
* Name:        isValidReceiver
*
* Description: Function to check whether the new
*              receiver is a valid new receiver.
*
* Arguments:   -std::string receiver: the receiver we want to verify
*              -std::vector<std::pair<std::string, unsigned int>> receivers:
*               pairs of the public keys of the people we want to send to and
*               the amount of coins we want to send them
* Returns the receiver public key is valid
**************************************************/
        inline bool isValidReceiver(std::string receiver, std::vector<std::pair<std::string, unsigned int>>& receivers){
                // Is the receiver public key valid?
                if (std::get<bool>(validPublicKey(receiver)) == false && receiver != "") {
                        std::cout << std::get<std::string>(validPublicKey(receiver)) << std::endl;
                        std::cout << "" << std::endl;
                        return false;
                }
                // Check whether the receiver is already in the (draft) transaction
                for (auto& receiverRecord: receivers) {
                        std::string receiverPk = std::get<std::string>(receiverRecord);
                        if (receiverPk == receiver) {
                                std::cout << "The receiver is already added!" << std::endl;
                                std::cout << "" << std::endl;
                                return false;
                        }
                }  
                return true; 
        }

/*************************************************
* Name:        endingOfSendTransactionInterfaceCustom
*
* Description: Function to finalize the transaction and
*              send it.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               pairs of the public keys of the people we want to send to and
*               the amount of coins we want to send them
* Returns nothing
**************************************************/
        inline void endingOfSendTransactionInterfaceCustom(std::vector<std::pair<std::string, unsigned int>>& receivers){
                std::vector<std::string> result = createTransactionIdAndCommitTransaction();
                if (result[1] != "a") {
                        sendingTransactionCustom(receivers, result[0]);
                }
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::cout << "" << std::endl;
                std::cout << "" << std::endl;
        }

/*************************************************
* Name:        finishedInterface
*
* Description: Function to check whether the user is
*              finished adding more users.
*
* Arguments:   -bool addReceiver: boolean that indicates
*               whether we want to add another receiver
* Returns addReceiver
**************************************************/
        inline bool finishedInterface(bool addReceiver){
                std::string finished = "";
                while (finished != "n" && finished != "y") {
                        std::cout << "Do you want to add a new receiver (y/n): ";
                        getline(std::cin, finished);
                }
                std::cout << "" << std::endl;
                if (finished == "n") {
                        addReceiver = false;
                }
                return addReceiver;
        }

/*************************************************
* Name:        addNewReceiver
*
* Description: Function to add a new wallet to the
*              known wallets.
*
* Arguments:   -std::string receiver: the public key
*               of the wallet we wish to add
* Returns nothing
**************************************************/
        inline void addNewReceiver(std::string receiver){
                bool found = false;
                for (auto& walletPk: getWallets()) {
                        if (walletPk == receiver) {
                                found = true;
                        }
                }
                if (found == false) {
                        wallets_.push_back(receiver);
                }
        }

/*************************************************
* Name:        sendTransactionInterfaceCustom
*
* Description: Function to handle the send transaction
*              interface of the customized wallet.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               the list of receiver public key and sent amount we want to
*               fill in
* Returns nothing
**************************************************/
        inline void sendTransactionInterfaceCustom(std::vector<std::pair<std::string, unsigned int>>& receivers){
                receivers = handleKnownReceivers(receivers);
                    bool addReceiver = true;
                    std::cout << "" << std::endl;
                    std::cout << "---------NEW RECEIVERS---------" << std::endl;
                    while (addReceiver) {
                        std::string receiver = "-";
                        while (receiver == "-") {
                            std::string read;
                            while (read != "y" && read != "n") {
                                std::cout << "Read receiver's public key from file 'publicKey.txt' (y/n): ";
                                getline(std::cin, read);
                            }
                            receiver = publicKeyFromFile();
                            if (read == "n") {
                                if (receivers.empty()) {
                                    receiver = "-";
                                    std::cout << "Should at least add one receiver!" << std::endl;
                                    std::cout << "" << std::endl;
                                } else {
                                    goto End;
                                }
                            } else {
                                if (isValidReceiver(receiver, receivers) == false) {
                                        receiver = "-";
                                }
                            }
                        }
                        std::string amount = "";
                        while (amount == "") {
                            std::cout << "Coins to '" << receiver.substr(0, 100) << "': ";
                            getline(std::cin, amount);
                            if (std::get<bool>(validStringOfIntegers(amount)) == false) {
                                std::cout << std::get<std::string>(validStringOfIntegers(amount)) << std::endl;
                                amount = "";
                            }
                        }
                        std::pair<std::string, unsigned int> newReceiver = std::pair<std::string, unsigned int>(receiver, stoi(amount));
                        receivers.push_back(newReceiver);
                        addNewReceiver(receiver);
                        addReceiver = finishedInterface(addReceiver);
                    }
                    End:
                    endingOfSendTransactionInterfaceCustom(receivers);
        }

/*************************************************
* Name:        sendingTransactionCustom
*
* Description: Function to send the transaction to the wallet
*              and print information of the transaction to the 
*              wallet user.
*
* Arguments:   -std::vector<std::pair<std::string, unsigned int>> receivers:
*               pairs of the public keys of the people we want to send to and
*               the amount of coins we want to send them
*              -std::string transactionId: the id of the transaction we want to build
*
* Returns nothing
**************************************************/
        inline void sendingTransactionCustom(std::vector<std::pair<std::string, unsigned int>>& receivers, std::string transactionId){
                std::pair<std::pair<bool,std::string>,Transaction> result = makeTransaction(receivers, transactionId);
                if (std::get<bool>(result.first)) {
                        std::pair<bool,std::string> sendRes = sendTransaction(std::get<Transaction>(result));
                        // Print the result of the grpc
                        std::cout << std::get<std::string>(sendRes) << std::endl;
                        // Print the transaction  
                        if (std::get<bool>(sendRes)) {
                                Transaction transactionToPrint = std::get<Transaction>(result);
                                // Transaction
                                std::cout << "Send transaction: " << transactionToPrint.IdToString() << std::endl;

                                // Inputs
                                for (auto& senderRecord: transactionToPrint.getInputs()) {
                                    std::string txId = senderRecord.IDToString();
                                    int txOffset = senderRecord.getOutputIndex();
                                    std::cout << "   <-- Sender input: " << txId << " at " << txOffset << std::endl;
                                }

                                // Outputs
                                for (auto& receiverRecord: transactionToPrint.getOutputs()) {
                                    std::string receiverPk = receiverRecord.getReceiverPk();
                                    unsigned int receiverValue = receiverRecord.getValue();
                                    std::cout << "   --> Receiver: " << receiverPk.substr(0, 100) << ", " << receiverValue << std::endl;
                                }
                        }
                } else {
                        std::cout << "[WALLET " << getWalletID() << "] --> " << std::get<std::string>(result.first) << std::endl;
                }
        }

/*************************************************
* Name:        sendTransactionInterface
*
* Description: Function to interface with the user about
*              sending a transaction.
*
* Arguments:   -std::vector<std::string> possibleReceivers: all the possible receivers
*               this wallet could send to 
*              -std::string amountMilan: the amount of coins we want to send to Milan
*              -std::string amountLinde: the amount of coins we want to send to Linde
*              -std::string amountGlenn: the amount of coins we want to send to Glenn
*              -std::vector<std::pair<std::string, unsigned int>> receivers:
*               the vector of receivers we want to fill in
*              -std::string receiver: the person we want to send coins to
*              -std::string numberOfreceivers: the number of receiver we want to send 
*               coins to
*
* Returns nothing
**************************************************/
inline void sendTransactionInterface(std::vector<std::string>& possibleReceivers, std::string amountMilan, std::string amountLinde, std::string amountGlenn, std::vector<std::pair<std::string, unsigned int>>& receivers, std::string numberOfreceivers, std::string receiver){
	std::cout << "" << std::endl;
        std::cout << "---------RECEIVERS---------" << std::endl;
        std::vector<std::string> names = {"Milan", "Linde", "Glenn"};
        names.erase(names.begin() + stoi(getWalletID()) - 1); 
        while (numberOfreceivers != "1" && numberOfreceivers != "2") {
        	std::cout << "Number of receivers (1/2): ";
                getline(std::cin, numberOfreceivers);
        }
        if (numberOfreceivers == "1"){
       	        while (receiver != possibleReceivers[0] && receiver != possibleReceivers[1]){
       		        std::cout << "Receiver " << names[0] << "/" << names[1] << " (" << possibleReceivers[0] << "/" << possibleReceivers[1] << "): ";
       		        getline(std::cin, receiver);
       	        }
       	        possibleReceivers.clear();
       	        possibleReceivers.push_back(receiver);
        }
        if ((std::find(possibleReceivers.begin(),possibleReceivers.end(),"1")) != possibleReceivers.end()){
       	        amountMilan = receiveAmount(amountMilan, receivers, "1");
        }
        if ((std::find(possibleReceivers.begin(),possibleReceivers.end(),"2")) != possibleReceivers.end()){
       		amountLinde = receiveAmount(amountLinde, receivers, "2");
        }
        if ((std::find(possibleReceivers.begin(),possibleReceivers.end(),"3")) != possibleReceivers.end()) {
       		amountGlenn = receiveAmount(amountGlenn, receivers, "3");
        }
	std::vector<std::string> result = createTransactionIdAndCommitTransaction();
        if (result[1] != "a") {
                sendingTransaction(receivers, result[0], numberOfreceivers, receiver, amountLinde, amountGlenn, amountMilan);
        }
        std::cout << "------------------------------------------------------------------------------------------" << std::endl;
        std::cout << "------------------------------------------------------------------------------------------" << std::endl;
        std::cout << "" << std::endl;
        std::cout << "" << std::endl;
                    
}

/*************************************************
* Name:        runDefaultWallet
*
* Description: Function to run the default wallets.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
inline void runDefaultWallet(){
        while (true) {
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::cout << "------------------------------------------------------------------------------------------" << std::endl;
                std::string function = "";
                std::string numberOfreceivers = "";
                std::string receiver = "";
                std::string amountLinde = "";
                std::string amountGlenn = "";
                std::string amountMilan = "";
                std::vector<std::pair<std::string, unsigned int>> receivers;

                while (function != "1" && function != "2" && function != "3" && function != "4") {
                    std::cout << "Send transaction/Synchronize/Configuration/Quit (1/2/3/4): ";
                    getline(std::cin, function);
                }
                std::cout << "" << std::endl;

                if (function == "1") {
                    std::vector<std::string> possibleReceivers = {"1","2","3"};
                    possibleReceivers.erase(possibleReceivers.begin() + stoi(getWalletID()) - 1); 
                    sendTransactionInterface(possibleReceivers, amountMilan, amountLinde, amountGlenn, receivers, numberOfreceivers, receiver);
                } else if (function == "2") {
                    walletSynchronizationInterface();

                } else if (function == "3") {
                    walletConfigurationInterface();
                } else {
                    return;
                }
            }
}

/*************************************************
* Name:        publicKeyFromFile
*
* Description: returns a string that contains the info 
*              inside a txt file.
* Arguments:   None
*
* Returns the string
**************************************************/   
        inline std::string publicKeyFromFile(){
                std::string fileInfo;
                std::string filePub = "publicKey.txt";
                ifstream file(filePub, ifstream::binary);
                file >> fileInfo;
                return fileInfo;
        }

/*************************************************
* Name:        run
*
* Description: Function that actually runs an 
*              interactive wallet.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        void run();

/*************************************************
* Name:        walletSynchronizationInterface
*
* Description: Function to do the synchronization
*              function of the wallet interface.
*
* Arguments:   None
*
* Returns nothing
**************************************************/

        void walletSynchronizationInterface();

/*************************************************
* Name:        walletConfigurationInterface
*
* Description: Function to do the configuration
*              function of the wallet interface.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        void walletConfigurationInterface();
};

int main(int argc, char const *argv[]);

#endif