#ifndef TRANSACTION_H
#define TRANSACTION_H
#include "IO.hpp"
#include "convert.hpp"
#include <vector>
#include <algorithm>
#include <utility>
#include <string>
#include <complex>
#include <tuple>
#include <iostream>
#include <cryptopp/sha3.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>

using namespace CryptoPP;

class Transaction{

    private:
// A sommation of the inputs of the transaction.
        std::vector<Input> inputs_;
// A sommation of the outputs of the transaction.
        std::vector<Output> outputs_;
// The signature of the person who will pay the amount specified in the outputs.
        std::string senderSig_;
// The ID tag of the transaction.
        std::vector<unsigned char> txID_;
        
    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the transaction.
*
* Arguments:   None
*
* No Return value
**************************************************/
        Transaction()
        : inputs_({})
        , outputs_({})
        , senderSig_("")
        , txID_({})
        {}

/*************************************************
* Name:        Constructor
*
* Description: Constructs the transaction.
*
* Arguments:   -const std::vector<Input>& inputs: a sommation of 
*               the inputs of the transaction
*              -const std::vector<Output>& outputs: a sommation of 
*               the outputs of the transaction
*              -const std::string senderSig: the signature of the 
*               person who will pay the amount specified in the outputs
*              -const std::vector<unsigned char>& txId: the ID tag of 
*               the transaction 
*
* No Return value
**************************************************/
        Transaction(const std::vector<Input>& inputs, const std::vector<Output>& outputs, const std::string senderSig,const std::vector<unsigned char>& txId)
        : inputs_(inputs)
        , outputs_(outputs)
        , senderSig_(senderSig)
        , txID_(txId)
        {}

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old transaction
               in a new transaction.
*
* Arguments:   -Transaction const& transaction: the
*               transaction we want to copy
*
* No Return value
**************************************************/
        Transaction(Transaction const& transaction)
        : inputs_(transaction.inputs_)
        , outputs_(transaction.outputs_)
        , senderSig_(transaction.senderSig_)
        , txID_(transaction.txID_)
        {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the transaction is equal
*              to a given transaction and otherwise 
*              makes sure they are.
*
* Arguments:   -const Transaction& transaction: the
*               transaction we want to match to
*
* Returns *this (success)
**************************************************/
        Transaction& operator=(Transaction const& transaction){
                if (!(*this == transaction)){
                        inputs_ = transaction.inputs_;
                        outputs_ = transaction.outputs_;
                        senderSig_ = transaction.senderSig_;
                        txID_ = transaction.txID_;
                }
            return *this;
        }

/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the transaction matches a 
*              given input.
*
* Arguments:   Input const& objInput1: inputobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
        bool operator==(Transaction const& transaction){
                if (inputs_.size() != transaction.inputs_.size()){
                        return false;
                }
                else {
                        for (int i=0; i<inputs_.size(); ++i){
                                if (!(inputs_[i] == transaction.inputs_[i])){
                                        return false;
                                }
                        }
                        if (outputs_.size() != transaction.outputs_.size()){
                                return false;
                        }
                        else {
                                for (int i=0; i<outputs_.size(); ++i){
                                        if (!(outputs_[i] == transaction.outputs_[i])){
                                                return false;
                                        }
                                }
                                if (senderSig_ != transaction.senderSig_){
                                        return false;
                                }
                                else {
                                        if (txID_.size() != transaction.txID_.size()){
                                                return false;
                                        }
                                        else {
                                                for (int i=0; i<txID_.size(); ++i){
                                                        if (txID_[i] != transaction.txID_[i]){
                                                                return false;
                                                        }
                                                }
                                        }
                                }
                        }
                }
        return true;
        }
        
/*************************************************
* Name:        getInputs
*
* Description: Get the inputs of the transaction.
*
* Arguments:   None
*
* Returns inputs_
**************************************************/
        inline std::vector<Input> getInputs() const {return inputs_;}

/*************************************************
* Name:        getOutputs
*
* Description: Get the outputs of the transaction.
*
* Arguments:   None
*
* Returns outputs_
**************************************************/
        inline std::vector<Output> getOutputs() const {return outputs_;}

/*************************************************
* Name:        getSenderSig
*
* Description: Get the signature of the person who sent
*              out the transaction.
*
* Arguments:   None
*
* Returns senderSig
**************************************************/
        inline std::string getSenderSig() const {return senderSig_;}

/*************************************************
* Name:        setSenderSig
*
* Description: Set the sendersig to a given sender 
*              signature.
*
* Arguments:   -std::string senderSig: the sender
*               signature we want to set 
*
* Returns nothing
**************************************************/
        inline void setSenderSig(std::string sendersig){
                senderSig_ = sendersig;
        }

/*************************************************
* Name:        getId
*
* Description: Get the id of the transaction.
*
* Arguments:   None
*
* Returns txId_
**************************************************/
        inline std::vector<unsigned char> getId() const {return txID_;}

/*************************************************
* Name:        hashTransaction
*
* Description: Returns the hash of the transaction object.
*
* Arguments:   None
*
* Returns hashedtransaction
**************************************************/
        inline std::string hashTransaction(){
            std::string transactionString= "";
            int nbInputs = inputs_.size();
            for (int i=0; i< nbInputs; i++){
                transactionString = transactionString + (inputs_[i]).inputToString();
            }
            int nbOutputs = outputs_.size();
            for (int i=0; i< nbOutputs;++i){
                transactionString = transactionString + (outputs_[i]).outputToString();
            }
            int txSize = txID_.size();
            for (int i=0; i< txSize;++i){
                transactionString = transactionString + std::to_string(txID_[i]);
            }
            SHA3_256 hash;
            std::string hashedTransaction;
            hash.Update((const CryptoPP::byte*)transactionString.data(),transactionString.size());
            hashedTransaction.resize(hash.DigestSize());
            hash.Final((CryptoPP::byte*) &hashedTransaction[0]);
            std::string hashedtransaction = ASCIIToHex(hashedTransaction);
            return hashedtransaction;
        }

/*************************************************
* Name:        IdToString
*
* Description: Transform the id of the transaction into
*              a string in order to be able to hash it.
*
* Arguments:   None
*
* Returns idString
**************************************************/
        inline std::string IdToString(){
            std::string idString = "";
            int txSize = txID_.size();
            for (int i=0; i<txSize;i++){
                idString.push_back(txID_[i]);
            }
            return idString;
        }

/*************************************************
* Name:        getSize
*
* Description: Estimate the size of the transaction.
*
* Arguments:   None
*
* Returns size
**************************************************/
        inline int getSize(){
            int size = 0;
            size = size + sizeof(std::vector<Input>);
            for (int i= 0; i < inputs_.size(); ++i){
                size = size + (inputs_[i]).getSize();
            }
            size = size + sizeof(std::vector<Output>);
            for (int i= 0; i < outputs_.size(); ++i){
                size = size + (outputs_[i]).getSize();
            }
            size = size + sizeof(senderSig_);
            size = size + sizeof(std::vector<unsigned char>) + (sizeof(unsigned char) * txID_.size());
            return size;
        }
};    

#endif