#ifndef  BLOCKCHAIN_H
#define BLOCKCHAIN_H 

#include <cryptopp/sha3.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Transaction.hpp"
#include "Block.hpp"
#include "UTXO.hpp"
#include "toml.hpp"


using namespace CryptoPP;
using namespace std;

class BlockChain{
    private:
// A vector with all the blocks that belong to the chain.
        std::vector<Block> blocks_;
// The length of the block chain without the genesis block.
        int chainLength_ = 0;
// The genesis block at the beginning of the chain.
        Block genesisBlock_;
// The epoch number of the last block in the chain.
        int currentEpoch_ = 0;
// The index of the last block in the chain that has been finalized.
        int indexLastFinalizedBlock_ = 0;
// The unspend transaction outputs for the blockchain.
        UTXO utxo_;

    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the blockchain and takes no arguments.
*
* Arguments:   None
*
* No Return value
**************************************************/
        BlockChain()
            : blocks_({}) // all the blocks in the chain
            , chainLength_(0) // should be the number of blocks without the genesis block // the genesis block at the beginning of the chain
            , currentEpoch_(0) // the epoch number of the last block in the chain
            , indexLastFinalizedBlock_(0) // the index of the last finalized block in the block chain 
            {
                std::vector<std::pair<std::string,std::string>> defaultWallets;
                // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
                const toml::value data1 = toml::parse("defaultWallets.toml");
                const auto& table1 = toml::find(data1, "wallets");
                for(const auto& v : table1.as_array() ) {
                    const auto walletId = toml::find<std::string>(v, "wallet_id");
                    const auto walletPublicKey = toml::find<std::string>(v, "public_key");
                    std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
                    defaultWallets.push_back(defaultWallet);
                }
                const toml::value data2 = toml::parse("developers.toml");
                const auto& table2 = toml::find(data2, "developers");
                const auto key = toml::find<std::string>(table2, "public_key");
                std::string developersPublicKey = key;
                // make the default genesis block
                Block genesisBlock = createGenesisBlock(defaultWallets);
                genesisBlock_ = genesisBlock;
                blocks_.push_back(genesisBlock_);
                UTXO utxo;
                utxo.initialize({genesisBlock});
                utxo_ = utxo;
            }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the blockchain.
*
* Arguments:   -int utxoId: the id we want the utxo to have
*
* No Return value
**************************************************/ 
        BlockChain(int utxoId)
            : chainLength_(0)
            , currentEpoch_(0)
            , indexLastFinalizedBlock_(0)
            , utxo_(utxoId)
            {
                std::vector<std::pair<std::string,std::string>> defaultWallets;
                // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
                const toml::value data1 = toml::parse("defaultWallets.toml");
                const auto& table1 = toml::find(data1, "wallets");
                for(const auto& v : table1.as_array() ) {
                    const auto walletId = toml::find<std::string>(v, "wallet_id");
                    const auto walletPublicKey = toml::find<std::string>(v, "public_key");
                    std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
                    defaultWallets.push_back(defaultWallet);
                }
                // Take the public key from the developers out of the TOML file
                const toml::value data2 = toml::parse("developers.toml");
                const auto& table2 = toml::find(data2, "developers");
                const auto key = toml::find<std::string>(table2, "public_key");
                std::string developersPublicKey = key;
                //make genesis block
                Block genesisBlock = createGenesisBlock(defaultWallets);
                genesisBlock_ = genesisBlock;
                blocks_.push_back(genesisBlock_);
                utxo_.initialize({genesisBlock});
            }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the blockchain.
*
* Arguments:   -const int currentEpoch: the epoch number 
*               of the last block in the chain
*              -Block block: the genesis block at the 
*               beginning of the chain
*              -int utxoId: the id we want the utxo to have
*
* No Return value
**************************************************/
        BlockChain(const int currentEpoch, Block block, int utxoId)
            : currentEpoch_(currentEpoch)
            , chainLength_(0)
            , indexLastFinalizedBlock_(0)
            {
                genesisBlock_= block;
                blocks_ = {genesisBlock_};
                UTXO utxo_(utxoId);
                utxo_.initialize(blocks_);
            }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old blockchain
               in a new blockchain.
*
* Arguments:   -const BlockChain& blockchain: the blockchain
*               we want to copy
*
* No Return value
**************************************************/
        BlockChain(BlockChain const& blockchain)
            : chainLength_(blockchain.chainLength_)
            , genesisBlock_(blockchain.genesisBlock_)
            , currentEpoch_(blockchain.currentEpoch_)
            , blocks_(blockchain.blocks_)
            , indexLastFinalizedBlock_(blockchain.indexLastFinalizedBlock_)
            , utxo_(blockchain.utxo_)
            {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the blockchain is equal
*              to a given blockchain and otherwise 
*              makes sure they are.
*
* Arguments:   -const BlockChain& blockchain: the blockchain
*               we want to match to
*
* Returns *this (success)
**************************************************/
        inline BlockChain& operator=(BlockChain const& blockchain){
            if (!(*this == blockchain)){
                chainLength_ = blockchain.chainLength_;
                genesisBlock_ = blockchain.genesisBlock_;
                currentEpoch_ = blockchain.currentEpoch_;
                blocks_ = blockchain.blocks_;
                indexLastFinalizedBlock_ = blockchain.indexLastFinalizedBlock_;
                utxo_ = blockchain.utxo_;
            }
            return *this;
        }

/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the blockchain matches a 
*              given blockchain.
*
* Arguments:   BlockChain const& blockchain: blockchainobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
        inline bool operator==(BlockChain const& blockchain){
            if (blocks_.size() != blockchain.blocks_.size()){
                return false;
            }
            else {
                for (int i=0; i<blocks_.size(); ++i){
                    if (!(blocks_[i] == blockchain.blocks_[i])){
                        return false;
                    } 
                }
                if (chainLength_ != blockchain.chainLength_){
                    return false;
                }
                else if (!(genesisBlock_ == blockchain.genesisBlock_)){
                    return false;
                }
                else if (currentEpoch_ != blockchain.currentEpoch_){
                    return false;
                }
                else if (indexLastFinalizedBlock_ != blockchain.indexLastFinalizedBlock_){
                    return false;
                }
                else if (!(utxo_ == blockchain.utxo_)){
                    return false;
                }
                else {
                    return true;
                }
            }
        }

/*************************************************
* Name:        getBlocks
*
* Description: Get all the blocks in the blockchain.
*
* Arguments:   None
*
* Returns blocks_
**************************************************/
        inline vector<Block> getBlocks() const {return blocks_;}

/*************************************************
* Name:        getChainLength
*
* Description: Get the length of the blockchain
*              without the genesis block.
*
* Arguments:   None
*
* Returns chainLength
**************************************************/
        inline int getChainLength() const {return chainLength_;}

/*************************************************
* Name:        getCurrentEpoch
*
* Description: Get the epoch number of the last 
*              block in the chain.
*
* Arguments:   None
*
* Returns currentEpoch_
**************************************************/
        inline int getCurrentEpoch() const {return currentEpoch_;}

/*************************************************
* Name:        getIndexLastFinalizedBlock
*
* Description: Get the index of the last block in the
*              blockchain that has been finalized.
*
* Arguments:   None
*
* Returns indexLastFinalizedBlock_
**************************************************/
        inline int getIndexLastFinalizedBlock() const {return indexLastFinalizedBlock_;}

/*************************************************
* Name:        getUTXO
*
* Description: Get the UTXO of the blockchain.
*
* Arguments:   None
*
* Returns utxo_
**************************************************/
        inline UTXO getUTXO() const {return utxo_;}        

/*************************************************
* Name:        addBlockToChain
*
* Description: Function that checks whether a block
*              could be added to the chain and if so
*              adds it.
*
* Arguments:   -Block& block: the block we want to add
*
* Returns nothing
**************************************************/
        inline void addBlockToChain(Block& block){
            if (block.isNotarized() == true and block.getEpoch()> currentEpoch_ and block.getHashedPrefix() == hashPrefix()){
                std::pair<bool,std::string> result = utxo_.update(block);
                if (std::get<bool>(result)){
                    //finalize
                    if (((blocks_[chainLength_]).getEpoch() == block.getEpoch() -1) && (blocks_[chainLength_-1].getEpoch() == block.getEpoch() -2)){
                        indexLastFinalizedBlock_ = chainLength_-1;
                        for (int i=0;i<chainLength_; ++i){
                            blocks_[i].finalize();
                        }
                    }
                    blocks_.push_back(block);
                    currentEpoch_ = block.getEpoch();
                    chainLength_ = chainLength_ + 1;
                }
                else{
                    std::cout << "[BLOCKCHAIN]: Block can not be added to the block chain, since the payload is not valid!" << std::endl;
                    std::cout << "[BLOCKCHAIN]: " << std::get<std::string>(result) << std::endl;
                }
            }
            else{
                std::cout << "[BLOCKCHAIN]: Block can not be added to the block chain, since it is not notarized or it comes from an older epoch or it has an invalid prefix hash!" << std::endl;
            }
        };

/*************************************************
* Name:        couldBeAddedToChain
*
* Description: Function that checks whether a block could
*              be added to a blockchain.
*
* Arguments:   -Block& block: the block we want to add
*              -bool shouldBeNotarized: Boolean that notifies the function
*               whether or not this block should be notarized
*
* Returns true if it can be added to the chain and false if it can not
**************************************************/
        inline bool couldBeAddedToChain( Block block, bool shouldBeNotarized){
            if (block.getSize() > 2000000){
                std::cout << "[BLOCKCHAIN]: The block is too large." << std:: endl;
                return false;
            }
            if (shouldBeNotarized){
                if (block.isNotarized() == false || block.getEpoch() <= getCurrentEpoch() || block.getHashedPrefix() != hashPrefix()){
                    std::cout << "[BLOCKCHAIN]: Block can not be added to the block chain, since the payload is not valid or it has an invalid prefix hash or is not notarized!" << std::endl;
                    return false;
                }
            }
            else{
                if(block.getEpoch() <= getCurrentEpoch() || block.getHashedPrefix() != hashPrefix()){
                    std::cout << "[BLOCKCHAIN]: Block can not be added to the block chain, since the payload is not valid or it has an invalid prefix hash!" << std::endl;
                    return false;
                }
            }
            std::pair<bool, std::string> blockCheck = utxo_.verify(block);
            string reason =std::get<string>(blockCheck);
            std::cout << reason << std::endl;
            return std::get<bool>(blockCheck);
        };

/*************************************************
* Name:        hashPrefix
*
* Description: Function that computes the hash of the last
*              block in the chain.
*
* Arguments:   None
*
* Returns hashedprefix
**************************************************/
        inline string hashPrefix(){
            Block lastBlock = blocks_[chainLength_];
            std::string hashedprefix = lastBlock.hashBlock();
            return hashedprefix;
        };
};
#endif