#include "IO.hpp"
#include "Header.hpp"
#include "Transaction.hpp"
#include "KeyPair.hpp"
#include "Block.hpp"
#include "UTXO.hpp"
#include "BlockChain.hpp"
#include "testing/acutest.h"
#include "toml.hpp"
#include <exception>
#include <stdexcept>
#include <string>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>

// Convert: hexadecimal to ASCII test
void testHexToASCIIConvert(){
    std::string hex1 = "68616c6c6f";
    std::string ascii1 =  hexToASCII(hex1);
    TEST_ASSERT(ascii1 == "hallo");
    std::string hex2 = "23352d202a";
    std::string ascii2 = hexToASCII(hex2);
    TEST_ASSERT(ascii2 == "#5- *");
}

// Convert: ASCII to hexadecimal test
void testASCIIToHexConvert(){
    std::string ascii1 = "hallo";
    std::string hex1 = ASCIIToHex(ascii1);
    TEST_ASSERT(hex1 == "68616c6c6f");
    std::string ascii2 = "#5- *";
    std::string hex2 = ASCIIToHex(ascii2);
    TEST_ASSERT(hex2 == "23352d202a");
}

// Convert: ASCII to decimal test
void testASCIIToDecConvert(){
    std::string ascii1 = "hallo";
    std::string dec1 = ASCIIToDec(ascii1);
    TEST_ASSERT(dec1 == "10497108108111");
    std::string ascii2 = "#5- *";
    std::string dec2 = ASCIIToDec(ascii2);
    TEST_ASSERT(dec2 == "3553453242");
}

// Input: equality operator test
void testEqualityOperatorInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Input input2({}, 0);
    TEST_ASSERT(!(input1==input2));
    Input input3({'h','a','l','l','o'}, 0);
    Input input4({'h','a','l','l', 'i'}, 0);
    TEST_ASSERT(!(input3 == input4));
    Input input5({'h','a','l','l','o'}, 0);
    Input input6({'h','a','l','l','o'}, 1);
    TEST_ASSERT(!(input5 == input6));
    Input input7({'h', 'a', 'l', 'l', 'o'}, 0);
    Input input8({}, 1);
    TEST_ASSERT(!(input7==input8));
    Input input9({'h','e','y'}, 0);
    Input input10({'h','e','y'}, 0);
    TEST_ASSERT(input9 == input10);
}

// Input: reference transaction id getter test
void testTxIdGetterInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    TEST_ASSERT(input1.getTxId()[0] == 'h');
    TEST_ASSERT(input1.getTxId()[1] == 'a');
    TEST_ASSERT(input1.getTxId()[2] == 'l');
    TEST_ASSERT(input1.getTxId()[3] == 'l');
    TEST_ASSERT(input1.getTxId()[4] == 'o');
    Input input2({}, 0);
    TEST_ASSERT((input2.getTxId()).size() == 0);
    Input input3({'H', '2', '#'},0);
    TEST_ASSERT(input3.getTxId()[0] == 'H');
    TEST_ASSERT(input3.getTxId()[1] == '2');
    TEST_ASSERT(input3.getTxId()[2] == '#');
}

// Input: reference output index getter test
void testOutputIndexGetterInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    TEST_ASSERT(input1.getOutputIndex() == 0);
    Input input2({'h', 'a', 'l', 'l', 'o'}, 4);
    TEST_ASSERT(input2.getOutputIndex() == 4);
    Input input3({'h', 'a', 'l', 'l', 'o'}, -1);
    TEST_ASSERT(input3.getOutputIndex() == 0);
}

// Input: make string test
void testMakeStringInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    TEST_ASSERT(input1.inputToString() == "0hallo");
    Input input2({}, 0);
    TEST_ASSERT(input2.inputToString() == "0");
    Input input3({'h', 'a', 'l', 'l', 'o'}, -1);
    TEST_ASSERT(input3.inputToString() == "0hallo");
}

// Input: make string of transaction id test
void testIdToStringInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    TEST_ASSERT(input1.IDToString() == "hallo");
    Input input2({}, 0);
    TEST_ASSERT(input2.IDToString() == "");
    Input input3({'H', '2', '#'},0);
    TEST_ASSERT(input3.IDToString() == "H2#");
}

// Input: size getter test
void testSizeGetterInput(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Input input2({}, 0);
    TEST_ASSERT(input1.getSize() > input2.getSize());
}

// Output: equality operator test
void testEqualityOperatorOutput(){
    Output output1(0, "");
    Output output2(1, "");
    TEST_ASSERT(!(output1 == output2));
    Output output3(0,"");
    Output output4(0, "hdfrfgzrteh");
    TEST_ASSERT(!(output3 == output4));
    Output output5(0, "dkslfreds");
    Output output6(0, "djlksfbdd");
    TEST_ASSERT(!(output5 == output6));
    Output output7(1, "jsdklfsd");
    Output output8(2, "sfdg");
    TEST_ASSERT(!(output7 == output8));
    Output output9(14, "alfa");
    Output output10(14, "alfa");
    TEST_ASSERT(output9 == output10);
}

// Output: value getter test
void testValueGetterOutput(){
    Output output1(0, "");
    TEST_ASSERT(output1.getValue() == 0);
    Output output2(3, "");
    TEST_ASSERT(output2.getValue() == 3);
    Output output3(-8, "");
    TEST_ASSERT(output3.getValue() == 4294967288);
}

// Output: receiver public key test
void testReceiverPkGetterOutput(){
    Output output1(0, "");
    TEST_ASSERT(output1.getReceiverPk() == "");
    Output output2(0, "alfa");
    TEST_ASSERT(output2.getReceiverPk() == "alfa");
    Output output3(0, "ds45/#");
    TEST_ASSERT(output3.getReceiverPk() == "ds45/#");
}

// Output: make string test
void testMakeStringOutput(){
    Output output1(0, "");
    TEST_ASSERT(output1.outputToString() == "0");
    Output output2(-8, "");
    TEST_ASSERT(output2.outputToString() == "4294967288");
    Output output3(4, "");
    TEST_ASSERT(output3.outputToString() == "4");
    Output output4(0, "alfa");
    TEST_ASSERT(output4.outputToString() == "0alfa");
    Output output5(-8, "alfa");
    TEST_ASSERT(output5.outputToString() == "4294967288alfa");
    Output output6(4, "alfa");
    TEST_ASSERT(output6.outputToString() == "4alfa");
    Output output7(0, "a5/#");
    TEST_ASSERT(output7.outputToString() == "0a5/#");
    Output output8(-8, "a5/#");
    TEST_ASSERT(output8.outputToString() == "4294967288a5/#");
    Output output9(4, "a5/#");
    TEST_ASSERT(output9.outputToString() == "4a5/#");
}

// Output: size getter test
void testSizeGetterOutput(){
    Output output1(0, "");
    Output output7(0, "a5/#");
    TEST_ASSERT(output1.getSize() < output7.getSize());
}

// Header: equality operator test
void testEqualityOperatorHeader(){
    Header header1(0,{}, "", "", {});
    Header header2(4,{}, "", "", {});
    TEST_ASSERT(!(header1 == header2));
    Header header3(0,{}, "", "", {});
    Header header4(0,{"alfa"}, "", "", {});
    TEST_ASSERT(!(header3 == header4));
    Header header5(0,{"beat it"}, "", "", {});
    Header header6(0,{"alfa"}, "", "", {});
    TEST_ASSERT(!(header5 == header6));
    Header header7(0,{}, "alfa", "", {});
    Header header8(0,{}, "electric", "", {});
    TEST_ASSERT(!(header7 == header8));
    Header header9(0,{}, "", "alfa", {});
    Header header10(0,{}, "", "electric", {});
    TEST_ASSERT(!(header9 == header10));
    Header header11(0,{}, "", "", {{}});
    Header header12(0,{}, "", "", {});
    TEST_ASSERT(!(header11 == header12));
    Header header13(0,{}, "", "", {{}});
    Header header14(0,{}, "", "", {{'a'}});
    TEST_ASSERT(!(header13 == header14));
    Header header15(0,{}, "", "", {{'a'}});
    Header header16(0,{}, "", "", {{'b'}});
    TEST_ASSERT(!(header15 == header16));
    Header header17(0,{"dell", "asus"}, "alfa", "beat", {{'a', 'b'}, {'e', 'g', 'r'}});
    Header header18(0,{"dell", "asus"}, "alfa", "beat", {{'a', 'b'}, {'e', 'g', 'r'}});
    TEST_ASSERT(header17 == header18);

}

// Header: id getter test
void testIdGetterHeader(){
    Header header1(0,{}, "", "", {});
    TEST_ASSERT(header1.getId() == 0);
    Header header2(4,{}, "", "", {});
    TEST_ASSERT(header2.getId() == 4);
    Header header3(-1,{}, "", "", {});
    TEST_ASSERT(header3.getId() == -1);
}

// Header: validator signatures getter test
void testValidatorSigsGetterHeader(){
    Header header1(0,{}, "", "", {});
    TEST_ASSERT(header1.getValidSigs().size() == 0);
    Header header2(0,{"alfa"}, "", "", {});
    TEST_ASSERT(header2.getValidSigs()[0] ==  "alfa");
    Header header3(0,{"alfa", "a5#*"}, "", "", {});
    TEST_ASSERT(header3.getValidSigs()[0] == "alfa");
    TEST_ASSERT(header3.getValidSigs()[1] == "a5#*");
}

// Header: previous block digest getter test
void testPrevBlockDigestGetterHeader(){
    Header header1(0,{}, "", "", {});
    TEST_ASSERT(header1.getHashedPrefix() == "");
    Header header2(0,{}, "alfa", "", {});
    TEST_ASSERT(header2.getHashedPrefix() == "alfa");
    Header header3(0,{}, "a5#* /", "", {});
    TEST_ASSERT(header3.getHashedPrefix() == "a5#* /");
}

// Header: merkel root tree getter test
void testMerkelRootGetterHeader(){
    Header header1(0,{}, "", "", {});
    TEST_ASSERT(header1.getMerkelRoot() == "");
    Header header2(0,{}, "", "alfa", {});
    TEST_ASSERT(header2.getMerkelRoot() == "alfa");
    Header header3(0,{}, "", "a5#* /", {});
    TEST_ASSERT(header3.getMerkelRoot() == "a5#* /");
}

// Header: transaction ids getter test
void testTxIdsGetterHeader(){
    Header header1(0,{}, "", "", {});
    TEST_ASSERT(header1.getTxIds().size() == 0);
    Header header2(0,{}, "", "", {{}});
    TEST_ASSERT(header2.getTxIds()[0].size() == 0);
    Header header3(0,{}, "", "", {{'a', '#', '5'}});
    TEST_ASSERT(header3.getTxIds().size() == 1);
    TEST_ASSERT(header3.getTxIds()[0][0] == 'a');
    TEST_ASSERT(header3.getTxIds()[0][1] == '#');
    TEST_ASSERT(header3.getTxIds()[0][2] ==  '5');
}

// Header: add signature test
void testAddSignatureHeader(){
    Header header1(0,{}, "", "", {});
    header1.addSignature("");
    TEST_ASSERT(header1.getValidSigs()[0] == "");
    header1.addSignature("alfa");
    TEST_ASSERT(header1.getValidSigs()[1] == "alfa");
    header1.addSignature("a5/ #");
    TEST_ASSERT(header1.getValidSigs()[2] == "a5/ #");
}

// Header: size getter test
void testSizeGetterHeader(){
    Header header1(0,{}, "", "", {});
    Header header2(0,{"dell", "asus"}, "alfa", "beat", {{'a', 'b'}, {'e', 'g', 'r'}});
    TEST_ASSERT(header1.getSize() < header2.getSize());
}

// Transaction: equality operator test
void testEqualityOperatorTransaction(){
    Transaction transaction1({}, {},"", {});
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Transaction transaction2({input1}, {},"", {});
    TEST_ASSERT(!(transaction1 == transaction2));
    Input input2({'h'}, 1);
    Transaction transaction3({input2}, {},"", {});
    TEST_ASSERT(!(transaction2 == transaction3));
    Output output1(14, "alfa");
    Transaction transaction4({}, {output1},"", {});
    TEST_ASSERT(!(transaction1 == transaction4));
    Output output2(12, "beta");
    Transaction transaction5({}, {output2},"", {});
    TEST_ASSERT(!(transaction4 == transaction5));
    Transaction transaction6({}, {},"alfa", {});
    TEST_ASSERT(!(transaction1 == transaction6));
    Transaction transaction7({}, {},"", {'a'});
    TEST_ASSERT(!(transaction1 == transaction7));
    Transaction transaction8({}, {},"", {'b'});
    TEST_ASSERT(!(transaction8 == transaction7));
    Transaction transaction9({input1}, {output2}, "alfa",{'h','e'});
    Transaction transaction10({input1}, {output2}, "alfa",{'h','e'});
    TEST_ASSERT(transaction9 == transaction10);
}

// Transaction: inputs getter test
void testInputsGetterTransaction(){
    Transaction transaction1({}, {},"", {});
    TEST_ASSERT(transaction1.getInputs().size() == 0);
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Transaction transaction2({input1}, {},"", {});
    TEST_ASSERT(transaction2.getInputs()[0] == input1);
    Input input2({'h'}, 4);
    Transaction transaction3({input1, input2}, {},"", {});
    TEST_ASSERT(transaction3.getInputs()[1] == input2);
}

// Transaction: outputs getter test
void testOutputsGetterTransaction(){
    Transaction transaction1({}, {},"", {});
    TEST_ASSERT(transaction1.getOutputs().size() == 0);
    Output output1(14, "alfa");
    Transaction transaction2({},{output1}, "",{});
    TEST_ASSERT(transaction2.getOutputs()[0] == output1);
    Output output2(12, "haha");
    Transaction transaction3({},{output1, output2}, "",{});
    TEST_ASSERT(transaction3.getOutputs()[1] == output2);
}

// Transaction: sender signature getter test
void testSenderSigGetterTransaction(){
    Transaction transaction1({}, {},"", {});
    TEST_ASSERT(transaction1.getSenderSig() == "");
    Transaction transaction2({}, {},"alfa", {});
    TEST_ASSERT(transaction2.getSenderSig() == "alfa");
    Transaction transaction3({}, {},"a5/ #", {});
    TEST_ASSERT(transaction3.getSenderSig() == "a5/ #");
}

// Transaction: transaction id getter test
void testTxIdGetterTransaction(){
    Transaction transaction1({}, {},"", {});
    TEST_ASSERT(transaction1.getId().size() == 0);
    Transaction transaction2({}, {},"", {'a', 'l'});
    TEST_ASSERT(transaction2.getId()[0] == 'a');
    TEST_ASSERT(transaction2.getId()[1] == 'l');
    Transaction transaction3({}, {},"", {'a','5','#', ' '});
    TEST_ASSERT(transaction3.getId()[0] == 'a');
    TEST_ASSERT(transaction3.getId()[1] == '5');
    TEST_ASSERT(transaction3.getId()[2] == '#');
    TEST_ASSERT(transaction3.getId()[3] == ' ');
}

// Transaction: sender signature setter test
void testSenderSigSetterTransaction(){
    Transaction transaction1({}, {},"", {});
    transaction1.setSenderSig("alfa");
    TEST_ASSERT(transaction1.getSenderSig() == "alfa");
    transaction1.setSenderSig("   ");
    TEST_ASSERT(transaction1.getSenderSig() == "   ");
    transaction1.setSenderSig("#/*-");
    TEST_ASSERT(transaction1.getSenderSig() == "#/*-");
}

// Transaction: hash test
void testHashTransaction(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Output output2(14, "alfa");
    Transaction transaction1({}, {},"", {});
    Transaction transaction2({input1}, {output2}, "alfa",{'h','e'});
    TEST_ASSERT(transaction2.hashTransaction() != transaction1.hashTransaction());
    Transaction transaction3({input1}, {output2}, "alfa",{'h','e'});
    TEST_ASSERT(transaction2.hashTransaction() ==  transaction3.hashTransaction());
}

// Transaction: id to string transformer test
void testIdToStringTransaction(){
    Transaction transaction1({}, {},"", {});
    TEST_ASSERT(transaction1.IdToString() == "");
    Transaction transaction2({}, {},"", {'a', 'l'});
    TEST_ASSERT(transaction2.IdToString() == "al");
    Transaction transaction3({}, {},"", {'a','5','#', ' '});
    TEST_ASSERT(transaction3.IdToString() == "a5# ");
}

// Transaction: size getter test
void testSizeGetterTransaction(){
    Input input1({'h', 'a', 'l', 'l', 'o'}, 0);
    Output output2(14, "alfa");
    Transaction transaction1({}, {},"", {});
    Transaction transaction2({input1}, {output2}, "alfa",{'h','e'});
    TEST_ASSERT(transaction1.getSize() < transaction2.getSize());
}

// KeyPair: public key string getter test
void testPublicKeyGetterKeyPair(){
    KeyPair keypair1("admin");
    keypair1.generateKeyPair("admin");
    TEST_ASSERT(keypair1.getPublicKey().length() != 0);
}

// KeyPair: hashed password getter test
void testHashPasswordGetterKeyPair(){
    KeyPair keypair1("admin");
    KeyPair keypair2("alfa");
    TEST_ASSERT(keypair1.getHashedPasswd() != keypair2.getHashedPasswd());
    KeyPair keypair3("alfa");
    TEST_ASSERT(keypair2.getHashedPasswd() == keypair3.getHashedPasswd());
}

// KeyPair: generate key pairs test
void testGenerateKeyPair(){
    KeyPair keypair1("admin");
    keypair1.generateKeyPair("admin");
    KeyPair keypair2("alfa");
    keypair2.generateKeyPair("alfa");
    TEST_ASSERT(keypair2.getPublicKey() != keypair1.getPublicKey());
}

// KeyPair: creating and verifying signatures test
void testVerifyAndCreateKeyPair(){
    std::string message = ASCIIToHex("hallo, ik ben Linde");
    std::string message2 = ASCIIToHex("dit mag niet werken");
    KeyPair keypair1("admin");
    keypair1.generateKeyPair("admin");
    std::string signature = keypair1.createSignature(message, "admin");
    TEST_ASSERT(verifySignature(signature, message, keypair1.getPublicKey()) == 1);
    KeyPair keypair2("admin");
    keypair2.generateKeyPair("admin");
    TEST_ASSERT(verifySignature(signature, message, keypair2.getPublicKey()) == 0);
    TEST_ASSERT(verifySignature(signature, message2, keypair1.getPublicKey()) == 0);
} 

// Block: equality operator test
void testEqualityOperatorBlock(){
    Block block1("",0,{});
    Block block2("",4,{});
    TEST_ASSERT(!(block1 == block2));
    Block block3("help",0,{});
    TEST_ASSERT(!(block1 == block3));
    Transaction transaction1({},{},"", {});
    Block block4("",0,{transaction1});
    TEST_ASSERT(!(block1 == block4));
    Transaction transaction2({},{}, "alfa", {});
    Block block5("", 0,{transaction2});
    TEST_ASSERT(!(block4 == block5));
    Block block6("",0,{});
    block6.notarize(0);
    TEST_ASSERT(!(block1 == block6));
    Block block7("",0,{});
    KeyPair keypair1("ADMIN");
    keypair1.generateKeyPair("ADMIN");
    std::string message = block7.hashBlock();
    std::string signature = keypair1.createSignature(message, "ADMIN");
    block7.addSignatureToBlock(keypair1.getPublicKey(),signature,4);
    TEST_ASSERT(!(block1 == block7));
    Block block8("",0,{});
    block8.finalize();
    TEST_ASSERT(!(block1 == block8));
}

// Block: epoch getter test
void testEpochGetterBlock(){
    Block block1("",0,{});
    TEST_ASSERT(block1.getEpoch() == 0);
    Block block2("",4,{});
    TEST_ASSERT(block2.getEpoch() == 4);
    Block block3("",-1,{});
    TEST_ASSERT(block3.getEpoch() == -1);
}

// Block: previous hash getter test
void testPreviousHashGetterBlock(){
    Block block1("", 0,{});
    TEST_ASSERT(block1.getHashedPrefix() == "");
    Block block2("alfa", 0, {});
    TEST_ASSERT(block2.getHashedPrefix() == "alfa");
    Block block3("a5/ #",0,{});
    TEST_ASSERT(block3.getHashedPrefix() == "a5/ #");
}

// Block: payload getter test
void testPayloadGetterBlock(){
    Block block1("",0, {});
    TEST_ASSERT(block1.getPayload().size() == 0);
    Transaction transaction1({},{}, "alfa", {});
    Block block2("",0,{transaction1});
    TEST_ASSERT(block2.getPayload()[0] == transaction1);
    Transaction transaction2({},{}, "beat", {});
    Block block3("",0,{transaction1, transaction2});
    TEST_ASSERT(block3.getPayload()[0] == transaction1);
    TEST_ASSERT(block3.getPayload()[1] == transaction2);
}

// Block: notarized test
void testNotarizeBlock(){
    Block block1("", 0,{});
    block1.notarize(4);
    TEST_ASSERT(block1.isNotarized() == 0);
    block1.notarize(0);
    TEST_ASSERT(block1.isNotarized() == 1);
}

// Block: number of signatures getter test
void testNbSignaturesGetterBlock(){
    Block block1("", 0,{});
    TEST_ASSERT(block1.getNbSignatures() == 0);
    KeyPair keypair1("ADMIN");
    keypair1.generateKeyPair("ADMIN");
    std::string message = block1.hashBlock();
    std::string signature = keypair1.createSignature(message, "ADMIN");
    block1.addSignatureToBlock(keypair1.getPublicKey(),signature, 4);
    TEST_ASSERT(block1.getNbSignatures() == 1);
}

// Block: finalize test
void testFinalizeBlock(){
    Block block1("",0,{});
    TEST_ASSERT(block1.isFinalized() == 0);
    block1.finalize();
    TEST_ASSERT(block1.isFinalized() == 1);
}

// Block: header getter test
void testHeaderGetterBlock(){
    Block block1("", 0,{});
    std::vector<std::vector<unsigned char>> txids = {};
    for (int i=0; i<block1.getPayload().size(); ++i){
        txids.push_back(block1.getPayload()[i].getId());
    }
    std::string merkelRootHash = block1.CreateMerkelRoot();
    Header header1(0,{}, "", merkelRootHash, txids);
    TEST_ASSERT(block1.getHeader() == header1);
}

// Block: create merkel root test
void testCreateMerkelRootBlock(){
    Block block1("",0, {});
    Block block2("", 0, {});
    TEST_ASSERT(block1.CreateMerkelRoot() == block2.CreateMerkelRoot());
    Input input1({'h'}, 0);
    Output output1(14, "hallo");
    Transaction transaction1({input1}, {output1}, "alfa", {'h', 'a', 'l'});
    Block block3("alfa", 0,{transaction1});
    TEST_ASSERT(block1.CreateMerkelRoot() != block3.CreateMerkelRoot());
    Block block4("alfa", 0, {transaction1});
    TEST_ASSERT(block3.CreateMerkelRoot()  == block4.CreateMerkelRoot());
    Transaction transaction2({},{}, "alfa", {});
    Block block5("amfa", 2, {transaction1, transaction2});
    TEST_ASSERT(block4.CreateMerkelRoot() != block5.CreateMerkelRoot());
}

// Block: transaction in block test
void testTransactionInBlock(){
    Block block1("",0, {});
    Input input1({'h'}, 0);
    Output output1(14, "hallo");
    Transaction transaction1({input1}, {output1}, "alfa", {'h', 'a', 'l'});
    Block block2("alfa", 0,{transaction1});
    TEST_ASSERT(block1.transactionInBlock(transaction1) == 0);
    TEST_ASSERT(block2.transactionInBlock(transaction1) == 1);
}

// Block: hash block test
void testHashBlock(){
    Block block1("",0, {});
    Block block2("", 0, {});
    TEST_ASSERT(block1.hashBlock() == block2.hashBlock());
    Input input1({'h'}, 0);
    Output output1(14, "hallo");
    Transaction transaction1({input1}, {output1}, "alfa", {'h', 'a', 'l'});
    Block block3("alfa", 0,{transaction1});
    TEST_ASSERT(block1.hashBlock() != block3.hashBlock());
    Block block4("alfa", 0, {transaction1});
    TEST_ASSERT(block3.hashBlock()  == block4.hashBlock());
    Transaction transaction2({},{}, "alfa", {});
    Block block5("amfa", 2, {transaction1, transaction2});
    TEST_ASSERT(block4.hashBlock() != block5.hashBlock());
}

// Block: size getter test
void testSizeGetterBlock(){
    Block block1("",0,{});
    Input input1({'h'}, 0);
    Output output1(14, "hallo");
    Transaction transaction1({input1}, {output1}, "alfa", {'h', 'a', 'l'});
    Block block2("",0, {transaction1});
    TEST_ASSERT(block1.getSize() < block2.getSize());
}

// BlockChain: equality operator test
void testEqualityOperatorBlockChain(){
    BlockChain blockchain1(4);
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;
    //make genesis block
    Block genesisBlock = createGenesisBlock(defaultWallets);
    BlockChain blockchain2(2, genesisBlock,4);
    TEST_ASSERT(!(blockchain1 == blockchain2));
    BlockChain blockchain3(0,genesisBlock, 2);
    TEST_ASSERT(!(blockchain1 == blockchain3));
    BlockChain blockchain4(4);
    Block block1(blockchain4.hashPrefix(),1, {});
    block1.notarize(0);
    blockchain4.addBlockToChain(block1);
    TEST_ASSERT(!(blockchain4 == blockchain1));
    BlockChain blockchain5(4);
    TEST_ASSERT(blockchain1 == blockchain5);
}

// BlockChain: blocks getter test
void testBlocksGetterBlockChain(){
    BlockChain blockchain1(4);
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;
    Block block1 = createGenesisBlock(defaultWallets);
    TEST_ASSERT(blockchain1.getBlocks()[0] == block1);
    Block block2(blockchain1.hashPrefix(),1, {});
    block2.notarize(0);
    blockchain1.addBlockToChain(block2);
    TEST_ASSERT(blockchain1.getBlocks()[1] == block2);
}

// BlockChain: chainlength getter test
void testChainLengthGetterBlockChain(){
    BlockChain blockchain1(4);
    TEST_ASSERT(blockchain1.getChainLength() == 0);
    Block block2(blockchain1.hashPrefix(),1, {});
    block2.notarize(0);
    blockchain1.addBlockToChain(block2);
    TEST_ASSERT(blockchain1.getChainLength() == 1);
}

// BlockChain: current epoch getter test
void testCurrentEpochGetterBlockChain(){
    BlockChain blockchain1(4);
    TEST_ASSERT(blockchain1.getCurrentEpoch() == 0);
    Block block2(blockchain1.hashPrefix(),1, {});
    block2.notarize(0);
    blockchain1.addBlockToChain(block2);
    TEST_ASSERT(blockchain1.getCurrentEpoch() == 1);
}

// BlockChain: index last finalized block getter test
void testIndexLastFinalizedBlockGetterBlockChain(){
    BlockChain blockchain1(4);
    TEST_ASSERT(blockchain1.getIndexLastFinalizedBlock() == 0);
    Block block1(blockchain1.hashPrefix(), 1, {});
    block1.notarize(0);
    blockchain1.addBlockToChain(block1);
    TEST_ASSERT(blockchain1.getIndexLastFinalizedBlock() == 0);
    Block block2(blockchain1.hashPrefix(), 2, {});
    block2.notarize(0);
    blockchain1.addBlockToChain(block2);
    TEST_ASSERT(blockchain1.getIndexLastFinalizedBlock() == 0);
    Block block3(blockchain1.hashPrefix(), 3, {});
    block3.notarize(0);
    blockchain1.addBlockToChain(block3);
    TEST_ASSERT(blockchain1.getIndexLastFinalizedBlock() == 1);
}

// BlockChain: UTXO getter test
void testUTXOGetterBlockChain(){
    BlockChain blockchain1(4);
    UTXO utxo1(4);
    TEST_ASSERT(blockchain1.getUTXO().getUTXOId() == 4);
}

// BlockChain: add block test
void testAddBlockBlockChain(){
    BlockChain blockchain1(4);
    Block block1(blockchain1.hashPrefix(), 1, {});
    blockchain1.addBlockToChain(block1);
    TEST_ASSERT(blockchain1.getChainLength() == 0);
    Block block2(blockchain1.hashPrefix(), 0, {});
    block2.notarize(0);
    blockchain1.addBlockToChain(block2);
    TEST_ASSERT(blockchain1.getChainLength() == 0);
    Block block3("alfsdgeqd", 1,{});
    block3.notarize(0);
    blockchain1.addBlockToChain(block3);
    TEST_ASSERT(blockchain1.getChainLength() == 0);
    block1.notarize(0);
    blockchain1.addBlockToChain(block1);
    TEST_ASSERT(blockchain1.getChainLength() == 1);
}

// BlockChain: could be added test
void testCouldBeAddedBlockChain(){
    BlockChain blockchain1(4);
    Block block1(blockchain1.hashPrefix(), 1, {});
    TEST_ASSERT(blockchain1.couldBeAddedToChain(block1,true) == 0);
    Block block2(blockchain1.hashPrefix(), 0, {});
    block2.notarize(0);
    TEST_ASSERT(blockchain1.couldBeAddedToChain(block2, true) == 0);
    Block block3("alfsdgeqd", 1,{});
    block3.notarize(0);
    TEST_ASSERT(blockchain1.couldBeAddedToChain(block3, true) == 0);
    block1.notarize(0);
    TEST_ASSERT(blockchain1.couldBeAddedToChain(block1, true) == 1);
}

// BlockChain: hash prefix test
void testHashPrefixBlockChain(){
    BlockChain blockchain1(4);
    Block block1(blockchain1.hashPrefix(), 1,{});
    block1.notarize(0);
    blockchain1.addBlockToChain(block1);
    TEST_ASSERT(block1.hashBlock() == blockchain1.hashPrefix());
    Block block2("ndfagr",1,{});
    TEST_ASSERT(block2.hashBlock() != blockchain1.hashPrefix());
}

// UTXO: type definitions
        typedef std::pair<std::string, unsigned int> previousTransaction;
        typedef std::map<previousTransaction, unsigned int> unspendOutputs;
        typedef std::map<std::string, unspendOutputs> Records;

// UTXO: constructor without input test
void testConstructorNoInputUTXO(){
    UTXO utxo;
    std::string developerPublicKey = "ba72272ad32b0795375d5574c59aa4f3adba6c52fe16ec7e0a278e031bf9b5b72b3ed113676b92bc985a13e3087860bb062449a29abe2391aea4f0110f06dfa0b44802a0057e67b709303ae4568c18c0efaef83f1122c893899e76db9eeacc44379530457493689b1cd0bf20a79da919af6f39218bed5d81bf3fb6c0c244999b9c0181a1387d89a0013eb55c664e955f6e7d1721e9bb0e507e9ce8e76205518f9785673d28c260ebab32c2afb4a7ccabb4dd55cded0daae4a00ec115fdac11b4f7f118e949313eb4cd79961d8b4f7bb79feca3d88d2fbc3ce812c618ff0e5b6908250d6f34303f099fcca3a083463ddd98fce2e2ea8a9ba0830281d3eac74fb994e5c4356ff1232489e274de5bf61ffef662ce0ae362144145a4b63f23d39353c6860ec840e12d787277f754320934adfc7eec78f1eec821a62b6b08e79a55113faad7ef7002fdebb478c1c04d97ae070c97d2d4c0b4c43ba0037ef387c05899a60f8698eccd0221a15e6a1d3ee78997809860a7b016d5bd522768af207aaf722f517a46063712e5af74e8013a3fe7701411cbb5bafb7a0242140a6c671d16fbd0d2cb4ed2e07e7762cf45fe2aea53065488a4139bd1dfd8e49736d9ca01e88531689a65fa3c5bec6d272533b646dfd050ec04e7049d13a65176b053c7f98b1c07fe153b135a069ecedca56c42b97e67fb2004be7ac7f9f4151fb2a5ca448babe0c6aa5a5d697601339787e382a0710c9f81ab36b8e13874f6583a2d75c21c66f43e60ea3c4712ab7336329b575646c8fd6e203f81caf54515c9bdd02da428b5cb9d5773927dc3e140bb70e4db780473525407d12579e0d46e213d3a7dedc6da55f379e1586f03424a0d115694e09f29b7eb6a4821f37f147e5b36750b0fcc6b999c79b8aebdd494e85065645e854650ee0066cb7d22f052727223c7c62c37d91518d587879d2c5c830391860baafa7ddae2b004841ab3b1bed651a408807ecb751d5d965b637bc418144a1813ae17f2495479e2810e48c7d64684596b77d51d38f6edb3563acd65b6dc09d7d291342c3ed603b022fb4229488f2a67fde683f0291bcece88e9ef4e2c95ebdd7f5651bd209a0ebcd29f1b14e2c9a2621021dbae28468fa51a753768e9d69779cfa378d3c4777eded6c83d4d660e43dfefaf28bd6ded9889d6df8e4761afb6acbb443ace316fbfa66c3236504d17eceda0f47c42ce7766e81b041b5bd387f953e4a50ffecf7d7d002630e8e8ed40198feb18955172667d8d3816c1b46fa1b49094543b5e287db1ca9c80b79400ecaf9ad541ccbc5c3de0297be186a8be8255972d90172c101cbb065744394ff9eae37b0af6c9322528d8c5f1d1cbc84a46f7f0601f20300d5621e54b5a721d5b120b7ac3deb88c18c787f9918f4819086239708677318a539fc2d9bab753f4ae0c3812a7a486e5433c7f07ad7422d0793d87747d3260a8a3a5dbe1b9c3e37e7b1deed35763a4144dd23c315f6ab288b7c5cffa4d847369747f3aa82129dbbcde17b7d62c77beddfe081e663224c8722485c618027899122b49a2163bd50428162aef34bc09cfa0c3ce140591306bba7c140fff6def1cd19c609a9e0a49791aaf44510c7a6eb42f1025ac7d8add18f074d57b76b21c2f915939e28274b1471f5a702123f3709b24e80c25cbceae0268348f0cab69e38cbc6ebf0af938ad258c2c0d835776f78051bf62f7925aecef242d657de28423fb08a670a8791f7d8dce9cdd73af11d7280818bf6b9d18f1ecdc111f51e921a8fbfa45d5a32cca0092e2def5ebd87190d937c2999209d4a2a3a8aaad4995b142bef77592b8288f0fbc3b587fbb8990f24815e06aa16edb5a47e3faaa9753e5c21aa5af93a0d1a7bc4f3ba39cdeae9509e25ed15040e8c7dbd072884b4433812326041d27466a1c9553de97b9d512df5421f3d845c369041c51d04d508a227eab25728a6898d668570c93a0a513f329af822c38cdd8dc39d1d737e2127be0455f01fc12f5ef55fcc3ecb1996116ff4e4ede4109eb5ef0c7336d08c4b5711eec4457f019f1f52d5ade05aa6db350d914a734d1741efabae8e9995b502db2f24ff117e350b819ad511ce0b1e1975711f819701ed64900c1fc1b3f6c5691dbb7f1ee61c98cf632480d7f17fbbf7f1f8206e6af387109290a2e0498fc732445c9c0935e0c99e0b6de501f6f3a05ee4636d5e0b3baa67c1a225efefef3605df46d9def090c68c0dbd720dfe4cb249f535e948a4304cf76715dac8bd35ae80b8e7708327f01cf3aeefc131fcf5ea1ef9dbb6884d62c9e2adee04dd08dd1126cfaae9f7b089cec1cc6e0e424f335a404b5fa19875385c84b4a68b95ef69b845a140a2657d8d59aa0a632412ea39ed4563d13b95b62025433668e1ff906d3d68c4b427a5317d8bed45120dc3fe406b8a4f7a1e95a235824b3135b98e954b5dffdfc3044c2fbc1c6d2697c0dd748749bae4fbd7237e5fe3bb92aa1ca9e52070a35a0248e9fd4ca05c309de30f57d1e4bb86212aadcca4121024859220a1a49a4bd6f5673f2c5ae8f08f9222bb0155953681f6a861d2abd43039c1781f364916bfd5eb704b5954fddeb8480b561ba853456bbef6ea86e824e34ad5e3070743a9e15a28dbf48ebe7959d9e410d8bb4c73cbe3c7e9f201ad225a2fe485911fc5d2dafd8d7e56b65e972ae00edb57350e22d4e16c7763774ce3abff422b7b209a7c7ffeb9c52c256a3ea3abf575a2a1cfa113c3e5f9b27eeebba8d78b6e6f98f63582c4c2b900032fbff5c00ce4a1439b4767ffd1dcad1ec5630ce6b016eedf20e3da39025540815e342ffda156b71e033a41f0355f4f3bf0b5d9ab8c799e11244486f285e1d4677d58832bd94816b288ea27e32f34d0df4e349372f80918db9cf10306674ee9885ae6d2b028e34968b1e36097977da67380f378d604959849c09685f0aebfa4da5bd41a7334ea0939a006f04438f18b016cb8f248c110e6eb7a072fbd51c8175e62d120ca770c9b052c8c0f79579cba000a9aba970610f21884402c7d71a8ee3e677c624afe3a2dbfb04c1b0eaf18bdaa096de053270085347f1fe459de82c7c0be6478bff0683269917d4549ccec7e9aad4bd2abeeaf7df89ace2df2b689c66dea210ed6d41490c3226df791a7bae99406b1e43991fce723038197841f3fa9cdb22a291e5f37bfbc86cdc63a6c86680f67fd88de8f16fb7c4dea3f857cbc4babe5ba7e8467ab4a06f260b814d0be09ed4a9da51fd92ccbd105b9ab5e92746241544319a0290874bc1fdee450461ee0d852b35807138ed4f9e72f1951ed1a4d4fa4239e7d94720f834f0dbc388d492a1175c763c864443a05ed8572a8e45c31e03b9caca922d4f4cf20980683e7ed57881626137f2e527121c7598b2851ca9bf534abd5c2ce7f574f3496128fa7486eb327a8c7cf852fd659ac100d7da82532ec88165963e080d399a5dc8aa033dbf694a93a5539b51a7b991551f6a7a531d1090baa393088669c3a7017e2db2a59515cd4b6f3f4c76964e6643b8c1e10292383d6dc04a7a3562a5209e785d224bb9e3cf3deb12e562109994cca304e6813954a5332c090a6c10c6b7d7c2a624efbeb40512fde9f1a3df53342f310fbb94ae55b02c60fc08e4296fd453d2a99ba4d5018b73";
    std::pair<bool, unspendOutputs> developerRecord = utxo.getRecord(developerPublicKey);
    previousTransaction pt("x", 0);
    TEST_ASSERT(std::get<bool>(developerRecord) == true);
    TEST_ASSERT(std::get<unspendOutputs>(developerRecord).size() == 1);
    TEST_ASSERT((std::get<unspendOutputs>(developerRecord)).operator[](pt) == 100000);
    TEST_ASSERT(utxo.getRecords().size() == 1);
    TEST_ASSERT(utxo.getEpoch() == -1);
}

// UTXO: constructor with input test
void testConstructorInputUTXO(){
    UTXO utxo(50);
    TEST_ASSERT(utxo.getUTXOId() == 50);
    std::string developerPublicKey = "ba72272ad32b0795375d5574c59aa4f3adba6c52fe16ec7e0a278e031bf9b5b72b3ed113676b92bc985a13e3087860bb062449a29abe2391aea4f0110f06dfa0b44802a0057e67b709303ae4568c18c0efaef83f1122c893899e76db9eeacc44379530457493689b1cd0bf20a79da919af6f39218bed5d81bf3fb6c0c244999b9c0181a1387d89a0013eb55c664e955f6e7d1721e9bb0e507e9ce8e76205518f9785673d28c260ebab32c2afb4a7ccabb4dd55cded0daae4a00ec115fdac11b4f7f118e949313eb4cd79961d8b4f7bb79feca3d88d2fbc3ce812c618ff0e5b6908250d6f34303f099fcca3a083463ddd98fce2e2ea8a9ba0830281d3eac74fb994e5c4356ff1232489e274de5bf61ffef662ce0ae362144145a4b63f23d39353c6860ec840e12d787277f754320934adfc7eec78f1eec821a62b6b08e79a55113faad7ef7002fdebb478c1c04d97ae070c97d2d4c0b4c43ba0037ef387c05899a60f8698eccd0221a15e6a1d3ee78997809860a7b016d5bd522768af207aaf722f517a46063712e5af74e8013a3fe7701411cbb5bafb7a0242140a6c671d16fbd0d2cb4ed2e07e7762cf45fe2aea53065488a4139bd1dfd8e49736d9ca01e88531689a65fa3c5bec6d272533b646dfd050ec04e7049d13a65176b053c7f98b1c07fe153b135a069ecedca56c42b97e67fb2004be7ac7f9f4151fb2a5ca448babe0c6aa5a5d697601339787e382a0710c9f81ab36b8e13874f6583a2d75c21c66f43e60ea3c4712ab7336329b575646c8fd6e203f81caf54515c9bdd02da428b5cb9d5773927dc3e140bb70e4db780473525407d12579e0d46e213d3a7dedc6da55f379e1586f03424a0d115694e09f29b7eb6a4821f37f147e5b36750b0fcc6b999c79b8aebdd494e85065645e854650ee0066cb7d22f052727223c7c62c37d91518d587879d2c5c830391860baafa7ddae2b004841ab3b1bed651a408807ecb751d5d965b637bc418144a1813ae17f2495479e2810e48c7d64684596b77d51d38f6edb3563acd65b6dc09d7d291342c3ed603b022fb4229488f2a67fde683f0291bcece88e9ef4e2c95ebdd7f5651bd209a0ebcd29f1b14e2c9a2621021dbae28468fa51a753768e9d69779cfa378d3c4777eded6c83d4d660e43dfefaf28bd6ded9889d6df8e4761afb6acbb443ace316fbfa66c3236504d17eceda0f47c42ce7766e81b041b5bd387f953e4a50ffecf7d7d002630e8e8ed40198feb18955172667d8d3816c1b46fa1b49094543b5e287db1ca9c80b79400ecaf9ad541ccbc5c3de0297be186a8be8255972d90172c101cbb065744394ff9eae37b0af6c9322528d8c5f1d1cbc84a46f7f0601f20300d5621e54b5a721d5b120b7ac3deb88c18c787f9918f4819086239708677318a539fc2d9bab753f4ae0c3812a7a486e5433c7f07ad7422d0793d87747d3260a8a3a5dbe1b9c3e37e7b1deed35763a4144dd23c315f6ab288b7c5cffa4d847369747f3aa82129dbbcde17b7d62c77beddfe081e663224c8722485c618027899122b49a2163bd50428162aef34bc09cfa0c3ce140591306bba7c140fff6def1cd19c609a9e0a49791aaf44510c7a6eb42f1025ac7d8add18f074d57b76b21c2f915939e28274b1471f5a702123f3709b24e80c25cbceae0268348f0cab69e38cbc6ebf0af938ad258c2c0d835776f78051bf62f7925aecef242d657de28423fb08a670a8791f7d8dce9cdd73af11d7280818bf6b9d18f1ecdc111f51e921a8fbfa45d5a32cca0092e2def5ebd87190d937c2999209d4a2a3a8aaad4995b142bef77592b8288f0fbc3b587fbb8990f24815e06aa16edb5a47e3faaa9753e5c21aa5af93a0d1a7bc4f3ba39cdeae9509e25ed15040e8c7dbd072884b4433812326041d27466a1c9553de97b9d512df5421f3d845c369041c51d04d508a227eab25728a6898d668570c93a0a513f329af822c38cdd8dc39d1d737e2127be0455f01fc12f5ef55fcc3ecb1996116ff4e4ede4109eb5ef0c7336d08c4b5711eec4457f019f1f52d5ade05aa6db350d914a734d1741efabae8e9995b502db2f24ff117e350b819ad511ce0b1e1975711f819701ed64900c1fc1b3f6c5691dbb7f1ee61c98cf632480d7f17fbbf7f1f8206e6af387109290a2e0498fc732445c9c0935e0c99e0b6de501f6f3a05ee4636d5e0b3baa67c1a225efefef3605df46d9def090c68c0dbd720dfe4cb249f535e948a4304cf76715dac8bd35ae80b8e7708327f01cf3aeefc131fcf5ea1ef9dbb6884d62c9e2adee04dd08dd1126cfaae9f7b089cec1cc6e0e424f335a404b5fa19875385c84b4a68b95ef69b845a140a2657d8d59aa0a632412ea39ed4563d13b95b62025433668e1ff906d3d68c4b427a5317d8bed45120dc3fe406b8a4f7a1e95a235824b3135b98e954b5dffdfc3044c2fbc1c6d2697c0dd748749bae4fbd7237e5fe3bb92aa1ca9e52070a35a0248e9fd4ca05c309de30f57d1e4bb86212aadcca4121024859220a1a49a4bd6f5673f2c5ae8f08f9222bb0155953681f6a861d2abd43039c1781f364916bfd5eb704b5954fddeb8480b561ba853456bbef6ea86e824e34ad5e3070743a9e15a28dbf48ebe7959d9e410d8bb4c73cbe3c7e9f201ad225a2fe485911fc5d2dafd8d7e56b65e972ae00edb57350e22d4e16c7763774ce3abff422b7b209a7c7ffeb9c52c256a3ea3abf575a2a1cfa113c3e5f9b27eeebba8d78b6e6f98f63582c4c2b900032fbff5c00ce4a1439b4767ffd1dcad1ec5630ce6b016eedf20e3da39025540815e342ffda156b71e033a41f0355f4f3bf0b5d9ab8c799e11244486f285e1d4677d58832bd94816b288ea27e32f34d0df4e349372f80918db9cf10306674ee9885ae6d2b028e34968b1e36097977da67380f378d604959849c09685f0aebfa4da5bd41a7334ea0939a006f04438f18b016cb8f248c110e6eb7a072fbd51c8175e62d120ca770c9b052c8c0f79579cba000a9aba970610f21884402c7d71a8ee3e677c624afe3a2dbfb04c1b0eaf18bdaa096de053270085347f1fe459de82c7c0be6478bff0683269917d4549ccec7e9aad4bd2abeeaf7df89ace2df2b689c66dea210ed6d41490c3226df791a7bae99406b1e43991fce723038197841f3fa9cdb22a291e5f37bfbc86cdc63a6c86680f67fd88de8f16fb7c4dea3f857cbc4babe5ba7e8467ab4a06f260b814d0be09ed4a9da51fd92ccbd105b9ab5e92746241544319a0290874bc1fdee450461ee0d852b35807138ed4f9e72f1951ed1a4d4fa4239e7d94720f834f0dbc388d492a1175c763c864443a05ed8572a8e45c31e03b9caca922d4f4cf20980683e7ed57881626137f2e527121c7598b2851ca9bf534abd5c2ce7f574f3496128fa7486eb327a8c7cf852fd659ac100d7da82532ec88165963e080d399a5dc8aa033dbf694a93a5539b51a7b991551f6a7a531d1090baa393088669c3a7017e2db2a59515cd4b6f3f4c76964e6643b8c1e10292383d6dc04a7a3562a5209e785d224bb9e3cf3deb12e562109994cca304e6813954a5332c090a6c10c6b7d7c2a624efbeb40512fde9f1a3df53342f310fbb94ae55b02c60fc08e4296fd453d2a99ba4d5018b73";
    std::pair<bool, unspendOutputs> developerRecord = utxo.getRecord(developerPublicKey);
    previousTransaction pt("x", 0);
    TEST_ASSERT(std::get<bool>(developerRecord) == true);
    TEST_ASSERT(std::get<unspendOutputs>(developerRecord).size() == 1);
    TEST_ASSERT((std::get<unspendOutputs>(developerRecord))[pt] == 100000);
    TEST_ASSERT(utxo.getRecords().size() == 1);
    TEST_ASSERT(utxo.getEpoch() == -1);
}

// UTXO: equality operator test
void testEqualityOperatorUTXO(){
    UTXO utxo1(50);
    UTXO utxo2(100);
    TEST_ASSERT(!(utxo1 == utxo2));
}

// UTXO: initialize function test
void testInitializationUTXO(){
    UTXO utxo;
    Block block;
    bool valid = std::get<bool>(utxo.initialize({block}));
    TEST_ASSERT(valid == true);

    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string pk = key;
    std::string developersPublicKey = "ba72272ad32b0795375d5574c59aa4f3adba6c52fe16ec7e0a278e031bf9b5b72b3ed113676b92bc985a13e3087860bb062449a29abe2391aea4f0110f06dfa0b44802a0057e67b709303ae4568c18c0efaef83f1122c893899e76db9eeacc44379530457493689b1cd0bf20a79da919af6f39218bed5d81bf3fb6c0c244999b9c0181a1387d89a0013eb55c664e955f6e7d1721e9bb0e507e9ce8e76205518f9785673d28c260ebab32c2afb4a7ccabb4dd55cded0daae4a00ec115fdac11b4f7f118e949313eb4cd79961d8b4f7bb79feca3d88d2fbc3ce812c618ff0e5b6908250d6f34303f099fcca3a083463ddd98fce2e2ea8a9ba0830281d3eac74fb994e5c4356ff1232489e274de5bf61ffef662ce0ae362144145a4b63f23d39353c6860ec840e12d787277f754320934adfc7eec78f1eec821a62b6b08e79a55113faad7ef7002fdebb478c1c04d97ae070c97d2d4c0b4c43ba0037ef387c05899a60f8698eccd0221a15e6a1d3ee78997809860a7b016d5bd522768af207aaf722f517a46063712e5af74e8013a3fe7701411cbb5bafb7a0242140a6c671d16fbd0d2cb4ed2e07e7762cf45fe2aea53065488a4139bd1dfd8e49736d9ca01e88531689a65fa3c5bec6d272533b646dfd050ec04e7049d13a65176b053c7f98b1c07fe153b135a069ecedca56c42b97e67fb2004be7ac7f9f4151fb2a5ca448babe0c6aa5a5d697601339787e382a0710c9f81ab36b8e13874f6583a2d75c21c66f43e60ea3c4712ab7336329b575646c8fd6e203f81caf54515c9bdd02da428b5cb9d5773927dc3e140bb70e4db780473525407d12579e0d46e213d3a7dedc6da55f379e1586f03424a0d115694e09f29b7eb6a4821f37f147e5b36750b0fcc6b999c79b8aebdd494e85065645e854650ee0066cb7d22f052727223c7c62c37d91518d587879d2c5c830391860baafa7ddae2b004841ab3b1bed651a408807ecb751d5d965b637bc418144a1813ae17f2495479e2810e48c7d64684596b77d51d38f6edb3563acd65b6dc09d7d291342c3ed603b022fb4229488f2a67fde683f0291bcece88e9ef4e2c95ebdd7f5651bd209a0ebcd29f1b14e2c9a2621021dbae28468fa51a753768e9d69779cfa378d3c4777eded6c83d4d660e43dfefaf28bd6ded9889d6df8e4761afb6acbb443ace316fbfa66c3236504d17eceda0f47c42ce7766e81b041b5bd387f953e4a50ffecf7d7d002630e8e8ed40198feb18955172667d8d3816c1b46fa1b49094543b5e287db1ca9c80b79400ecaf9ad541ccbc5c3de0297be186a8be8255972d90172c101cbb065744394ff9eae37b0af6c9322528d8c5f1d1cbc84a46f7f0601f20300d5621e54b5a721d5b120b7ac3deb88c18c787f9918f4819086239708677318a539fc2d9bab753f4ae0c3812a7a486e5433c7f07ad7422d0793d87747d3260a8a3a5dbe1b9c3e37e7b1deed35763a4144dd23c315f6ab288b7c5cffa4d847369747f3aa82129dbbcde17b7d62c77beddfe081e663224c8722485c618027899122b49a2163bd50428162aef34bc09cfa0c3ce140591306bba7c140fff6def1cd19c609a9e0a49791aaf44510c7a6eb42f1025ac7d8add18f074d57b76b21c2f915939e28274b1471f5a702123f3709b24e80c25cbceae0268348f0cab69e38cbc6ebf0af938ad258c2c0d835776f78051bf62f7925aecef242d657de28423fb08a670a8791f7d8dce9cdd73af11d7280818bf6b9d18f1ecdc111f51e921a8fbfa45d5a32cca0092e2def5ebd87190d937c2999209d4a2a3a8aaad4995b142bef77592b8288f0fbc3b587fbb8990f24815e06aa16edb5a47e3faaa9753e5c21aa5af93a0d1a7bc4f3ba39cdeae9509e25ed15040e8c7dbd072884b4433812326041d27466a1c9553de97b9d512df5421f3d845c369041c51d04d508a227eab25728a6898d668570c93a0a513f329af822c38cdd8dc39d1d737e2127be0455f01fc12f5ef55fcc3ecb1996116ff4e4ede4109eb5ef0c7336d08c4b5711eec4457f019f1f52d5ade05aa6db350d914a734d1741efabae8e9995b502db2f24ff117e350b819ad511ce0b1e1975711f819701ed64900c1fc1b3f6c5691dbb7f1ee61c98cf632480d7f17fbbf7f1f8206e6af387109290a2e0498fc732445c9c0935e0c99e0b6de501f6f3a05ee4636d5e0b3baa67c1a225efefef3605df46d9def090c68c0dbd720dfe4cb249f535e948a4304cf76715dac8bd35ae80b8e7708327f01cf3aeefc131fcf5ea1ef9dbb6884d62c9e2adee04dd08dd1126cfaae9f7b089cec1cc6e0e424f335a404b5fa19875385c84b4a68b95ef69b845a140a2657d8d59aa0a632412ea39ed4563d13b95b62025433668e1ff906d3d68c4b427a5317d8bed45120dc3fe406b8a4f7a1e95a235824b3135b98e954b5dffdfc3044c2fbc1c6d2697c0dd748749bae4fbd7237e5fe3bb92aa1ca9e52070a35a0248e9fd4ca05c309de30f57d1e4bb86212aadcca4121024859220a1a49a4bd6f5673f2c5ae8f08f9222bb0155953681f6a861d2abd43039c1781f364916bfd5eb704b5954fddeb8480b561ba853456bbef6ea86e824e34ad5e3070743a9e15a28dbf48ebe7959d9e410d8bb4c73cbe3c7e9f201ad225a2fe485911fc5d2dafd8d7e56b65e972ae00edb57350e22d4e16c7763774ce3abff422b7b209a7c7ffeb9c52c256a3ea3abf575a2a1cfa113c3e5f9b27eeebba8d78b6e6f98f63582c4c2b900032fbff5c00ce4a1439b4767ffd1dcad1ec5630ce6b016eedf20e3da39025540815e342ffda156b71e033a41f0355f4f3bf0b5d9ab8c799e11244486f285e1d4677d58832bd94816b288ea27e32f34d0df4e349372f80918db9cf10306674ee9885ae6d2b028e34968b1e36097977da67380f378d604959849c09685f0aebfa4da5bd41a7334ea0939a006f04438f18b016cb8f248c110e6eb7a072fbd51c8175e62d120ca770c9b052c8c0f79579cba000a9aba970610f21884402c7d71a8ee3e677c624afe3a2dbfb04c1b0eaf18bdaa096de053270085347f1fe459de82c7c0be6478bff0683269917d4549ccec7e9aad4bd2abeeaf7df89ace2df2b689c66dea210ed6d41490c3226df791a7bae99406b1e43991fce723038197841f3fa9cdb22a291e5f37bfbc86cdc63a6c86680f67fd88de8f16fb7c4dea3f857cbc4babe5ba7e8467ab4a06f260b814d0be09ed4a9da51fd92ccbd105b9ab5e92746241544319a0290874bc1fdee450461ee0d852b35807138ed4f9e72f1951ed1a4d4fa4239e7d94720f834f0dbc388d492a1175c763c864443a05ed8572a8e45c31e03b9caca922d4f4cf20980683e7ed57881626137f2e527121c7598b2851ca9bf534abd5c2ce7f574f3496128fa7486eb327a8c7cf852fd659ac100d7da82532ec88165963e080d399a5dc8aa033dbf694a93a5539b51a7b991551f6a7a531d1090baa393088669c3a7017e2db2a59515cd4b6f3f4c76964e6643b8c1e10292383d6dc04a7a3562a5209e785d224bb9e3cf3deb12e562109994cca304e6813954a5332c090a6c10c6b7d7c2a624efbeb40512fde9f1a3df53342f310fbb94ae55b02c60fc08e4296fd453d2a99ba4d5018b73";
    TEST_ASSERT(pk == developersPublicKey);
    // transaction from developer to Milan wallet
    Block genesis = createGenesisBlock(defaultWallets);
    UTXO utxo1;
    bool valid1 = std::get<bool>(utxo1.initialize({genesis}));
    TEST_ASSERT(valid1 == true);
    TEST_ASSERT(utxo1.getEpoch() == 0);
    // initialize again should not be possible
    bool valid2 = std::get<bool>(utxo1.initialize({genesis}));
    TEST_ASSERT(valid2 == false);
    TEST_ASSERT(utxo1.getRecords().size() == 4);
}

// UTXO: get record function test
void testGetRecordUTXO(){
    UTXO utxo1(50);

    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);

    TEST_ASSERT(utxo1.getRecords().size() == 4);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[0].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[0].second)).size() == 1);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[1].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[1].second)).size() == 1);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[2].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[2].second)).size() == 1);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord("alfa")) == false);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord("0")) == false);
}

// UTXO: get number of coins function test
void testGetNumberOfCoinsUTXO() {
    UTXO utxo1(50);

    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);
    std::pair<bool,unsigned int> result1 = utxo1.getNumberOfCoins(developersPublicKey, "first", 3);
    std::pair<bool,unsigned int> result2 = utxo1.getNumberOfCoins(developersPublicKey, "first", 0);
    std::pair<bool,unsigned int> result3 = utxo1.getNumberOfCoins(developersPublicKey, "fist", 3);
    std::pair<bool,unsigned int> result4 = utxo1.getNumberOfCoins("developersPublicKey", "fist", 3);
    std::pair<bool,unsigned int> result5 = utxo1.getNumberOfCoins(defaultWallets[0].second, "first", 0);
    std::pair<bool,unsigned int> result6 = utxo1.getNumberOfCoins(defaultWallets[1].second, "first", 1);
    std::pair<bool,unsigned int> result7 = utxo1.getNumberOfCoins(defaultWallets[2].second, "first", 2);
    std::pair<bool,unsigned int> result8 = utxo1.getNumberOfCoins(defaultWallets[2].second, "first", 1);
    TEST_ASSERT(std::get<bool>(result1) == true);
    TEST_ASSERT(std::get<unsigned int>(result1) == 97000);
    TEST_ASSERT(std::get<bool>(result2) == false);
    TEST_ASSERT(std::get<unsigned int>(result2) == 0);
    TEST_ASSERT(std::get<bool>(result3) == false);
    TEST_ASSERT(std::get<unsigned int>(result3) == 0);
    TEST_ASSERT(std::get<bool>(result4) == false);
    TEST_ASSERT(std::get<unsigned int>(result4) == 0);
    TEST_ASSERT(std::get<bool>(result5) == true);
    TEST_ASSERT(std::get<unsigned int>(result5) == 1000);
    TEST_ASSERT(std::get<bool>(result6) == true);
    TEST_ASSERT(std::get<unsigned int>(result6) == 1000);
    TEST_ASSERT(std::get<bool>(result7) == true);
    TEST_ASSERT(std::get<unsigned int>(result7) == 1000);
    TEST_ASSERT(std::get<bool>(result8) == false);
    TEST_ASSERT(std::get<unsigned int>(result8) == 0);
}


// UTXO: correspong public key function test
void testCorrespondingPublicKeyUTXO(){
    UTXO utxo1(50);
    
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);
    std::pair<bool,std::string> result1 = utxo1.findCorrespondingPublicKey("first", 0);
    std::pair<bool,std::string> result2 = utxo1.findCorrespondingPublicKey("first", 1);
    std::pair<bool,std::string> result3 = utxo1.findCorrespondingPublicKey("first", 2);
    std::pair<bool,std::string> result4 = utxo1.findCorrespondingPublicKey("first", 3);
    std::pair<bool,std::string> result5 = utxo1.findCorrespondingPublicKey("first", 4);
    TEST_ASSERT(std::get<bool>(result1) == true);
    TEST_ASSERT(std::get<std::string>(result1) == defaultWallets[0].second);
    TEST_ASSERT(std::get<bool>(result2) == true);
    TEST_ASSERT(std::get<std::string>(result2) == defaultWallets[1].second);
    TEST_ASSERT(std::get<bool>(result3) == true);
    TEST_ASSERT(std::get<std::string>(result3) == defaultWallets[2].second);
    TEST_ASSERT(std::get<bool>(result4) == true);
    TEST_ASSERT(std::get<std::string>(result4) == developersPublicKey);
    TEST_ASSERT(std::get<bool>(result5) == false);
    TEST_ASSERT(std::get<std::string>(result5) == "");
}

// UTXO: add unspend output function test
void testAddUnspendOutputUTXO(){
    UTXO utxo1(50);
    
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);

    previousTransaction ptDev1("oke", 10);
    previousTransaction ptDev2("first", 3);
    unsigned int coins1 = 450;
    unsigned int coins2 = 500;

    utxo1.addUnspendOutput(developersPublicKey, ptDev1, coins1);

    TEST_ASSERT(utxo1.getRecords().size() == 4);
    utxo1.addUnspendOutput("user", ptDev2, coins2);
    TEST_ASSERT(utxo1.getRecords().size() == 5);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord("user")) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord("user"))[ptDev2] == 500);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(developersPublicKey)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(developersPublicKey)).size() == 2);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(developersPublicKey))[ptDev1] == coins1);
}

// UTXO: verify transaction function test
void testVerifyTransactionUTXO(){
    UTXO utxo1(50);
    
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key1 = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key1;
    const auto key2 = toml::find<std::string>(table2, "private_key");
    std::string developersPrivateKey = key2;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);

    // Transaction 1
    std::vector<unsigned char> transactionId({'s', 'e', 'c', 'o', 'n','d'});
    // All inputs
    Input inputFromDevelopers1(std::vector<unsigned char>({'f', 'i', 'r', 's', 't'}), 3);
    // All outputs
    std::vector<Output> outputs;
    // Milan
    Output outputToMilan(500, defaultWallets[0].second);
    outputs.push_back(outputToMilan);
    // Linde
    Output outputToLinde(1000, defaultWallets[1].second);
    outputs.push_back(outputToLinde);
    // Glenn
    Output outputToGlenn(500, defaultWallets[2].second);
    outputs.push_back(outputToGlenn);
    // Developers
    Output outputToDevelopers(95000, developersPublicKey);
    outputs.push_back(outputToDevelopers);
    // Making the transaction
    Transaction transaction1({inputFromDevelopers1}, outputs, "", transactionId);
    // Signing the transaction with the developers' wallet
    KeyPair devKeyPair(developersPublicKey, developersPrivateKey, "admin");
    std::string message1 = transaction1.hashTransaction();
    std::string signedMessage1 = devKeyPair.createSignature(message1, "admin");
    // Setting the signature on the block
    transaction1.setSenderSig(signedMessage1);
    TEST_ASSERT(std::get<bool>(utxo1.verify(transaction1)) == true);

    // Transaction 2
    Input inputFromDevelopers2(std::vector<unsigned char>({'f', 'i', 'r', 's', 't'}), 2);
    Transaction transaction2({inputFromDevelopers2}, outputs, "", transactionId);
    // Signing the transaction with the developers' wallet
    std::string message2 = transaction1.hashTransaction();
    std::string signedMessage2 = devKeyPair.createSignature(message2, "admin");
    // Setting the signature on the block
    transaction1.setSenderSig(signedMessage2);
    TEST_ASSERT(std::get<bool>(utxo1.verify(transaction2)) == false);

    // Transaction 3
    // Milan
    Output outputToMilan3(500, defaultWallets[0].second);
    // Linde
    Output outputToLinde3(1000, defaultWallets[1].second);
    // Glenn
    Output outputToGlenn3(500, defaultWallets[2].second);
    // Developers
    Output outputToDevelopers3(90000, developersPublicKey);
    Transaction transaction3({inputFromDevelopers1}, {outputToMilan3, outputToLinde3, outputToGlenn3, outputToDevelopers3}, "", transactionId);
    // Signing the transaction with the developers' wallet
    std::string message3 = transaction1.hashTransaction();
    std::string signedMessage3 = devKeyPair.createSignature(message3, "admin");
    // Setting the signature on the transaction
    transaction1.setSenderSig(signedMessage3);
    std::cout << std::get<std::string>(utxo1.verify(transaction3)) << std::endl;
    TEST_ASSERT(std::get<bool>(utxo1.verify(transaction3)) == false);

    // Empty transaction
    Transaction transaction4({}, {}, "", transactionId);
    std::cout << std::get<std::string>(utxo1.verify(transaction4)) << std::endl;
    TEST_ASSERT(std::get<bool>(utxo1.verify(transaction4)) == false);

    // Using same input twice
    Transaction transaction5({inputFromDevelopers1, inputFromDevelopers1}, {outputToMilan3, outputToLinde3, outputToGlenn3, outputToDevelopers3}, "", transactionId);
    std::cout << std::get<std::string>(utxo1.verify(transaction5)) << std::endl;
    TEST_ASSERT(std::get<bool>(utxo1.verify(transaction5)) == false);
}

// UTXO: verify block function test
void testVerifyBlockUTXO(){
    UTXO utxo1(50);
    
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key1 = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key1;
    const auto key2 = toml::find<std::string>(table2, "private_key");
    std::string developersPrivateKey = key2;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);

    // Transaction 1
    std::vector<unsigned char> transactionId({'s', 'e', 'c', 'o', 'n','d'});
    // All inputs
    Input inputFromDevelopers1(std::vector<unsigned char>({'f', 'i', 'r', 's', 't'}), 3);
    // All outputs
    std::vector<Output> outputs;
    // Milan
    Output outputToMilan(500, defaultWallets[0].second);
    outputs.push_back(outputToMilan);
    // Linde
    Output outputToLinde(1000, defaultWallets[1].second);
    outputs.push_back(outputToLinde);
    // Glenn
    Output outputToGlenn(500, defaultWallets[2].second);
    outputs.push_back(outputToGlenn);
    // Developers
    Output outputToDevelopers(95000, developersPublicKey);
    outputs.push_back(outputToDevelopers);
    // Making the transaction
    Transaction transaction1({inputFromDevelopers1}, outputs, "", transactionId);
    // Signing the transaction with the developers' wallet
    KeyPair devKeyPair(developersPublicKey, developersPrivateKey, "admin");
    std::string message1 = transaction1.hashTransaction();
    std::string signedMessage1 = devKeyPair.createSignature(message1, "admin");
    // Setting the signature on the transaction
    transaction1.setSenderSig(signedMessage1);
    Block block1("", 1, {transaction1});
    Block block2("", 1, {transaction1, transaction1});
    TEST_ASSERT(std::get<bool>(utxo1.verify(block1)) == true);
    std::cout << std::get<std::string>(utxo1.verify(block2)) << std::endl;
    TEST_ASSERT(std::get<bool>(utxo1.verify(block2)) == false);
}

// UTXO: update transaction function test
void testUpdateTransactionUTXO(){
    UTXO utxo1(50);
    
    // Put the public keys of other default wallets from the TOML file into the vector defaultWallets
    std::vector<std::pair<std::string,std::string>> defaultWallets;
    const toml::value data1 = toml::parse("defaultWallets.toml");
    const auto& table1 = toml::find(data1, "wallets");
    for(const auto& v : table1.as_array() ) {
        const auto walletId = toml::find<std::string>(v, "wallet_id");
        const auto walletPublicKey = toml::find<std::string>(v, "public_key");
        std::pair<std::string,std::string> defaultWallet(walletId, walletPublicKey);
        defaultWallets.push_back(defaultWallet);
    }
    // Take the public key from the developers out of the TOML file
    const toml::value data2 = toml::parse("developers.toml");
    const auto& table2 = toml::find(data2, "developers");
    const auto key1 = toml::find<std::string>(table2, "public_key");
    std::string developersPublicKey = key1;
    const auto key2 = toml::find<std::string>(table2, "private_key");
    std::string developersPrivateKey = key2;

    Block genesis = createGenesisBlock(defaultWallets);
    utxo1.update(genesis);

    // Transaction 1
    std::vector<unsigned char> transactionId({'s', 'e', 'c', 'o', 'n','d'});
    // All inputs
    Input inputFromDevelopers1(std::vector<unsigned char>({'f', 'i', 'r', 's', 't'}), 3);
    // All outputs
    std::vector<Output> outputs;
    // Milan
    Output outputToMilan(500, defaultWallets[0].second);
    outputs.push_back(outputToMilan);
    // Linde
    Output outputToLinde(1000, defaultWallets[1].second);
    outputs.push_back(outputToLinde);
    // Glenn
    Output outputToGlenn(500, defaultWallets[2].second);
    outputs.push_back(outputToGlenn);
    // Developers
    Output outputToDevelopers(95000, developersPublicKey);
    outputs.push_back(outputToDevelopers);
    // Making the transaction
    Transaction transaction1({inputFromDevelopers1}, outputs, "", transactionId);
    // Signing the transaction with the developers' wallet
    KeyPair devKeyPair(developersPublicKey, developersPrivateKey, "admin");
    std::string message1 = transaction1.hashTransaction();
    std::string signedMessage1 = devKeyPair.createSignature(message1, "admin");
    // Setting the signature on the transaction
    transaction1.setSenderSig(signedMessage1);
    TEST_ASSERT(std::get<bool>(utxo1.update(transaction1)) == true);
    std::pair<std::string, unsigned int> ptdev("second", 3);
    TEST_ASSERT(utxo1.getRecords().size() == 4);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(developersPublicKey)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(developersPublicKey)).size() == 1);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(developersPublicKey))[ptdev] == 95000);

    std::pair<std::string, unsigned int> ptwallet01("first", 0);
    std::pair<std::string, unsigned int> ptwallet02("second", 0);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[0].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[0].second)).size() == 2);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[0].second))[ptwallet01] == 1000);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[0].second))[ptwallet02] == 500);

    std::pair<std::string, unsigned int> ptwallet11("first", 1);
    std::pair<std::string, unsigned int> ptwallet12("second", 1);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[1].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[1].second)).size() == 2);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[1].second))[ptwallet11] == 1000);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[1].second))[ptwallet12] == 1000);

    std::pair<std::string, unsigned int> ptwallet21("first", 2);
    std::pair<std::string, unsigned int> ptwallet22("second", 2);
    TEST_ASSERT(std::get<bool>(utxo1.getRecord(defaultWallets[2].second)) == true);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[2].second)).size() == 2);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[2].second))[ptwallet21] == 1000);
    TEST_ASSERT(std::get<unspendOutputs>(utxo1.getRecord(defaultWallets[2].second))[ptwallet22] == 500);
}

TEST_LIST = {
    //Convert
    {"hexadecimal to ascii convert", testHexToASCIIConvert},
    {"ascii to hexadecimal convert", testASCIIToHexConvert},
    {"ascii to decimal convert", testASCIIToDecConvert},
    //Input
    {"equality operator input", testEqualityOperatorInput},
    {"txId getter input", testTxIdGetterInput},
    {"output index getter input", testOutputIndexGetterInput},
    {"make string of input", testMakeStringInput},
    {"size getter input", testSizeGetterInput},
    //Output
    {"equality operator output", testEqualityOperatorOutput},
    {"value getter output", testValueGetterOutput},
    {"receiver public key getter output", testReceiverPkGetterOutput},
    {"make string of input", testMakeStringOutput},
    {"size getter output", testSizeGetterOutput},
    //Header
    {"equality operator header", testEqualityOperatorHeader},
    {"id getter header", testIdGetterHeader},
    {"validator signatures getter header", testValidatorSigsGetterHeader},
    {"previous block digest getter header", testPrevBlockDigestGetterHeader},
    {"merkel root getter header", testMerkelRootGetterHeader},
    {"transaction ids getter header", testTxIdsGetterHeader},
    {"add signature header", testAddSignatureHeader},
    {"size getter header", testSizeGetterHeader},
    //Transaction
    {"equality operator transaction", testEqualityOperatorTransaction},
    {"inputs getter transaction", testInputsGetterTransaction},
    {"outputs getter transaction", testOutputsGetterTransaction},
    {"sender signature getter transaction", testSenderSigGetterTransaction},
    {"transaction id getter transaction", testTxIdGetterTransaction},
    {"sender signature setter transaction", testSenderSigSetterTransaction},
    {"hash transaction", testHashTransaction},
    {"id to string transaction", testIdToStringTransaction},
    {"size getter transaction", testSizeGetterTransaction},
    //Signature
    {"public key getter signature", testPublicKeyGetterKeyPair},
    {"hashed password getter signature", testHashPasswordGetterKeyPair},
    {"generate keypair signature", testGenerateKeyPair},
    {"verifying and creating signature", testVerifyAndCreateKeyPair},
    //Block
    {"equality operator block", testEqualityOperatorBlock},
    {"epoch getter block", testEpochGetterBlock},
    {"previous hash getter block", testPreviousHashGetterBlock},
    {"payload getter block", testPayloadGetterBlock},
    {"notarize block", testNotarizeBlock},
    {"number of signatures getter block", testNbSignaturesGetterBlock},
    {"finalize block", testFinalizeBlock},
    {"header getter block", testHeaderGetterBlock},
    {"create merkel root tree block", testCreateMerkelRootBlock},
    {"transaction in block", testTransactionInBlock},
    {"hash block", testHashBlock},
    {"size getter block", testSizeGetterBlock},
    //BlockChain
    {"equality operator blockchain", testEqualityOperatorBlockChain},
    {"blocks getter blockchain", testBlocksGetterBlockChain},
    {"chainlength getter blockchain", testChainLengthGetterBlockChain},
    {"current epoch getter blockchain", testCurrentEpochGetterBlockChain},
    {"index last finalized block getter blockchain", testIndexLastFinalizedBlockGetterBlockChain},
    {"UTXO getter blockchain", testUTXOGetterBlockChain},
    {"add block blockchain", testAddBlockBlockChain},
    {"could be added blockchain", testCouldBeAddedBlockChain},
    {"hash prefix blockchain", testHashPrefixBlockChain},
    //UTXO
    {"constructor with no input UTXO", testConstructorNoInputUTXO},
    {"constructor with with id input UTXO", testConstructorInputUTXO},
    {"equality operator UTXO", testEqualityOperatorUTXO},
    {"initialize function of blocks UTXO", testInitializationUTXO},
    {"get record function UTXO", testGetRecordUTXO},
    {"get number of coins function UTXO", testGetNumberOfCoinsUTXO},
    {"get corresponding public key function UTXO", testCorrespondingPublicKeyUTXO},
    {"add unspend output function UTXO", testAddUnspendOutputUTXO},
    {"verify transaction function UTXO", testVerifyTransactionUTXO},
    {"verify block function in UTXO", testVerifyBlockUTXO},
    {"update transaction function in UTXO", testUpdateTransactionUTXO},
    {NULL, NULL}
};