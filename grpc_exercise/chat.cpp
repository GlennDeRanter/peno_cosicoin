#include <iostream>
#include <memory>
#include <string>
#include <list>
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
//#include <grpcpp/ext/proto_server_reflection_plugin.h>


#ifdef BAZEL_BUILD
#include "chat.grpc.pb.h"
#else
#include "chat.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using chat::ChatMessage;
using chat::Ack;
using chat::Chat;

class chatClient {
 public:
  chatClient(std::shared_ptr<Channel> channel)
      : stub_(Chat::NewStub(channel)) {}

  chatClient(chatClient const& objchat)
    {
      stub_ = objchat.getStub();
    }

  const chatClient& operator=(chatClient const& objchat)
    {
      return objchat;
    }
  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  std::string Talk(const std::string& user, const std::string& message) {
    // Data we are sending to the server.
    ChatMessage post;
    post.set_name(user);
    post.set_message(message);
  
    Ack Acknowledge;
    ClientContext context;

    // The actual  RPC.
    Status status = stub_->Talk(&context, post, &Acknowledge);
    // Act upon its status.
    if (status.ok()) {
      return "acknowledged";
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return "RPC failed";
    }
  }
  public:
    std::shared_ptr<Chat::Stub> getStub() const {
      return stub_;
    }

  private:
  std::shared_ptr<Chat::Stub> stub_;
};

class GreeterServiceImpl final : public Chat::Service {
    Status Talk(ServerContext* context, const ChatMessage* post, Ack* acknowledge
    ) {
    std::cout << post->name() << ":" << post->message() << std::endl;
    return Status::OK;
  }
};

  void RunServer(std::string server_address) {
    GreeterServiceImpl service;

    grpc::EnableDefaultHealthCheckService(true);
    //grpc::reflection::InitProtoReflectionServerBuilderPlugin();
    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    builder.RegisterService(&service);
    // Finally assemble the server.
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server -> Wait();
    }

void clientActions(int argc, char** argv){
  sleep(2);
  std::string username;
  std::string target_address;
  std::vector<chatClient> poolOfReceivers;

  std::cout << "Enter your username:" << std::endl;
  std::cin >> username;
  std::cout << "Welcome to chat, " << username << std::endl;

  for(int indx = 2; indx<argc; ++indx){
    target_address = argv[indx];
    chatClient client(grpc::CreateChannel(
    target_address, grpc::InsecureChannelCredentials()));  
    poolOfReceivers.push_back(client);
  };

  // right now the server never exits, but we could also make it so that we exit the while loop at some point

  while (true){
    // here we can do something, I don't know if this is the way they want us to implement it though
    std::string message;
    std::cin >> message;
        // std::cout << username << ":" << message << std::endl;
    for (int nmb =0; nmb<argc-2; ++nmb){
      poolOfReceivers.at(nmb).Talk(username,message);
    }
    
  }
}

int main(int argc, char** argv) {
  
  std::string arg_val;
  std::cout << argc << std::endl;
  arg_val = argv[1];

  std::thread talkingThreads[2];
  talkingThreads[0] = std::thread(RunServer, arg_val);
  talkingThreads[1] = std::thread(clientActions, argc, argv);
  talkingThreads[0].join();
  talkingThreads[1].join();
  return 0;
}

