\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Speed}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{ Keygen speed}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Signing speed}{7}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{Verification speed}{10}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Memory}{13}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Keygen memory}{14}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Signing memory}{16}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{3}{Verification memory}{18}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Hardware implementation possible}{20}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Software implementations found}{22}{0}{4}
