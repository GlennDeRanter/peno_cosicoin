#ifndef UTXO_H
#define UTXO_H

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "Transaction.hpp"
#include "Block.hpp"
#include "toml.hpp"


class UTXO {
    public:
// Type definition for the previous transaction, containing the transaction id and the output index referenced
        typedef std::pair<std::string, unsigned int> previousTransaction;
// Type definition for the unspend outputs, containing the previous transaction information and the amount that has been left unspent
        typedef std::map<previousTransaction, unsigned int> unspendOutputs;
// Type definition for the records, containing the public key and the unspend outputs corresponding to the public key
        typedef std::map<std::string, unspendOutputs> Records;

    private:
// The UTXO records, 
        Records utxo_;
// The id of the UTXO to link it to a single blockchain
        int UTXOId_;
// The epoch number up until which the UTXO is updated for the block chain
        int epoch_;

    public:


/*************************************************
* Name:        Default constructor
*
* Description: Constructs the UTXO.
*
* Arguments:   None
*
* No Return value
**************************************************/
        UTXO()
        : epoch_(-1)
        , utxo_()
        {
            UTXOId_ = rand()%(5000);
            // Take the public key from the developers out of the TOML file
            const toml::value data2 = toml::parse("developers.toml");
            const auto& table2 = toml::find(data2, "developers");
            const auto key = toml::find<std::string>(table2, "public_key");
            std::string developersPublicKey = key;

            // Default: only developers have coins
            previousTransaction pt("x", 0);
            unspendOutputs firstUnspendOutput;
            firstUnspendOutput.insert(std::pair<previousTransaction, unsigned int>(pt, 100000));
            Records utxo;
            utxo[developersPublicKey] = firstUnspendOutput;
            utxo_ = utxo;            
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the UTXO.
*
* Arguments:   -int id: the unique id we which to give
*               to the UTXO
*
* No Return value
**************************************************/
        UTXO(int id)
        : epoch_(-1)
        , UTXOId_(id)
        , utxo_()
        {
            // Take the public key from the developers out of the TOML file
            const toml::value data2 = toml::parse("developers.toml");
            const auto& table2 = toml::find(data2, "developers");
            const auto key = toml::find<std::string>(table2, "public_key");
            std::string developersPublicKey = key;

            // Default: only developers have coins
            previousTransaction pt("x", 0);
            unspendOutputs firstUnspendOutput;
            firstUnspendOutput.insert(std::pair<previousTransaction, unsigned int>(pt, 100000));
            Records utxo;
            utxo[developersPublicKey] = firstUnspendOutput;
            utxo_ = utxo;
        }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old UTXO
               in a new UTXO.
*
* Arguments:   -UTXO const& utxo: the UTXO we want to copy
*
* No Return value
**************************************************/
        UTXO(UTXO const& utxo)
            : utxo_(utxo.getRecords())
            , UTXOId_(utxo.getUTXOId())
            , epoch_(utxo.getEpoch())
            {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the UTXO is equal
*              to a given UTXO and otherwise 
*              makes sure they are.
*
* Arguments:   -UTXO const& utxo: the UTXO we want to match to
*
* Returns *this (success)
**************************************************/
        UTXO& operator=(UTXO const& utxo){
            if (!(*this == utxo)) {
                utxo_ = utxo.getRecords();
                UTXOId_ = utxo.getUTXOId();
                epoch_ = utxo.getEpoch();
            }
         
            return *this;
        }

/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the UTXO matches a 
*              given UTXO.
*
* Arguments:   -UTXO const& utxo: utxo object
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
        bool operator==(UTXO const& utxo){
                if (epoch_ != utxo.getEpoch() || UTXOId_ != utxo.getUTXOId()){
                    return false;
                }
                else {
                    if (utxo_.size() != utxo.getRecords().size()) {
                        return false;
                    } else {
                        if (utxo_ != utxo.getRecords()) {
                            return false;
                        }
                    }
                }
        return true;
        }


/*************************************************
* Name:        initialize (for multiple blocks)
*
* Description: Update the UTXO with the given blocks
*              if possible (it is updated until the last valid block).
*
* Arguments:   -std::vector<Block> blocks: blocks we
*               want to update our UTXO with
*
* Returns bool that tells us whether the update was successful
* and a string that gives the reason if it is not
**************************************************/
        inline std::pair<bool,std::string> initialize(std::vector<Block> blocks) {
            // Update the UTXO
            for (int i=0; i<blocks.size(); ++i) {
                Block block = blocks[i];
                std::pair<bool,std::string> result = update(block);
                if (std::get<bool>(result) == false) {
                    std::cout << "[UTXO " << getUTXOId() << "] --> The given UTXO is initialized with " << i << " blocks out of " << blocks.size() << ", since the error: "<< std::get<std::string>(result) << std::endl;
                    return std::pair<bool,std::string>(false, std::get<std::string>(result));
                }
            }
            return std::pair<bool,std::string>(true, "");
        }

/*************************************************
* Name:        update (for one transaction)
*
* Description: Update the UTXO with a given transaction
*              if possible.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns bool that tells us whether the update was successful
* and a string that gives the reason if it is not
**************************************************/
        std::pair<bool,std::string> update(Transaction transaction);

/*************************************************
* Name:        update (for one block)
*
* Description: Update the UTXO with a given block
*              if possible.
*
* Arguments:   -Block block: block we
*               want to update our UTXO with
*
* Returns bool that tells us whether the update was successful
* and a string that gives the reason if it is not
**************************************************/
        std::pair<bool,std::string> update(Block block);

/*************************************************
* Name:        verifyInput
*
* Description: Get the last epoch for which records
*              have been added in the UTXO.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns the total number of coins the sender has in 
* its used unspend outputs in a pair with its public
* key and a pair telling whether the input is valid
* and a reason if not
**************************************************/
        std::pair<std::pair<unsigned int,std::string>, std::pair<bool, std::string>> verifyInput(Transaction transaction);

/*************************************************
* Name:        verify (for one transaction)
*
* Description: Verify if the transaction is correct
*              given the balances in the UTXO.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns bool that tells us whether the verification was successful
* and a string that gives the reason if it is not
**************************************************/
        std::pair<bool,std::string> verify(Transaction transaction);

/*************************************************
* Name:        verify (for one block)
*
* Description: Verify if the block is correct
*              given the balances in the UTXO.
*
* Arguments:   -Block block: block we
*               want to update our UTXO with
*
* Returns bool that tells us whether the verification was successful
* and a string that gives the reason if it is not
**************************************************/
        std::pair<bool,std::string> verify(Block block);

/*************************************************
* Name:        validPublicKey
*
* Description: Function to check whether the given
*              public key is valid
*
* Arguments:   - The the public key to check
*
* Returns a boolean indicating if the public key is 
*         valid and the reason if not
**************************************************/
        inline std::pair<bool,std::string> validPublicKey(std::string publicKey) {
              if (publicKey.length() != 5184) {
                 return std::pair<bool,std::string>(false, "the public key of the receiver should be 4095 hexadecimal characters long");
              }
              if (publicKey.find_first_not_of("0123456789abcdefABCDEF", 2) != std::string::npos) {
                 return std::pair<bool,std::string>(false, "the public key of the receiver should contain only hexadecimal characters");
              }
              return std::pair<bool,std::string>(true, "");
        }

/*************************************************
* Name:        addPublicKey
*
* Description: Add the given public key to the UTXO
*              and setting the balance to zero.
*
* Arguments:   -string publicKey: the public key we
*               want to add to the UTXO
*
* Returns nothing
**************************************************/
        void addUnspendOutput(std::string publicKey, previousTransaction pt, unsigned int value);

/*************************************************
* Name:        findCorrespondingPublicKey
*
* Description: Find the public key for a given previous
*              transaction id and output offset.
*
* Arguments:   -string transactionId: the id of the transaction
*               from which we want to find the public key
*              -unsigned int outputOffset: the output offset of the
*               output from which we want to find the public key
*
* Returns a boolean that is true if the corresponding public key has
* been found and returns the public key we need
**************************************************/
        std::pair<bool,std::string> findCorrespondingPublicKey(std::string transactionId, unsigned int outputOffset);

/*************************************************
* Name:        getNumberOfCoins
*
* Description: Find the number of coins at the output of
*              the public key for the given transaction id
*              and offset.
*
* Arguments:   -string publicKey: the public key for which we
*               want to know the number of coins
*              -string transactionId: the transaction id for which
*               we want to know the number of coins
*              -unsigned int outputOffset: the output offset for which
*               we want to know the number of coins
*
* Returns a boolean that is true if the combination has
* been found and returns the amount
**************************************************/
        std::pair<bool,unsigned int> getNumberOfCoins(std::string publicKey, std::string transactionId, unsigned int outputOffset);

/*************************************************
* Name:        getRecords
*
* Description: Get the records in the UTXO.
*
* Arguments:   None
*
* Returns utxo_
**************************************************/
        inline Records getRecords() const {return utxo_;}

/*************************************************
* Name:        getRecord
*
* Description: Get the record in the UTXO of the given
*              public key.
*
* Arguments:   -string publicKey: public key of which we 
*               want to find the record in the UTXO of.
*
* Returns a boolean that tells us whether there is a record corresponding
* to the public key and if so also the record
**************************************************/
        inline std::pair<bool,unspendOutputs> getRecord(std::string publicKey) const {
                for (auto& record: utxo_) {
                        if (record.first == publicKey){
                                return std::pair<bool,unspendOutputs>(true, record.second);
                        }
                }
            unspendOutputs output;
            return std::pair<bool,unspendOutputs>(false, output);
        }  

/*************************************************
* Name:        getUTXOId
*
* Description: Get the id of the UTXO.
*
* Arguments:   None
*
* Returns UTXOId_
**************************************************/
        inline int getUTXOId() const {return UTXOId_;} 

/*************************************************
* Name:        getEpoch
*
* Description: Get the last epoch for which records
*              have been added in the UTXO.
*
* Arguments:   None
*
* Returns epoch_
**************************************************/
        inline int getEpoch() const {return epoch_;}

};

#endif 
