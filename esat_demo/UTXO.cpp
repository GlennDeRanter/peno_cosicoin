


#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include "Transaction.hpp"
#include "UTXO.hpp"
#include "Block.hpp"

/*************************************************
* Name:        update (for one transaction)
*
* Description: Update the UTXO with a given transaction
*              if possible.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns bool that tells us whether the update was successful
* and a string that gives the reason if it is not
**************************************************/
    std::pair<bool,std::string> UTXO::update(Transaction transaction) {
        std::pair<bool,std::string> result = verify(transaction);
        std::string txId = transaction.IdToString();
        if (std::get<bool>(result) == false) {
            return std::pair<bool,std::string>(false, std::get<std::string>(result));
        } else {
            // Reducing the inputs
            std::string idInput = (transaction.getInputs()[0]).IDToString();
            unsigned int indexInput = (transaction.getInputs()[0]).getOutputIndex();
            std::string publicKey = std::get<std::string>(findCorrespondingPublicKey(idInput, indexInput));
            unspendOutputs senderOutputs = std::get<unspendOutputs>(getRecord(publicKey));
            // Erase the output from the sender's record
            // Checking whether it is the last unspend output of the sender
            if (senderOutputs.size() == 1) {
                utxo_.erase(publicKey);
            } else {
                senderOutputs.erase(std::pair<std::string, unsigned int>(idInput, indexInput));
            }
            for (int i=1; i < transaction.getInputs().size(); i++) {
                std::string idInput = (transaction.getInputs()[i]).IDToString();
                unsigned int indexInput = (transaction.getInputs()[i]).getOutputIndex();
                // Erase the output from the sender's record
                senderOutputs.erase(std::pair<std::string, unsigned int>(idInput, indexInput));
            }
            // Incrementing the outputs
            for (unsigned int i=0; i < transaction.getOutputs().size(); i++) {
                std::string receiverPk = (transaction.getOutputs()[i]).getReceiverPk();
                unsigned int value = (transaction.getOutputs()[i]).getValue();
                // Include the output in the receiver's record
                previousTransaction pt(txId, i);
                addUnspendOutput(receiverPk, pt, value);
            }
        }
        return std::pair<bool,std::string>(true, "");
    }

/*************************************************
* Name:        update (for one block)
*
* Description: Update the UTXO with a given block
*              if possible.
*
* Arguments:   -Block block: block we
*               want to update our UTXO with
*
* Returns bool that tells us whether the update was successful
* and a string that gives the reason if it is not
**************************************************/
    std::pair<bool,std::string> UTXO::update(Block block) {
        std::pair<bool,std::string> result = verify(block);
        if (std::get<bool>(result) == false) {
            return std::pair<bool,std::string>(false, std::get<std::string>(result));
        } else {
            for (int i=0; i < block.getPayload().size(); i++) {
                Transaction transaction = block.getPayload()[i];
                update(transaction);
            }
        }
        // Update the epoch of the UTXO
        epoch_ = block.getEpoch();

        return std::pair<bool,std::string>(true, "");
    }

/*************************************************
* Name:        verifyInput
*
* Description: Get the last epoch for which records
*              have been added in the UTXO.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns the total number of coins the sender has in 
* its used unspend outputs in a pair with its public
* key and a pair telling whether the input is valid
* and a reason if not
**************************************************/
        std::pair<std::pair<unsigned int,std::string>, std::pair<bool, std::string>> UTXO::verifyInput(Transaction transaction){
                unsigned int inputValue = 0;
                std::string idInput = (transaction.getInputs()[0]).IDToString();
                unsigned int indexInput = (transaction.getInputs()[0]).getOutputIndex();
                std::pair<bool, std::string> result = findCorrespondingPublicKey(idInput, indexInput);
                if (std::get<bool>(result) == false) {
                        std::pair<bool, std::string> result(false, "The transaction with id " + transaction.IdToString() + " is invalid, since input 0 refering to transaction " + idInput + " and offset " + to_string(indexInput) + " is not present in the UTXO.");
                        std::pair<unsigned int, std::string> keyValue(0,"");
                        return std::pair<std::pair<unsigned int, std::string>, std::pair<bool, std::string>>(keyValue,result);
                }
                std::string firstPublicKey = std::get<std::string>(result);
                unspendOutputs possibleUTXOInputs = std::get<unspendOutputs>(getRecord(std::get<std::string>(result)));
                inputValue += std::get<unsigned int>(getNumberOfCoins(firstPublicKey, idInput, indexInput));
                possibleUTXOInputs.erase(std::pair<std::string, unsigned int>(idInput, indexInput));
                // Checking the other inputs
                for (int i=1; i < transaction.getInputs().size(); i++) {
                        std::string idInput = (transaction.getInputs()[i]).IDToString();
                        unsigned int indexInput = (transaction.getInputs()[i]).getOutputIndex();
                        // Is the input in the record of the public key?
                        std::pair<bool,unsigned int> result = getNumberOfCoins(firstPublicKey, idInput, indexInput);
                        if (std::get<bool>(result) == false) {
                            std::pair<bool, std::string> result2(false, "The transaction with id " + transaction.IdToString() + " is invalid, since input " + to_string(i) + " refering to transaction " + idInput + " and offset " + to_string(indexInput) + " is not present in the record of the public key.");
                            std::pair<unsigned int, std::string> keyValue2(0,"");
                            return std::pair<std::pair<unsigned int, std::string>, std::pair<bool, std::string>>(keyValue2,result2);
                        } 
                        else {
                            // Is it not used twice?
                            bool found = false;
                            std::pair<std::string, unsigned int> searchingInput(idInput, indexInput);
                            for (auto& unspend: possibleUTXOInputs) {
                                if (unspend.first == searchingInput){
                                        found == true;
                                }
                            }
                            if (found) {
                                inputValue += std::get<unsigned int>(result);
                                possibleUTXOInputs.erase(std::pair<std::string, unsigned int>(idInput, indexInput));
                            } else {
                                std::pair<bool, std::string> result2(false, "The transaction with id " + transaction.IdToString() + " is invalid, since input " + to_string(i) + " refering to transaction " + idInput + " and offset " + to_string(indexInput) + " is used twice.");
                                std::pair<unsigned int, std::string> keyValue2(0,"");
                                return std::pair<std::pair<unsigned int, std::string>, std::pair<bool, std::string>>(keyValue2, result2);
                            }
                        }
                }
                std::pair<bool, std::string> result3(true, "");
                std::pair<unsigned int, std::string> keyValue3(inputValue,firstPublicKey);
                return  std::pair<std::pair<unsigned int, std::string>, std::pair<bool, std::string>>(keyValue3, result3);
        }

/*************************************************
* Name:        verify (for one transaction)
*
* Description: Verify if the transaction is correct
*              given the balances in the UTXO.
*
* Arguments:   -Transaction transaction: transaction we
*               want to update our UTXO with
*
* Returns bool that tells us whether the verification was successful
* and a string that gives the reason if it is not
**************************************************/
    std::pair<bool,std::string> UTXO::verify(Transaction transaction) {
        // Is the transaction empty?
        if (transaction.getInputs().size() == 0) {
            return std::pair<bool, std::string>(false, "The transaction with id " + transaction.IdToString() + " is invalid, since it doens't contain any inputs.");
        } else if (transaction.getOutputs().size() == 0) {
            return std::pair<bool, std::string>(false, "The transaction with id " + transaction.IdToString() + " is invalid, since it doens't contain any outputs.");
        }
        std::pair<std::pair<unsigned int, std::string>, std::pair<bool, std::string>>result = verifyInput(transaction);
        std::pair<bool, std::string> reason = std::get<std::pair<bool, std::string>>(result);
        std::pair<unsigned int, std::string> keyValue = std::get<std::pair<unsigned int, std::string>>(result);
        unsigned int inputValue = std::get<unsigned int>(keyValue);
        std::string firstPublicKey = std::get<std::string>(keyValue);
        if (std::get<bool>(reason) == false){
            return reason;
        }

        unsigned int outputValue = 0;
        // Checking the outputs
        for (int i=0; i<transaction.getOutputs().size(); i++) {
            std::string receiverPK = (transaction.getOutputs()[i]).getReceiverPk();
            // Is the receiver public key valid?
            if (std::get<bool>(validPublicKey(receiverPK)) == false) {
                return std::pair<bool, std::string>(false, "The transaction with id " + transaction.IdToString() + " is invalid, since " + std::get<std::string>(validPublicKey(receiverPK)) + ".");
            }
            outputValue += (transaction.getOutputs()[i]).getValue();
        }
        // Inputs equal to outputs?
        if (inputValue != outputValue) {
            return std::pair<bool, std::string>(false, "The transaction with id " + transaction.IdToString() + " is invalid, since the sender with public key '" + firstPublicKey.substr(0, 50) + "' wants to spend " + to_string(outputValue) + " coins with only " + to_string(inputValue) + " coins available."); 
        }
        // Verify the signature!
        std::string senderSignature = transaction.getSenderSig();
        std::string message = transaction.hashTransaction(); 
        bool verified = verifySignature(senderSignature, message, firstPublicKey);
        if (!verified) {
            return std::pair<bool, std::string>(false, "The transaction with id " + transaction.IdToString() + " is invalid, since the sender with public key '" + firstPublicKey.substr(0, 50) + "' has not set the signature on the transaction properly.");
        }
        return std::pair<bool,std::string>(true, "");
    }

/*************************************************
* Name:        verify (for one block)
*
* Description: Verify if the block is correct
*              given the balances in the UTXO.
*
* Arguments:   -Block block: block we
*               want to update our UTXO with
*
* Returns bool that tells us whether the verification was successful
* and a string that gives the reason if it is not
**************************************************/
    std::pair<bool,std::string> UTXO::verify(Block block) {
        std::set<std::pair<std::string,int>> allInputs;
        // Checking the transactions individually
        std::vector<Transaction> payload = block.getPayload();
        for (int i=0; i<payload.size(); ++i) {
             Transaction transaction = payload[i];
             // Crossing checking between transactions if same input is not used twice
             for (int j=0; j<transaction.getInputs().size(); ++j) {
                 Input input = transaction.getInputs()[j];
                 std::pair<std::string,int> pt(input.IDToString(), input.getOutputIndex());
                 // Adding the inputs to the set if not already present otherwise the block is invalid
                 if (allInputs.count(pt)) {
                    // The input is already present
                    return std::pair<bool,std::string>(false, "The block is invalid, since multiple transactions use the same input.");
                 } else {
                    // The input is not present
                    allInputs.insert(pt);
                 }
             }
             std::pair<bool,std::string> verifiedTransaction = verify(transaction);
             if (std::get<bool>(verifiedTransaction) == false) {
                 return std::pair<bool,std::string>(false, std::get<std::string>(verifiedTransaction));
             }
        }
        return std::pair<bool,std::string>(true, "");
    }

/*************************************************
* Name:        addPublicKey
*
* Description: Add the given public key to the UTXO
*              and setting the balance to zero.
*
* Arguments:   -string publicKey: the public key we
*               want to add to the UTXO
*
* Returns nothing
**************************************************/
    void UTXO::addUnspendOutput(std::string publicKey, previousTransaction pt, unsigned int value) {
        // Checking if the public key is already in the UTXO and if needed creating a new record
        std::pair<bool,unspendOutputs> out = getRecord(publicKey);
        if (std::get<bool>(out) == false) {
            // Adding the record to the UTXO with the given output
            unspendOutputs receiverOutputs;
            receiverOutputs.insert(std::pair<previousTransaction, unsigned int>(pt, value));
            utxo_[publicKey] = receiverOutputs;
        } 
        else {
            unspendOutputs receiverOutputs = std::get<unspendOutputs>(getRecord(publicKey));
            receiverOutputs.insert(std::pair<previousTransaction, unsigned int>(pt, value));
            utxo_.erase(publicKey);
            utxo_[publicKey] = receiverOutputs;
        }
    }

/*************************************************
* Name:        findCorrespondingPublicKey
*
* Description: Find the public key for a given previous
*              transaction id and output offset.
*
* Arguments:   -string transactionId: the id of the transaction
*               from which we want to find the public key
*              -unsigned int outputOffset: the output offset of the
*               output from which we want to find the public key
*
* Returns a boolean that is true if the corresponding public key has
* been found and returns the public key we need
**************************************************/
    std::pair<bool,std::string> UTXO::findCorrespondingPublicKey(std::string transactionId, unsigned int outputOffset) {
        previousTransaction pt(transactionId, outputOffset);
        std::string foundKey = "";
        for (const auto& record: getRecords()) {
            for (int i=0; i<record.second.size(); ++i) { 
                auto outputs = (std::get<unspendOutputs>(record));
                for (const auto& output: outputs) {
                    if (output.first == pt){
                        foundKey = std::get<const std::string>(record);
                        return std::pair<bool, std::string> (true, foundKey);
                    }
                }
            }
        }
        return std::pair<bool,std::string>(false, "");
    }

/*************************************************
* Name:        getNumberOfCoins
*
* Description: Find the number of coins at the output of
*              the public key for the given transaction id
*              and offset.
*
* Arguments:   -string publicKey: the public key for which we
*               want to know the number of coins
*              -string transactionId: the transaction id for which
*               we want to know the number of coins
*              -unsigned int outputOffset: the output offset for which
*               we want to know the number of coins
*
* Returns a boolean that is true if the combination has
* been found and returns the amount
**************************************************/
    std::pair<bool,unsigned int> UTXO::getNumberOfCoins(std::string publicKey, std::string transactionId, unsigned int outputOffset) {
        previousTransaction st(transactionId, outputOffset);
        if (std::get<bool>(getRecord(publicKey)) == true) {
            unspendOutputs record = std::get<unspendOutputs>(getRecord(publicKey));
            for (const auto& output: record) {
                if (output.first == st) {
                    return std::pair<bool,unsigned int>(true, output.second);
                }
            }
        }
        return std::pair<bool,unsigned int>(false, 0);
    }