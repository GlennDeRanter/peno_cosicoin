#ifndef NODE_H
#define NODE_H

#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

#include <iostream>
#include <memory>
#include <string>
#include <list>
#include "toml.hpp"
#include "Block.hpp"
#include "BlockChain.hpp"
#include "KeyPair.hpp"
#include "convert.hpp"
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <mutex>

using grpc::Channel;
using grpc::Status;
using grpc::ClientContext;
using grpc::ServerContext;
using grpc::Server;
using grpc::Status;
using consensus::Consensus;
using consensus::Ack;
using consensus::Ballot;
using consensus::BallotPayload;
using consensus::VoteBallot;
using consensus::VotePayload;
using consensus::EpochInit;
using consensus::SendTx; //milan
using consensus::WalletIdentification; //milan
using consensus::InputResponse; //milan
using consensus::Echo;


class Validator final : public Consensus::Service {

    private:

// The channel of the validator.
        std::shared_ptr<Consensus::Stub> channel_;
// The id of the validator.
        int userID_;
// The epoch number the validator is currently in.
        int epoch_ = 0;
// The location of the blockchain in allBlockChains_ 
// to which the proposed block belongs.
        int locationProposedChain_;
// The id of the validator that is the leader.
        int leaderID_;
// The amount of validators in the network.
        int nmbOfReceivingValidators_;
// The keypair of the validator.
        KeyPair keyPair_;
// The signature the validator wants to make on the block
// in order to vote on it.
        std::string voteSignature_;
// The address of the validator.
        std::string validatorAddress_;
// A sommation of all the possible blockchains the validator could append
// a block to.
        std::vector<BlockChain> allBlockChains_;
// Transactions that are not yet in a notarized block.
        std::vector<Transaction> pendingTransactions_;
// All the validators in the network.
        std::list<Validator> poolOfReceivingValidators_;
// All the possible proposed blocks that this validator has seen 
// through the implicitechoing and the propose serverside.
        std::vector<Block> proposedBlocksByAnyValidator_; 
// All blockchains for which there is a proposed block.
        std::vector<BlockChain> proposedChainsByAnyValidator_;
// A map of the public key of the other nodes linked to their id.
        std::map<int, std::string> pkOfOtherValidators_;
// Block that the validator itself has received from a propose command.       
        Block blockProposal_;
// Boolean that indicates whether this validator is currently the leader.
        bool isLeader_ = false;
// Boolean that indicates whether the validator has received a proposal.
        bool receivedProposal_ = false;
// Boolean that indicates whether the epoch has started.
        bool epochHasStarted_ = false;
// Boolean that indicates whether there is already  a new block added this epoch
        bool alreadyAddedToChain_ =false;
// Boolean that indicates that the proposedBlock may be added to the blockchain
        bool addProposedBlockToChain_ = false;
// Boolean that indicates whether there is a proposed block added to a chain this epoch or not
        bool addedBlockToChainThisEpoch_ = false;
// Boolean that indicates whether the validator is ready to send his vote.
        bool voteCanBeSend_ = false; 
// Boolean that indicates whether the validator has received a proposal
// but has not yet voted.   
        bool proposalReceivedButVoteNotSend_ = false;
// A mutual exclusive flag that acts as a gate keeper to a section of code.
        std::mutex m;

    public:

/*************************************************
* Name:        Constructor
*
* Description: Constructs the validator.
*
* Arguments:   -int userID: the id of the validator
*              -std::shared_ptr<Channel> channel: the channel
*               of the validator
*              -std::string validatorAddress: the address of the validator
*              -std::string publicKey: the public key of the validator
*              -std::string privateKey: the private key of the validator
*
* No Return value
**************************************************/   
        Validator(int userID, std::shared_ptr<Channel> channel, std::string validatorAddress, std::string publicKey, std::string privateKey)
            :userID_(userID)
            ,epoch_(0) 
            ,keyPair_(publicKey, privateKey)
            ,validatorAddress_(validatorAddress)
            ,leaderID_(0)
            ,locationProposedChain_(0)
            ,channel_(Consensus::NewStub(channel)){
                allBlockChains_.push_back(BlockChain());
                const toml::value data = toml::parse("setup.toml");
                const auto& table = toml::find(data, "nodes");
                auto nmbOfNodes = toml::find<std::string>(data, "numberOfNodes");
                int numberOfNodes = std::stoi(nmbOfNodes);
                setNmbOfReceivingValidators(numberOfNodes);        
                for(const auto& v : table.as_array()){
                        const auto nodeAddress = toml::find<std::string>(v, "ip_address");
                        const auto userId = toml::find<std::string>(v, "id");
                        const auto validKey = toml::find<std::string>(v, "public_key");
                        if (nodeAddress != validatorAddress_){
                                Validator client(grpc::CreateChannel(nodeAddress, grpc::InsecureChannelCredentials()), std::stoi(userId)); 
                                addValidatorToPoolOfValidators(client);
                                addPkToPkOfOtherValidators(std::stoi(userId), validKey);
                        }
                }       
        }
        
/*************************************************
* Name:        Constructor
*
* Description: Constructs the validator.
*
* Arguments:   -int userID: the id of the validator
*              -std::shared_ptr<Channel> channel: the channel
*               of the validator
*
* No Return value
**************************************************/  
        Validator(std::shared_ptr<Channel> channel, int userID)
            :channel_(Consensus::NewStub(channel))
            ,epoch_(0)
            ,validatorAddress_("")
            ,leaderID_(0)
            ,locationProposedChain_(0)
            ,userID_(userID){
                allBlockChains_.push_back(BlockChain());
            }
        
/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old validator
               in a new validator.
*
* Arguments:   -Validator const& validator: the
*               validator we want to copy
*
* No Return value
**************************************************/
        Validator(Validator const& validator)
            :channel_(validator.getChannel())
            ,userID_(validator.getUserID())
            ,epoch_(validator.getEpochNumber())
            ,leaderID_(validator.getLeaderID())
            ,locationProposedChain_(validator.getLocationProposedChain())
            ,validatorAddress_(validator.getValidatorAddress())
            ,allBlockChains_(validator.getBlockChainVector())
            ,proposedBlocksByAnyValidator_(validator.getProposedBlocks())
            ,proposedChainsByAnyValidator_(validator.getProposedChains())
            ,pkOfOtherValidators_(validator.getPkOfOtherValidators()) {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the validator is equal
*              to a given validator and otherwise 
*              makes sure they are.
*
* Arguments:   -Wallet const& obj: the
*               validator we want to match to
*
* Returns *this (success)
**************************************************/
        inline const Validator& operator=(Validator const& validator){return validator;}

/*************************************************
* Name:        sendVote
*
* Description: Function that does the client side of the rpc 
*              command vote.
*
* Arguments:   -const long int user: id of the validator who wishes to
*               send the vote
*              -const VotePayload payload: the block
*               we want to vote on
*              -const std::string signature: the signature of the 
*               validator on the block
*
* Returns a string that tells us whether or not the vote was successful
* and if not why not
**************************************************/
        std::string sendVote(const long int user, const VotePayload payload, const std::string signature);

/*************************************************
* Name:        sendProposal
*
* Description: Function that does the client side of the rpc 
*              command propose.
*
* Arguments:   -const long int id: id of the validator who wishes to
*               send the vote
*              -const VotePayload payload: the block
*               we want to vote on
*              -const std::string signature: the signature of the 
*               validator on the block
*
* Returns a string that tells us whether or not the proposal was successful
* and if not why not
**************************************************/
        std::string sendProposal(const long int user, const BallotPayload payload, const std::string signature);

/*************************************************
* Name:        sendImplicitEcho
*
* Description: Function that does the client side of the rpc 
*              command implicit echo.
*
* Arguments:   -const long int receiverID: the id of the receiving validator
*              -const long int senderID: the id of the sending validator
*              -const std::string echoType: the type of echo we want to do
*               (reaction on either a vote or a proposal)
*              -const BallotPayload payload: the block of which we need to send
*               the echo of
*              -const std::string merkelroot:The merkelroot of the transactions 
*               in the block
*              -const std::string signature: signature that is in the message (vote or
*               proposal) we want to echo
*
* Returns a string that tells us whether or not the echo was successful
* and if not why not
**************************************************/
        std::string sendImplicitEcho(const long int receiverID, const long int senderID, const std::string echoType, const BallotPayload payload, const std::string merkelroot, const std::string signature);

/*************************************************
* Name:        Propose
*
* Description: Function that does the server side of the Propose rpc command.
*              Here we program how a validator reacts when it receives a proposal.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const Ballot* proposal: all the information needed for the 
*               proposal
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the proposal was proposed successful
**************************************************/
        Status Propose(ServerContext* context, const Ballot* proposal, Ack* acknowledge) override;

/*************************************************
* Name:        Vote
*
* Description: Function that does the server side of the vote rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const VoteBallot* vote: all the information needed for the 
*               vote
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the vote was sent successful
**************************************************/
        Status Vote(ServerContext* context, const VoteBallot* vote, Ack* acknowledge) override;

/*************************************************
* Name:        ImplicitEcho
*
* Description: Function that does the server side of the implicit echo rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const Echo* message: all the information needed for the 
*               implicit echo (either for the echo of a proposal or a vote)
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the echo was sent successful
**************************************************/
        Status ImplicitEcho(ServerContext* context, const Echo* echo, Ack* acknowledge) override;

/*************************************************
* Name:        InitEpoch
*
* Description: Function that does the reaction of the validator to
*              the message of the heartbeat server that a new epoch
*              is going to start.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const EpochInit* epoch: a message containing the epoch number
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the initialisation of the epoch was sent successful
**************************************************/
        Status InitEpoch(ServerContext* context, const EpochInit* epoch, Ack* acknowledge) override;

/*************************************************
* Name:        StartEpoch
*
* Description: Function that makes sure the validator starts the epoch.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const EpochInit* epoch: a message containing the epoch number
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the start of the epoch was sent successful
**************************************************/
        Status StartEpoch(ServerContext* context, const EpochInit* epoch, Ack* acknowledge) override;

/*************************************************
* Name:        CommitTransaction
*
* Description: Function that does the server side of the transaction
*              rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const SendTx* transaction: a message containing the transaction
                the wallet wants to send to the validator
*              -Ack* acknowledge: the answer from the server to the client
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
        Status CommitTransaction(ServerContext* context, const SendTx* transaction, Ack* acknowledge) override;

/*************************************************
* Name:        Synchronize
*
* Description: Function that does the server side of the synchronization
*              rpc command.
*
* Arguments:   -ServerContext* context: the state information associated with
*               a certain client
*              -const WalletIdentification* request: a message containing the id
*               and public key of the wallet that wishes to synchronize
*              -InputResponse* response: a message containing the UTXO, whether
*               it is valid and from which validator it comes
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
        Status Synchronize(ServerContext* context, const WalletIdentification* request, InputResponse* response) override;

/*************************************************
* Name:        clientActions
*
* Description: Function that runs after the validator has started a server
*              and does all the client side of the validator.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        void clientActions();

/*************************************************
* Name:        leaderActions
*
* Description: Function that runs when a validator is the leader 
*              for this epoch
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void leaderActions(){
                std::cout << "DEBUG INFORMATION: Number of pending transactions BEFORE being leader: " << getPendingTransactions().size() << "." << std::endl;
                    //the leader searches the longest chain he has seen and remember it's location
                    findLongestBlockChain();
                    assert(getLocationProposedChain() < getBlockChainVector().size());
                    //save the longest chain in the his proposed chain value
                    BlockChain chainProposal = getBlockChainVector()[getLocationProposedChain()];
                    addProposedChain(chainProposal);
                    //it hashes the parentHash
                    std::string parentHash = chainProposal.hashPrefix();           
                    //creates the payload for the rpc command
                    BallotPayload payload;
                    payload.set_epoch_number(epoch_);
                    payload.set_parent_hash(parentHash);
                    std::vector<Transaction> proposedBlockTransactions = putTransactionsInProposedBlock();
                    payload = addTransactionsToPayload(payload, proposedBlockTransactions);
                    //creates locally a block for himself and he signs it for himself
                    Block blockProposal = Block(parentHash, epoch_, proposedBlockTransactions);
                    std::string signature = createValidatorSignature(blockProposal.hashBlock());
                    blockProposal.addSignatureToBlock(retrievePublicKey(), signature, getNmbOfReceivingValidators());
                    eraseChainOutOfVector(chainProposal);
                    addBlockToProposedBlock(blockProposal);                
                    //print the proposal
                    printProposal(proposedBlockTransactions);
                    //send the proposal to all the nodes.
                    for (auto& client: getPoolOfReceivingValidators()){
                        client.sendProposal(userID_, payload , signature);
                    }
                    //because he's done everything a leader should do the Validator becomes a normal Validator again
                    std::cout << "[VALIDATOR " << getUserID() << "] --> Finished all my tasks as leader for epoch " << getEpochNumber() << "." << std::endl;
                    std::cout << "DEBUG INFORMATION: Number of pending transactions AFTER being leader: " << getPendingTransactions().size() << "." << std::endl;
                    setEpochIsStarted(false);
        }


/*************************************************
* Name:        addProposedBlockToBlockChainActions
*
* Description: Function that runs when a validator 
*              has notarized a block so that it can 
*              grow it's blockchain for this epoch
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void addProposedBlockToBlockChainActions(){
                Block addBlockToChain;
                std::unique_lock<std::mutex> lock1(m, std::defer_lock);
                lock1.lock();
                for (int i = 0; i < getProposedBlocks().size(); ++i){
                    Block proposeBlock = getProposedBlocks()[i];
                    if (proposeBlock.isNotarized()){
                        eraseBlockOutProposedBlocks(i);
                        addBlockToChain = proposeBlock;
                    }
                }
                BlockChain addChain;
                for (auto& proposeChain: getProposedChains()){
                    if (proposeChain.hashPrefix() == addBlockToChain.getHashedPrefix()){
                        addChain = proposeChain;
                    }
                    else{
                        addChainToVector(proposeChain);
                    }
                }
                lock1.unlock();
                eraseChainOutOfVector(addChain);
                if (addChain.couldBeAddedToChain(addBlockToChain, true)){
                    std::cout << "DEBUG INFORMATION: Number of pending transactions BEFORE: " << getPendingTransactions().size() << "." << std::endl;
                    removeUsedTransactionsOutOfPendingTransaction(addBlockToChain.getPayload());
                    std::cout << "DEBUG INFORMATION: Number of pending transactions AFTER: " << getPendingTransactions().size() << "." << std::endl;
                    addChain.addBlockToChain(addBlockToChain);
                }
                addChainToVector(addChain);
                setAddedBlockToChainThisEpoch(true);
                setAddProposedBlockToChain(false);
        }


/*************************************************
* Name:        updateLeader
*
* Description: Function to find the next leader.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        void updateLeader();

/*************************************************
* Name:        verifyProposedChain
*
* Description: Function that verifies whether the proposed
*              chain is the longest and also signs the proposed block
*
* Arguments:   -BlockChain chainProposal: the proposedBlockchain
*               -int locationBiggestChain: the location where the biggest blockchain
*               was kept.
*
* Returns nothing
**************************************************/
        inline void verifyProposedChain (int locationBiggestChain, BlockChain chainProposal){
            //Checks whether the proposed chain is  the longest chain seen and adds the signature of the leader  
            if ((locationProposedChain_ == locationBiggestChain) && (chainProposal.couldBeAddedToChain(getBlockProposal(), false))){
                
                std::string signature = createValidatorSignature(blockProposal_.hashBlock());
                voteSignature_ = signature;
                addProposedChain(chainProposal);
                eraseChainOutOfVector(chainProposal);
                for (int i = 0; i < proposedBlocksByAnyValidator_.size(); ++i){
                    Block blocks = proposedBlocksByAnyValidator_[i];
                    if (blockProposal_.getHeader().getMerkelRoot() == blocks.getHeader().getMerkelRoot()){
                        eraseBlockOutProposedBlocks(i);
                        blocks.addSignatureToBlock(retrievePublicKey(), signature, getNmbOfReceivingValidators());
                        addBlockToProposedBlock(blocks);
                    }
                }
                
                voteCanBeSend_ = true;
            }
        }

/*************************************************
* Name:        addReceivedVoteSignature
*
* Description: Function that finds the block on which is voted
*              and adds the received signature to this block
*
* Arguments:   -std::string merkelroot: merkelroot of the block
*               to which the signature belongs
*              -int voterId: id of the validator that supposedly signed
*               the block
*              -std::string signature: signature we want to add
*
* Returns votedBlock
**************************************************/
        inline Block addReceivedVoteSignature(std::string merkelroot, int voterId, std::string signature){
                Block votedBlock;
                for (int i = 0; i < proposedBlocksByAnyValidator_.size(); ++i){
                        Block blockVote = proposedBlocksByAnyValidator_[i];
                        if (merkelroot == blockVote.getHeader().getMerkelRoot()){
                                eraseBlockOutProposedBlocks(i);
                                blockVote.addSignatureToBlock(findPublicKey(voterId), signature, nmbOfReceivingValidators_);
                                addBlockToProposedBlock(blockVote);
                                if (blockVote.isNotarized()){
                                        if (!alreadyAddedToChain_){
                                        addProposedBlockToChain_ = true;
                                        }
                                }
                                votedBlock = blockVote;
                        }
                }
                return votedBlock;
        }

/*************************************************
* Name:        printReceivedProposal
*
* Description: Function that prints the received proposal.
*
* Arguments:   -std::vector<Transaction> transactionsProposedBlock:
*               vector that contains all the 
*               transactions of the proposed block
*              -Ballot* proposal: the received information from 
*               the received grpc command
*
* Returns nothing
**************************************************/
    inline void printReceivedProposal(const Ballot* proposal, std::vector<Transaction> transactionsProposedBlock){
        std::cout << "[VALIDATOR " << getUserID() << "] --> Received a proposal from validator " << proposal->name()  << " for epoch " << (proposal -> payload()).epoch_number() << "." << std::endl;
        std::cout << "              --> Number of proposed transactions: " << transactionsProposedBlock.size() << std::endl;
        if (transactionsProposedBlock.size() > 0) {
            std::cout << "              --> ";
            for (auto i = 0; i < transactionsProposedBlock.size(); ++i) {
                if (i != (transactionsProposedBlock.size()-1)) {
                    std::cout << transactionsProposedBlock[i].IdToString() << ", ";
                } else {
                    std::cout << transactionsProposedBlock[i].IdToString() << std::endl;
                }    
            }
        }
    }

/*************************************************
* Name:        printProposal
*
* Description: Function that prints the proposal.
*
* Arguments:   -std::vector<Transaction> proposedBlockTransactions:
*               vector that contains all the 
*               transactions of the proposed block
*
* Returns nothing
**************************************************/
        inline void printProposal(std::vector<Transaction> proposedBlockTransactions){
                std::cout << "[VALIDATOR " << getUserID() << "] --> Proposed a block with " << proposedBlockTransactions.size() << " transaction(s) and transaction id(s): ";
                    for (auto i = 0; i < proposedBlockTransactions.size(); ++i) {
                        if (i != (proposedBlockTransactions.size()-1)) {
                            std::cout << proposedBlockTransactions[i].IdToString() << ", ";
                        } else {
                            std::cout << proposedBlockTransactions[i].IdToString() << ".";
                        }    
                    }
                std::cout << "" << std::endl;
        }

/*************************************************
* Name:        cleanProposedChains
*
* Description: Function that removes all the proposed chains.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void cleanProposedChains(){
                if (addedBlockToChainThisEpoch_){
                        proposedChainsByAnyValidator_.clear(); 
                }
                else{
                        while(!proposedChainsByAnyValidator_.empty()){
                               BlockChain chain =  proposedChainsByAnyValidator_.back();
                               proposedChainsByAnyValidator_.pop_back();
                               addChainToVector(chain);
                        }
                }
                proposedChainsByAnyValidator_.shrink_to_fit();}

/*************************************************
* Name:        addChainToVector
*
* Description: Function that adds a chain to the vector
*              with all the chains the validator has seen.
*
* Arguments:   -BlockChain chain: the blockchain we want to add
*
* Returns nothing
**************************************************/
        inline void addChainToVector(BlockChain chain){
                bool alreadyInChain = false;
                for (int i= 0; i < allBlockChains_.size(); ++i){
                        BlockChain proposeChain = allBlockChains_[i];
                        if (proposeChain.hashPrefix() == chain.hashPrefix()){
                                allBlockChains_.erase(allBlockChains_.begin() + i);
                                allBlockChains_.push_back(chain);
                                alreadyInChain = true;
                        }
                }
                if (!alreadyInChain){        
                        allBlockChains_.push_back(chain);
                }
        }

/*************************************************
* Name:        addBlockToProposedBlock
*
* Description: Add a block to all the proposed blocks the
*              validator has seen.
*
* Arguments:   -Block block: the block we want to add
*
* Returns nothing
**************************************************/
        inline void addBlockToProposedBlock(Block block){
                bool alreadyInChain = false;
                for (int i= 0; i < proposedBlocksByAnyValidator_.size(); ++i){
                        Block proposeBlock = proposedBlocksByAnyValidator_[i];
                        if (proposeBlock.getHeader().getMerkelRoot() == block.getHeader().getMerkelRoot()){
                                proposedBlocksByAnyValidator_.erase(proposedBlocksByAnyValidator_.begin() + i);
                                proposedBlocksByAnyValidator_.push_back(block);
                                alreadyInChain = true;
                        }
                }
                if (!alreadyInChain){        
                        proposedBlocksByAnyValidator_.push_back(block);
                }
        }

/*************************************************
* Name:        cleanProposedBlocks
*
* Description: Function that removes all blocks out of
*              the proposed blocks vector.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void cleanProposedBlocks(){proposedBlocksByAnyValidator_.clear(); proposedBlocksByAnyValidator_.shrink_to_fit();}

/*************************************************
* Name:        eraseBlockOutProposedBlocks
*
* Description: Function that removes a single block out of
*              the proposed blocks vector.
*
* Arguments:   -int i: the value added to the 
*               iterator at the starting position
*
* Returns nothing
**************************************************/
        inline void eraseBlockOutProposedBlocks(int i) {proposedBlocksByAnyValidator_.erase(proposedBlocksByAnyValidator_.begin() + i);}


/*************************************************
* Name:        eraseChainOutOfVector
*
* Description: Function that erases a chain out of the vector
*              at a certain place.
*
* Arguments:   -int i:the value added to the iterator 
*               at the starting position
*
* Returns nothing
**************************************************/
        inline void eraseChainOutOfVector(BlockChain chain){
                for (int i = 0; i < allBlockChains_.size(); ++i){
                        BlockChain proposeChain = allBlockChains_[i];
                        if (proposeChain.hashPrefix() == chain.hashPrefix()){
                                allBlockChains_.erase(allBlockChains_.begin() + i);
                        }
                }
        }

/*************************************************
* Name:        findPublicKey
*
* Description: Function to find the public key of 
*              a validator.
*
* Arguments:   -int userID: the id of the validator of whom
*               we want to find the public key
*
* Returns key
**************************************************/
        inline std::string findPublicKey(int userID){
            std::string key = pkOfOtherValidators_.find(userID) -> second;
            return key;
        }

/*************************************************
* Name:        findLongestBlockChain
*
* Description: Function to find the longest blockchain in 
*              a validator.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void findLongestBlockChain(){
                int biggestChainLength = 0;
                for (long unsigned int i = 0; i < getBlockChainVector().size(); ++i){
                        BlockChain chain = getBlockChainVector()[i];
                        if ((chain.getChainLength() > biggestChainLength) && (chain.getBlocks().back().isNotarized())){
                                biggestChainLength = chain.getChainLength();
                                setLocationProposedChain(i);
                        }
                }
        }
/*************************************************
* Name:        commitTransactionStatusUpdate
*
* Description: Function to give the user the result of the
*              attempt to commit a transaction.
*
* Arguments:   -std::string walletId: the id of the wallet that
*               submitted the transaction
*              -Transaction receivedTransaction: the transaction
*               that we wanted to commit
*              -std::string: the transaction id of the transaction
*              -bool duplicate: a boolean that indicates whether
*               this transaction was not already stored
*              -bool valid: a boolean that indicates whether the transaction
*               was a valid one
*
* Returns nothing
**************************************************/
        inline void commitTransactionStatusUpdate(std::string walletId, Transaction receivedTransaction, std::string tx, bool duplicate, bool valid){
                if (duplicate) {
                        std::cout << "[VALIDATOR " << getUserID() << "] --> Did not store duplicate transaction from wallet " << walletId << " with transaction id: " << tx << std::endl;
                } 
                if (!valid) {
                        std::cout << "[VALIDATOR " << getUserID() << "] --> Did not store invalid transaction from wallet " << walletId << " with transaction id: " << tx << std::endl;
                }     
                if (!duplicate && valid) {
                        pendingTransactions_.push_back(receivedTransaction);
                        std::cout << "[VALIDATOR " << getUserID() << "] --> Stored transaction from wallet " << walletId << " with transaction id: " << tx << std::endl;
                }
        }

/*************************************************
* Name:        addProposedChain
*
* Description: Function that adds a proposed chain to all
*              the proposed chains.
*
* Arguments:   -BlockChain chain: the chain we want to add
*
* Returns nothing
**************************************************/
        inline void addProposedChain(BlockChain chain){
                bool alreadyInChain = false;
                for (int i= 0; i < proposedChainsByAnyValidator_.size(); ++i){
                        BlockChain proposeChain = proposedChainsByAnyValidator_[i];
                        if (proposeChain.hashPrefix() == chain.hashPrefix()){
                                proposedChainsByAnyValidator_.erase(proposedChainsByAnyValidator_.begin() + i);
                                proposedChainsByAnyValidator_.push_back(chain);
                                alreadyInChain = true;
                        }
                }
                if (!alreadyInChain){        
                        proposedChainsByAnyValidator_.push_back(chain);
                }
        }
 
/*************************************************
* Name:        createValidatorSignature
*
* Description: Function that creates a signature
*              of the validator on the block.
*
* Arguments:   -std::string hashedBlock: the hash of the
*               block we want to sign
*
* Returns the signed version of the hash of the block
**************************************************/
        inline std::string createValidatorSignature(std::string hashedBlock){
            return keyPair_.createSignature(hashedBlock, "admin");
        }

/*************************************************
* Name:        putTransactionsInProposedBlock
*
* Description: Function that returns all the pending 
*              transactions and deletes them out of the vector.
*
* Arguments:   None
*
* Returns transactions
**************************************************/
        inline std::vector<Transaction> putTransactionsInProposedBlock() {
            std::vector<Transaction> transactions = pendingTransactions_;
            pendingTransactions_.clear();
            pendingTransactions_.shrink_to_fit();
            return transactions;

        }

/*************************************************
* Name:        addValidatorToPoolOfValidators
*
* Description: Function that adds a validator to the list
*              with all the other validators in the network.
*
* Arguments:   -Validator client: the validator we want to add
*
* Returns nothing
**************************************************/
        inline void addValidatorToPoolOfValidators(Validator client){poolOfReceivingValidators_.push_front(client);}

/*************************************************
* Name:        addTransactionsToPayload
*
* Description: Function that adds the transaction to the payload
*              
*
* Arguments:   -BallotPayload payload: payload thats gonna be send
*               through a grpc command
*              -std::vector<Transaction> transactions: 
*               All the transactions that need to be added to payload
*               
*
* Returns payload
**************************************************/ 
        inline BallotPayload addTransactionsToPayload(BallotPayload payload, std::vector<Transaction> transactions){
           for (auto& transaction: transactions){
               consensus::SendTx* init = payload.add_transactions();
               init->set_sender_sig(transaction.getSenderSig());
               init->set_tx_id(transaction.IdToString());
               for (auto& input: transaction.getInputs()) {
                   consensus::SendTx::Input* in = init->add_inputs();
                   in->set_previous_tx_id(input.IDToString());
                   in->set_output_index(input.getOutputIndex());
               }
        
               for (auto& output: transaction.getOutputs()) {
                   consensus::SendTx::Output* out = init->add_outputs();
                   out->set_value(output.getValue());
                   out->set_receiver_pk(output.getReceiverPk());
               }
           }
           return payload;
        }

/*************************************************
* Name:        moveTransactionsFromPayload
*
* Description: Function that removes transactions out of the payload
*              and those transactions are put in a vector. 
*             
*
* Arguments:   -BallotPayload payload: 
*               this is the payload from a grpc command
*
* Returns Transactions
**************************************************/
    inline std::vector<Transaction> moveTransactionsFromPayload(BallotPayload payload){
        std::vector<Transaction> transactions;
        for (int m = 0; m < payload.transactions_size(); ++m){
            consensus::SendTx transaction = payload.transactions(m);
            // Reassemble the transaction
            // Main part
            std::string senderSigOfTransaction = transaction.sender_sig();
            std::string tx = transaction.tx_id();
            std::vector<unsigned char> txIdOfTransaction(tx.begin(), tx.end());
            // Inputs
            std::vector<Input> inputsOfTransaction;
            for (int i = 0; i < transaction.inputs_size(); i++) {
                const consensus::SendTx::Input& input = transaction.inputs(i);
                std::string str = input.previous_tx_id();
                unsigned int outputIndex = input.output_index();
                inputsOfTransaction.push_back(Input(std::vector<unsigned char>(str.begin(), str.end()), outputIndex));
            }
            // Outputs
            std::vector<Output> outputsOfTransaction;
            for (int i = 0; i < transaction.outputs_size(); i++) {
                const consensus::SendTx::Output& output = transaction.outputs(i);
                unsigned int value = output.value();
                std::string receiverPk = output.receiver_pk();
                outputsOfTransaction.push_back(Output(value, receiverPk));
            }
            Transaction receivedTransaction(inputsOfTransaction, outputsOfTransaction, senderSigOfTransaction, txIdOfTransaction);
            transactions.push_back(receivedTransaction);

        }
        return transactions;
    }

/*************************************************
* Name:        removeUsedTransactionsOutOfPendingTransaction
*
* Description: Function that removes transactions out of the pending
*              transactions if they are present in the list of the 
*              pending transactions of the validator.
*
* Arguments:   -std::vector<Transaction> proposedTransactions: the transactions
*               we wish to remove from the list of pending transactions
*
* Returns nothing
**************************************************/
        inline void removeUsedTransactionsOutOfPendingTransaction(std::vector<Transaction> proposedTransactions){
            for (auto& transaction: proposedTransactions){
                int i = 0;
                for (auto& pendingTransaction: pendingTransactions_){

                    if (transaction.getId() == pendingTransaction.getId()){
                        pendingTransactions_.erase(pendingTransactions_.begin()+i);
                    }
                    ++i;
                }
            }
        }

/*************************************************
* Name:        retrieveProposedBlocks
*
* Description: Function that retrieves the vector proposed blocks
*              so that it can be adjusted.
*
* Arguments:   None
*
* Returns proposedBlocksByAnyValidator_
**************************************************/
        inline std::vector<Block> retrieveProposedBlocks() {return proposedBlocksByAnyValidator_;}

/*************************************************
* Name:        retrieveProposedChains
*
* Description: Function that retrieves the vector proposed
*              chains so that they can be adjusted.
*
* Arguments:   None
*
* Returns proposedChainsByAnyValidator_
**************************************************/
        inline std::vector<BlockChain> retrieveProposedChains() {return proposedChainsByAnyValidator_;}

/*************************************************
* Name:        getValidatorPublicKeys
*
* Description: Function to get all the public keys of 
*              all the validators that are known to the validator.
*
* Arguments:   None
*
* Returns nodePublicKeys_
**************************************************/
        inline std::vector<std::string> getValidatorPublicKeys() {
                std::vector<std::string> allPublicKeys;
                for (const auto& [id, publicKey] : pkOfOtherValidators_) {
                        allPublicKeys.push_back(publicKey);
                }
                std::string ownPublicKey = keyPair_.getPublicKey();
                allPublicKeys.push_back(ownPublicKey);
                return allPublicKeys;
        }        

/*************************************************
* Name:        addPkToPkOfOtherValidators
*
* Description: Function that inserts a validators public
*              key to the map of other keys and ids.
*
* Arguments:   -int userId: the id of the validator we want
*               to add
*              -std::string publicKey: the public key we want
*               to add
*
* Returns nothing
**************************************************/
        inline void addPkToPkOfOtherValidators(int userID, std::string publicKey){pkOfOtherValidators_[userID] = publicKey;}

/*************************************************
* Name:        retrievePublicKey
*
* Description: Function that retrieves the validator own
*              public key.
*
* Arguments:   None
*
* Returns the public key of the validator
**************************************************/
        inline std::string retrievePublicKey() {return keyPair_.getPublicKey();}
        
/*************************************************
* Name:        retrieveLocationLongestBlockChains
*
* Description: Function that retrieves the location of the longest
*              blockchains of the validator and the proposed BlockChain
*
* Arguments:   -BallotPayload payload: this is the payload of an grpc command
*
* Returns std::vector<int> longestChains and chainProposal
**************************************************/
        inline std::pair<BlockChain, std::vector<int>> retrieveLocationLongestBlockChains(BallotPayload payload){
                int locationBiggestChain;
                int biggestChainLength = 0;
                std::vector<int> chainsOfSameLength;
                BlockChain chainProposal;
                for (long unsigned int i =0; i < allBlockChains_.size(); ++i){
                        BlockChain chain = allBlockChains_[i];
                        
                        //Checks whether this chain is longer than the longest chain it already checks and if the latest block is notarised, 
                        //If it is longer or equal than the variables locationBiggestChain and biggestChainLength are updated
                        if ((chain.getChainLength() >= biggestChainLength)&&(chain.getBlocks().back().isNotarized())){
                
                                //If it is the same length then we add the location in the vector that keeps all the location of the longest chains
                                if (chain.getChainLength() == biggestChainLength){
                                chainsOfSameLength.push_back(i);
                
                                //if it is longer than we erase all the values out of the vector and put the new location in it
                                } else{
                                        chainsOfSameLength.erase(chainsOfSameLength.begin(),chainsOfSameLength.end());
                                        chainsOfSameLength.push_back(i);
                                }
                                //we save the location and the length of the latest chain if it was longer
                                biggestChainLength = chain.getChainLength();
                                locationBiggestChain = i;  
                        }
                        //we call the function that automatically hashes the latest block of the chain
                        std::string hashChain = chain.hashPrefix();

                        //if the hash of the latest block equals the parenthash from the proposal than we know this is the chain the leader wants to extend
                        if (hashChain == payload.parent_hash()){
                                locationProposedChain_ = i;
                                chainProposal = chain;
                        }
                }
                return std::pair<BlockChain, std::vector<int>>(chainProposal, chainsOfSameLength);
        }
    public:

/*************************************************
* Name:        findWalletRecord
*
* Description: Function that searches for the record of the
*              wallet (public key) in the UTXO of the longest
*              chain(s).
*
* Arguments:   -std::string walletId: the id of the wallet we want to 
*               find the record of
*              -std::string publicKey: the public key of the wallet we want
*               to find the record of
*              -InputResponse* response: a message containing the UTXO, whether
*               it is valid and from which validator it comes
*              -std::vector<int> largestChainPositions: the positions of the largest
*               blockchains in the vector with all the blockchain 
*
* Returns the status on whether the transaction was committed successfully
**************************************************/
        void findWalletRecord(std::string walletId, std::string publicKey, InputResponse* response, std::vector<int> largestChainPositions);

/*************************************************
* Name:        reassembleTransaction
*
* Description: Function that searches for the record of the
*              wallet (public key) in the UTXO of the longest
*              chain(s).
*
* Arguments:   -const SendTx: the transaction from the grpc message
*               that we need to reassemble
*
* Returns {receivedTransaction,tx, walletId, txIdOfTransaction}
**************************************************/
        inline auto reassembleTransaction(const SendTx* transaction){
                std::string walletId = transaction->wallet_id();
                std::string senderSigOfTransaction = transaction->sender_sig();
                std::string tx = transaction->tx_id();
                std::vector<unsigned char> txIdOfTransaction(tx.begin(), tx.end());
                std::cout << "[VALIDATOR " << getUserID() << "] --> Received a transaction from wallet " << walletId << " with transaction ID: " << tx << std::endl;
                // Inputs
                std::vector<Input> inputsOfTransaction;
                for (int i = 0; i < transaction->inputs_size(); i++) {
                        const consensus::SendTx::Input& input = transaction->inputs(i);
                        std::string str = input.previous_tx_id();
                        unsigned int outputIndex = input.output_index();
                        inputsOfTransaction.push_back(Input(std::vector<unsigned char>(str.begin(), str.end()), outputIndex));
                }
                // Outputs
                std::vector<Output> outputsOfTransaction;
                for (int i = 0; i < transaction->outputs_size(); i++) {
                        const consensus::SendTx::Output& output = transaction->outputs(i);
                        unsigned int value = output.value();
                        std::string receiverPk = output.receiver_pk();
                        outputsOfTransaction.push_back(Output(value, receiverPk));
                }     
                //Received transaction
                Transaction receivedTransaction(inputsOfTransaction, outputsOfTransaction, senderSigOfTransaction, txIdOfTransaction);
                struct retVals{
                        Transaction transaction;
                        std::string tx, walletId;
                        std::vector<unsigned char> txIdOfTransaction;
                };
                return retVals {receivedTransaction,tx, walletId, txIdOfTransaction};
        }

/*************************************************
* Name:        findLargestChainPositions
*
* Description: Function that finds the positions of the
*              largest chains in the all chains vector.
*
* Arguments:   None
*
* Returns largestChainPositions
**************************************************/       
        inline std::vector<int> findLargestChainPositions(){
                int largestChainLength = 0;
                std::vector<int> largestChainPositions;
                for (long unsigned int i = 0; i < getBlockChainVector().size(); ++i){
                        BlockChain chain = getBlockChainVector()[i];
                        if (chain.getBlocks().back().isNotarized()) {
                                if (chain.getChainLength() > largestChainLength) {
                                        largestChainLength = chain.getChainLength();
                                        largestChainPositions.clear();
                                        largestChainPositions.push_back(i);
                                } else if (chain.getChainLength() == largestChainLength) {
                                        largestChainPositions.push_back(i);
                                }
                        }
                }   
                return largestChainPositions;
        }

/*************************************************
* Name:        getChannel
*
* Description: Function to get the channel of the validator.
*
* Arguments:   None
*
* Returns channel_
**************************************************/
        inline std::shared_ptr<Consensus::Stub> getChannel() const {return channel_;}

/*************************************************
* Name:        getUserID
*
* Description: Function to get the user id of the validator.
*
* Arguments:   None
*
* Returns userID_
**************************************************/
        inline int getUserID() const{return userID_;}

/*************************************************
* Name:        setUserID
*
* Description: Function to set the user id of the validator.
*
* Arguments:   -int newUserID = the user id we want to assign
*               to the validator
*
* Returns nothing
**************************************************/
        inline void setUserID(int newUserID){userID_ = newUserID;}
        
/*************************************************
* Name:        getEpochNumber
*
* Description: Function to get the epoch the validator
*              is currently at.
*
* Arguments:   None
*
* Returns epoch_
**************************************************/
        inline int getEpochNumber() const{return epoch_;}

/*************************************************
* Name:        setEpochNumber
*
* Description: Function to set the epoch the validator
*              is currently at.
*
* Arguments:   -int newEpochValue = the epoch we want the 
*               validator to be in
*
* Returns nothing
**************************************************/
        inline void setEpochNumber(int newEpochValue){epoch_ = newEpochValue;}

/*************************************************
* Name:        getLocationProposedChain
*
* Description: Function to get the location of the 
*              proposed chain.
*
* Arguments:   None
*
* Returns locationProposedChain_
**************************************************/
        inline int getLocationProposedChain() const{return locationProposedChain_;}

/*************************************************
* Name:        setLocationProposedChain
*
* Description: Function to set the location of the 
*              proposed chain.
*
* Arguments:   -int newLocation = the location we 
*               think the proposed chain is in 
*
* Returns nothing
**************************************************/
        inline void setLocationProposedChain(int newLocation) {locationProposedChain_ = newLocation;}

/*************************************************
* Name:        getLeaderID
*
* Description: Function to get the id of the validator
*              that is currently the leader.
*
* Arguments:   None
*
* Returns leaderID_
**************************************************/
        inline int getLeaderID() const{return leaderID_;}

/*************************************************
* Name:        setLeaderID
*
* Description: Function to set the id of the validator
*              that is currently the leader.
*
* Arguments:   -int newID = the new id for the leader
*
* Returns nothing
**************************************************/
        inline void setLeaderID(int newID){leaderID_ = newID;}

/*************************************************
* Name:        getNmbOfReceivingValidators
*
* Description: Function to get the number of
*              validators in the network.
*
* Arguments:   None
*
* Returns nmbOfReceivingValidators_
**************************************************/
        inline int getNmbOfReceivingValidators() const{return nmbOfReceivingValidators_;}

/*************************************************
* Name:        setNmbOfReceivingValidators
*
* Description: Function to set the number of
*              validators in the network.
*
* Arguments:   -int newNmb = the new number of validators
*               in the network
*
* Returns nothing
**************************************************/
        inline void setNmbOfReceivingValidators(int newNmb){nmbOfReceivingValidators_ = newNmb;}

/*************************************************
* Name:        getVoteSignature
*
* Description: Function to get the vote signature.
*
* Arguments:   None
*
* Returns voteSignature_
**************************************************/
        inline std::string getVoteSignature() const{return voteSignature_;}

/*************************************************
* Name:        setVoteSignature
*
* Description: Function to set the vote signature.
*
* Arguments:   -std::string newSig: the signature
*               we want to set the vote signature to
*
* Returns nothing
**************************************************/
        inline void setVoteSignature(std::string newSig){voteSignature_ = newSig;}

/*************************************************
* Name:        getValidatorAddress
*
* Description: Function to get the validator address.
*
* Arguments:   None
*
* Returns validatorAddress_
**************************************************/
        inline std::string getValidatorAddress() const{return validatorAddress_;}

/*************************************************
* Name:        setValidatorAddress
*
* Description: Function to set the validator address.
*
* Arguments:   -int newNodeAddress: the new address
*               we want the validator to have
*
* Returns nothing
**************************************************/
        inline void setValidatorAddress(int newNodeAddress){validatorAddress_ = newNodeAddress;}    

/*************************************************
* Name:        getBlockChainVector
*
* Description: Function to get the vector wit all
*              the blockchains.
*
* Arguments:   None
*
* Returns allBlockChains_
**************************************************/
        inline std::vector<BlockChain> getBlockChainVector() const{return allBlockChains_;}

/*************************************************
* Name:        getPendingTransactions
*
* Description: Function to get all the transactions
*              that have not yet been added to a 
*              notarized block.
*
* Arguments:   None
*
* Returns pendingTransactions_
**************************************************/
        inline std::vector<Transaction> getPendingTransactions() const{return pendingTransactions_;}

/*************************************************
* Name:        getPoolOfReceivingValidators
*
* Description: Function to get all the validators
*              in the network.
*
* Arguments:   None
*
* Returns poolOfReceivingValidators_
**************************************************/
        inline std::list<Validator> getPoolOfReceivingValidators() const{return poolOfReceivingValidators_;}

/*************************************************
* Name:        getProposedBlocks
*
* Description: Function to get all the blocks that 
*              have been proposed in the epoch.
*
* Arguments:   None
*
* Returns proposedBlocksByAnyValidator_
**************************************************/
        inline std::vector<Block> getProposedBlocks() const{return proposedBlocksByAnyValidator_;}

/*************************************************
* Name:        getPkOfTheOtherValidators
*
* Description: Function to get the map with every
*              valid validator public key and its
*              user id.
*
* Arguments:   None
*
* Returns pkOfOtherValidators_
**************************************************/
        inline std::map<int, std::string> getPkOfOtherValidators() const{return pkOfOtherValidators_;}

/*************************************************
* Name:        getProposedChains
*
* Description: Function to get all the chains that the
*              validator has seen and that correspond
*              to a proposed block.
*
* Arguments:   None
*
* Returns proposedChains
**************************************************/ 
        inline std::vector<BlockChain> getProposedChains() const{return proposedChainsByAnyValidator_;}

/*************************************************
* Name:        getBlockProposal
*
* Description: Function to get the block that was proposed
*              by the leader.
*
* Arguments:   None
*
* Returns blockProposal
**************************************************/
        inline Block getBlockProposal() const{return blockProposal_;}

/*************************************************
* Name:        setBlockProposal
*
* Description: Function to set the block that was proposed
*              by the leader.
*
* Arguments:   -Block newProposal: the new block proposal
*               from the leader
*
* Returns nothing
**************************************************/  
        inline void setBlockProposal(Block newProposal) {
            bool receivedEchoProposal = false;
            for (int i = 0; i < proposedBlocksByAnyValidator_.size(); ++i){
                Block proposeBlock = proposedBlocksByAnyValidator_[i];
                if (proposeBlock.getHeader().getMerkelRoot() == newProposal.getHeader().getMerkelRoot()){ 
                    blockProposal_ = proposeBlock;
                    proposedBlocksByAnyValidator_.erase(proposedBlocksByAnyValidator_.begin() + i);
                    receivedEchoProposal = true;
                } 
            }
            if(!receivedEchoProposal){ 
                blockProposal_ = newProposal;
            } 
        }

/*************************************************
* Name:        getIsLeader
*
* Description: Function to get the boolean that indicates
*              whether or not the validator is currently
*              the leader.
*
* Arguments:   None
*
* Returns isLeader_
**************************************************/
        inline bool getIsLeader() const{return isLeader_;}

/*************************************************
* Name:        setIsLeader
*
* Description: Function to set the boolean that indicates
*              whether or not the validator is currently
*              the leader.
*
* Arguments:   -bool newIsLeader: boolean that will be the 
*               new indication on whether or not the validator
*               is the leader
*
* Returns nothing
**************************************************/
        inline void setIsLeader(bool newIsLeader){isLeader_ = newIsLeader;}

/*************************************************
* Name:        getReceivedProposal
*
* Description: Function to check whether the validator
*              has already received a proposal.
*
* Arguments:   None
*
* Returns receivedProposal_
**************************************************/
        inline bool getReceivedProposal() const{return receivedProposal_;}

/*************************************************
* Name:        setReceivedProposal
*
* Description: Function to change whether the validator
*              has already received a proposal.
*
* Arguments:   -bool newReceived: the boolean that indicates
*               whether the validator has received a proposal
*
* Returns nothing
**************************************************/
        inline void setReceivedProposal(bool newReceived){receivedProposal_ = newReceived;}

/*************************************************
* Name:        getEpochIsStarted
*
* Description: Function to check whether the epoch
*              has started.
*
* Arguments:   None
*
* Returns epochHasStarted_
**************************************************/
        inline bool getEpochIsStarted() const{return epochHasStarted_;}

/*************************************************
* Name:        setEpochIsStarted
*
* Description: Function to change whether the epoch
*              has started.
*
* Arguments:   -bool isStarted: boolean that indicates
*               whether the epoch has started
*
* Returns nothing
**************************************************/
        inline void setEpochIsStarted(bool isStarted){epochHasStarted_ = isStarted;}

/*************************************************
* Name:        getAddProposedBlockToChain
*
* Description: Function that checks whether a block 
*              may be added to the proposed chain.
*
* Arguments:   None
*
* Returns addedProposedBlock_
**************************************************/
        inline bool getAddProposedBlockToChain() const{return addProposedBlockToChain_;}

/*************************************************
* Name:        setAddProposedBlockToChain
*
* Description: Function that changes whether a block 
*              may be added to the proposed chain.
*
* Arguments:   -bool newAdded: bool that determines
*               if a block may be added to the proposed chain
*
* Returns nothing
**************************************************/
        inline void setAddProposedBlockToChain(bool newAdded) {addProposedBlockToChain_ = newAdded;}

/*************************************************
* Name:        getAddedBlockToChainThisEpoch
*
* Description: Function that checks whether a block 
*              has been added to the proposed chain this epoch
*
* Arguments:   None
*
* Returns addedBlockToChainThisEpoch_
**************************************************/
        inline bool getAddedBlockToChainThisEpoch() const{return addedBlockToChainThisEpoch_;}

/*************************************************
* Name:        setAddedBlockToChainThisEpoch
*
* Description: Function that changes whether a block 
*              has been added this epoch
*
* Arguments:   -bool newAdded: bool that determines
*               if a block is added to the chain this epoch
*
* Returns nothing
**************************************************/
        inline void setAddedBlockToChainThisEpoch(bool newAdded) {addedBlockToChainThisEpoch_ = newAdded;}

/*************************************************
* Name:        getVoteCanBeSend
*
* Description: Function to check whether the validator
*              has already voted or not.
*
* Arguments:   None
*
* Returns voteCanBeSend_
**************************************************/
        inline bool getVoteCanBeSend() const{return voteCanBeSend_;}

/*************************************************
* Name:        setVoteCanBeSend
*
* Description: Function to change whether the proposed
*              block has been added to the chain.
*
* Arguments:   -bool newSend: boolean that indicates
*               whether the validator has sent a vote
*
* Returns nothing
**************************************************/
        inline void setVoteCanBeSend(bool newSend) {voteCanBeSend_ = newSend;}

/*************************************************
* Name:        getAlreadyAdded
*
* Description: Function that checks whether a block 
*              is already added to the proposed chain.
*
* Arguments:   None
*
* Returns alreadyAddedToChain_
**************************************************/
        inline bool getAlreadyAdded() const{return alreadyAddedToChain_;}

/*************************************************
* Name:        setAlreadyAdded
*
* Description: Function that changes whether a block 
*              is already added to the proposed chain.
*
* Arguments:   -bool newAlreadyAdded: bool that determines 
*               if a block is already added to the proposed chain
*
* Returns nothing
**************************************************/
        inline void setAlreadyAdded(bool newAlreadyAdded) {alreadyAddedToChain_ = newAlreadyAdded;}

/*************************************************
* Name:        getProposalReceivedButVoteNotSend
*
* Description: Function to check whether the validator
*              has received a proposal and needs to
*              block the echo response as he has not yet
*              voted.
*
* Arguments:   None
*
* Returns proposalReceivedButVoteNotSend_
**************************************************/
        inline bool getProposalReceivedButVoteNotSend() const{return proposalReceivedButVoteNotSend_;}

/*************************************************
* Name:        setProposalReceivedButVoteNotSend
*
* Description: Function to change whether the validator
*              has received a proposal and needs to
*              block the echo response as he has not yet
*              voted.
*
* Arguments:   -bool newSend: boolean that indicates whether
*               the validator has received a proposal but has
*               not yet voted on it
*
* Returns nothing
**************************************************/
        inline void setProposalReceivedButVoteNotSend(bool newSend) {proposalReceivedButVoteNotSend_ = newSend;}


};

/*************************************************
* Name:        replaceValidatorAddressForDirectoryName
*
* Description: replaces the ':' with an underscore so that 
*              the adress can be used to acces a directory
*               where the keypair is kept
* Arguments:   -std::string validatorAddress:
*               the IP address 
*               followed by a port number
*               
*
* Returns the adjusted validatorAddress
**************************************************/    
inline std::string replaceValidatorAdressForDirectoryName(std::string validatorAddress){
    std::string::size_type n;
    n = validatorAddress.find(":");
    validatorAddress.replace(n,1,"_");
    return validatorAddress;
}

/*************************************************
* Name:        replaceLocalAddressTonetworkAddress
*
* Description: replaces the '0.0.0.0:' with the network address so that 
*              the string can be used to find the value in the toml file
* Arguments:   -std::string localAddress:
*               the localAddress is the adress 
*               where the IP is 0.0.0.0
*               -std::string networkAddress:
*               this has the actual IP address
* Returns the adjusted nodeaddress
**************************************************/    
inline std::string replaceLocalAddressTonetworkAddress(std::string localAddress, std::string networkAddress){
    std::string::size_type n;
    n = localAddress.find("0.0.0.0:");
    localAddress.replace(n,7,networkAddress);
    return localAddress;
}

/*************************************************
* Name:        pushFileIntoString
*
* Description: returns a string that contains the info 
*              inside a txt file.
* Arguments:   -std::string validatorAddress:
*               the IP adress 
*               followed by a port number
*              -std::string keyType:
*               string that contains which type of key
*               is put inside te string
*               - std::string userID:
*               int that resembles the userID
*
* Returns the string
**************************************************/   
inline std::string pushFileIntoString(std::string validatorAddress, std::string keyType, std::string userID){
    std::string fileInfo;
    std::string headdirectory = "keyPairs/";
    std::string fileExtension = ".txt";
    userID = to_string(std::stoi(userID) - 1);
    std::string filePub = headdirectory + validatorAddress + keyType  + userID + fileExtension;
    ifstream file(filePub, ifstream::binary);
    file >> fileInfo;
    return fileInfo;
}
/*************************************************
* Name:        RunServer
*
* Description: Function that initialises the server side
*              of the validator.
*
* Arguments:   -std::string server_address: the local host
*               address of the validator
*              -std::string networkAddress: the ip address
*               of the validator
*              -Validator &service: the validator to which this
*               server belongs
*
* Returns server
**************************************************/
std::unique_ptr<Server> RunServer(std::string server_address, Validator &service);

int main(int argc, char** argv);

#endif