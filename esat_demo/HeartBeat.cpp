#include <unistd.h>
#include <iostream>
#include <memory>
#include <string>
#include <list>
#include <cmath>
#include "toml.hpp"
#include "HeartBeat.hpp"
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using consensus::Consensus;
using consensus::Ack;
using consensus::EpochInit;

/*************************************************
* Name:        initEpoch
*
* Description: Implements the functions that does the
*              the client side of the rpc command init
*              epoch.
*
* Arguments:   -std::shared_ptr<Consensus::Stub> validator:
*               the validator to whom the init epoch is sent
*               
*
* Returns 0 if everything works correctly and otherwise returns -1
**************************************************/
    int HeartBeatServer::initEpoch(std::shared_ptr<Consensus::Stub> validator) const {
        // Data we are sending to the Validator.
        EpochInit init;
        init.set_epoch_number(getEpoch());

        Ack Acknowledge;
        ClientContext context;

        // The actual  RPC.
        Status status = validator->InitEpoch(&context, init, &Acknowledge);
        
        // Act upon its status.
        if (status.ok()) {
          return 0;
        } else {
          std::cout << status.error_code() << ": " << status.error_message() << std::endl;
          return -1;
        }
    }

/*************************************************
* Name:        startEpoch
*
* Description: Implements the functions that does the
*              the client side of the rpc command start
*              epoch.
*
* Arguments:   -std::shared_ptr<Consensus::Stub> validator:
*               the validator to whom the start epoch is sent
*               
*
* Returns 0 if everything works correctly and otherwise returns -1
**************************************************/
    int HeartBeatServer::startEpoch(std::shared_ptr<Consensus::Stub> validator) const {
        // Data we are sending to Validator.
        EpochInit init;
        init.set_epoch_number(getEpoch());

        Ack Acknowledge;
        ClientContext context;

        // The actual  RPC.
        Status status = validator->StartEpoch(&context, init, &Acknowledge);
        
        // Act upon its status.
        if (status.ok()) {
            return 0;
        } else {
            std::cout << status.error_code() << ": " << status.error_message() << std::endl;
            return -1;
        }
    }

/*************************************************
* Name:        run
*
* Description: Implements the function that creates a
*              heartbeat server.
*
* Arguments:   -int numberOfEpochs: the amount of epochs 
*               the heartbeat server is supposed to run
*
* Returns nothing
**************************************************/
    void HeartBeatServer::run(int numberOfEpochs) {
        //number of nodes
        int numberOfValidators = getValidators().size();
        int threshold = numberOfValidators/3;
        int epochCount = 1;
        while (true) {
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;
            std::cout << "[HEARTBEAT]: Init epoch " << getEpoch() << "." << std::endl;
            int failedValidators = 0;
            int index = 0; 
            for (auto& client: getValidators()) {
                if (initEpoch(client) == 0) {
                    std::cout << "[HEARTBEAT]: Acknowledge by node address: " << getValidatorAddresses()[index] << "." << std::endl;
                } else {
                    std::cout << "[HEARTBEAT]: No acknowledge by node address: " << getValidatorAddresses()[index] << "." << std::endl;
                    failedValidators += 1;
                }
                if (failedValidators > threshold) {
                    std::cout << "Error: more than 1/3 of the validators are corrupted!" << std::endl;
                    return;
                }
                index = index + 1;
            }
            std::cout << "[HEARTBEAT]: Start epoch " << getEpoch() << "." << std::endl;
            for (auto& client: getValidators()) {
                startEpoch(client);
            }
            setEpoch(getEpoch() + 1);
            if (getEpoch()-1 == getMaxEpoch() || epochCount == numberOfEpochs) {
                return;
            }
            epochCount = epochCount + 1;
            
            sleep(2);
        }
    }

int main(int argc, char const* argv[]) {
// Taking the arguments from the command line
std::string server_address;
std::string max_of_epochs; // the maximum number of epochs the heartbeat server is allowed to run
std::string advance; // the number of epochs the heartbeat runs now (or until max_of_epochs is exceeded)
if (argc == 1) {
    server_address = argv[0];
    max_of_epochs = "0";
    advance = "0";
} else if (argc == 2) {
    server_address = argv[0];
    max_of_epochs = argv[1];
    assert (stoi(max_of_epochs) >= 0);
    advance = "0";
} else if (argc == 3) {
    server_address = argv[0];
    max_of_epochs = argv[1];
    assert (stoi(max_of_epochs) >= 0);
    advance = argv[2];
    assert (stoi(advance) >= 0);
} else {
    return 0;
}
// Stop the heartbeat server
if (advance == "0") {
    return 0;
}
// Create the heartbeat server
HeartBeatServer heartbeat_server(server_address, stoi(max_of_epochs));
// Run the heartbeat server "advance" number of epochs
heartbeat_server.run(stoi(advance));
// Advance the heartbeat server in steps
while (true) {
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "------------------------------------------------------------------------------------------" << std::endl;
    std::string advance = "";
    while (advance == "") {
        std::cout << "How many epochs should the heartbeat server procceed: ";
        getline(std::cin, advance);
    }
    assert (stoi(advance) >= 0);
    // Stop the heartbeat server
    if (advance == "0") {
        return 0;
    }
    // Run the heartbeat server "advance" number of epochs
    heartbeat_server.run(stoi(advance));
}
return 0;
}
