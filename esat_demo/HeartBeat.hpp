#ifndef HEARTBEAT_H
#define HEARTBEAT_H

#include <unistd.h>
#include <iostream>
#include <memory>
#include <string>
#include <list>
#include <vector>
#include "toml.hpp"
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

#ifdef BAZEL_BUILD
#include "consensus.grpc.pb.h"
#else
#include "consensus.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using consensus::Consensus;
using consensus::Ack;
using consensus::EpochInit;

class HeartBeatServer {
      private:
// Ip address of the heartbeat server.
            std::string address_;
// Ip addresses of the other validators.
            std::vector<std::string> validatorAddresses_;
// A list of all the channels used in grpc.
            std::list<std::shared_ptr<Consensus::Stub>> validatorChannels_;
// The epoch that the heartbeat server is currently in.
            int epoch_ = 1;
// The maximum epoch that the heartbeat server can be in.
            int maxEpoch_ = 0;

      public:

/*************************************************
* Name:        Constructor
*
* Description: Constructs the heartbeat server.
*
* Arguments:   -string address: the address we want
*               the heartbeat server to be at.
*              -int maxEpoch: maximum epoch we want
*               our heartbeat server to reach
*
* No Return value
**************************************************/
      HeartBeatServer(std::string address, int maxEpoch) 
        : address_(address)
        , maxEpoch_(maxEpoch)
      {
            // Put all validatorChannel addresses from the TOML file into the list "validators" and create a channel for each
            const toml::value data = toml::parse("setup.toml");
            const auto& table = toml::find(data, "nodes");
            for(const auto& v : table.as_array()) {
                  const auto validatorAddress = toml::find<std::string>(v, "ip_address");
                  validatorAddresses_.push_back(validatorAddress);
                  std::cout << "[HEARTBEAT]: Validator added with address: " << validatorAddress << "\n";
                  std::shared_ptr<Consensus::Stub> validatorChannel = Consensus::NewStub(grpc::CreateChannel(validatorAddress, grpc::InsecureChannelCredentials()));
                  validatorChannels_.push_back(validatorChannel);
            }
      }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old heartbeatserver
               in a new heartbeatserver.
*
* Arguments:   -HeartBeatServer const& heartbeatserver: the
*               heartbeat server we want to copy
*
* No Return value
**************************************************/
      HeartBeatServer(HeartBeatServer const& heartbeatserver)
          : address_(heartbeatserver.getAddress())
          , validatorAddresses_(heartbeatserver.getValidatorAddresses())
          , validatorChannels_(heartbeatserver.getValidators())
          , epoch_(getEpoch())
          , maxEpoch_(heartbeatserver.getMaxEpoch())
          {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the heartbeat server is equal
*              to a given heartbeat server and otherwise 
*              makes sure they are.
*
* Arguments:   -HeartBeatServer const& heartbeatserver: the
*               heartbeat server we want to match to
*
* Returns *this (success)
**************************************************/
      HeartBeatServer& operator=(HeartBeatServer const& heartbeatserver){
          if (address_ != heartbeatserver.getAddress() || validatorAddresses_ != getValidatorAddresses() || validatorChannels_ != getValidators() || epoch_ != getEpoch() || maxEpoch_ != getMaxEpoch()){
              address_ = heartbeatserver.getAddress();
              validatorAddresses_ = heartbeatserver.getValidatorAddresses();
              validatorChannels_ = getValidators();
              epoch_ = getEpoch();
              maxEpoch_ = getMaxEpoch();
          }
          return *this;
      }

/*************************************************
* Name:        initEpoch
*
* Description: Implements the functions that does the
*              the client side of the rpc command init
*              epoch.
*
* Arguments:   -std::shared_ptr<Consensus::Stub> validatorChannel:
*               the validatorChannel to whom the init epoch is sent
*               
*
* Returns 0 if everything works correctly and otherwise returns -1
**************************************************/
      int initEpoch(std::shared_ptr<Consensus::Stub> validatorChannel) const;

/*************************************************
* Name:        startEpoch
*
* Description: Implements the functions that does the
*              the client side of the rpc command start
*              epoch.
*
* Arguments:   -std::shared_ptr<Consensus::Stub> validatorChannel:
*               the validatorChannel to whom the start epoch is sent
*               
*
* Returns 0 if everything works correctly and otherwise returns -1
**************************************************/
      int startEpoch(std::shared_ptr<Consensus::Stub> validatorChannel) const;

/*************************************************
* Name:        run
*
* Description: Implements the function that creates a
*              heartbeat server.
*
* Arguments:   -int numberOfEpochs: the amount of epochs 
*               the heartbeat server is supposed to run
*
* Returns nothing
**************************************************/
      void run(int numberOfEpochs);

public:

/*************************************************
* Name:        getAddress
*
* Description: Function to get the address of the 
*              heartbeat server.
*
* Arguments:   None
*               
*
* Returns address_
**************************************************/
      inline std::string getAddress() const {return address_;}

/*************************************************
* Name:        getValidatorAddresses
*
* Description: Function to get the addresses of the
*              validators.
*
* Arguments:   None
*               
*
* Returns validatorAddresses_
**************************************************/
      inline std::vector<std::string> getValidatorAddresses() const {return validatorAddresses_;}

/*************************************************
* Name:        getValidatorChannels
*
* Description: Function to get the validatorChannel channels.
*
* Arguments:   None
*
* Returns validatorChannels_
**************************************************/
      inline std::list<std::shared_ptr<Consensus::Stub>> getValidators() const {return validatorChannels_;}

/*************************************************
* Name:        getEpoch
*
* Description: Function to get the epoch the heartbeat
*              server is currently in.
*
* Arguments:   None
*
* Returns epoch_
**************************************************/
      inline int getEpoch() const {return epoch_;}

/*************************************************
* Name:        setEpoch
*
* Description: Function to set the epoch the heartbeat
*              server is currently in.
*
* Arguments:   -int epoch = the epoch we want the heartbeat
*               server to be in
*
* Returns epoch_
**************************************************/
      inline void setEpoch(int epoch) {epoch_ = epoch;}

/*************************************************
* Name:        getMaxEpoch
*
* Description: Function to get the maximum epoch the 
*              heartbeat server can be in.
*
* Arguments:   None
*
* Returns maxEpoch_
**************************************************/
      inline int getMaxEpoch() const {return maxEpoch_;}
};

int main(int argc, char const *argv[]);

#endif