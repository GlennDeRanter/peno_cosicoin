#ifndef CONVERT_H
#define CONVERT_H
#include <cstdint>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstddef>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cstdint>

/*************************************************
* Name:        hexToASCII
*
* Description: Function that converts a hexadecimal string
*              to an ascii string.
*
* Arguments:   std::string hex: the hexadecimal string we
*              wish to convert.
*
* Returns ascii
**************************************************/
        inline std::string hexToASCII(std::string hex) {
                // initialize the ASCII code string as empty.
                std::string ascii = "";
                for (size_t i = 0; i < hex.length(); i += 2) {
                // extract two characters from hex string
                        std::string part = hex.substr(i, 2);
                // change it into base 16 and 
                // typecast as the character
                        char ch = stoul(part, nullptr, 16);
                // add this char to final ASCII string
                        ascii += ch;
                }
                return ascii;

        }

/*************************************************
* Name:        ASCIIToHex
*
* Description: Function that converts an ASCII string
*              to a hexadecimal string.
*
* Arguments:   std::string ascii: the ascii string we
*              wish to convert.
*
* Returns hex
**************************************************/
inline std::string ASCIIToHex(std::string ascii){
        std::stringstream ss;
        ss << std::hex << std::setfill('0');
        for (size_t i = 0; ascii.length() > i; ++i) {
                ss << std::setw(2) << static_cast<unsigned int>(static_cast<unsigned char>(ascii[i]));
        }
        std::string hex = ss.str();
        return hex;
        }

/*************************************************
* Name:        ASCIIToDec
*
* Description: Function that converts an ASCII string
*              to a decimal string.
*
* Arguments:   std::string ascii: the ascii string we
*              wish to convert.
*
* Returns dec
**************************************************/
        inline std::string ASCIIToDec(std::string ascii){
        std::stringstream ss;
        ss << std::dec << std::setfill('0');
        for (size_t i = 0; ascii.length() > i; ++i) {
                ss << std::setw(1) << static_cast<unsigned int>(static_cast<unsigned char>(ascii[i]));
        }
        std::string dec = ss.str();
        return dec;
        }

#endif