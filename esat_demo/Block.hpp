#ifndef BLOCK_H
#define BLOCK_H

#include <cryptopp/sha3.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <iostream>
#include <string>
#include <vector>
#include "Header.hpp"
#include "Transaction.hpp"
#include "KeyPair.hpp"
#include "convert.hpp"
#include "toml.hpp"


using namespace std;

class Block {
    private:

// The epoch number of the block
        int epoch_; 
// Hash of the previous block in the chain in which we intend to add this block.
        string hashedPrefix_;
// A sommation of all the transactions in this block.
        std::vector<Transaction> payload_ = {};
// A boolean that defines whether or not 2/3 of all the validators have signed this block.
        bool notarized_ = false;
// The number of signatures that the block got.
        int nbOfSignatures_ = 0;
// A boolean that defines whether or not a block is final.
        bool finalized_ = false;
// The header of the block.
        Header header_;
// The maximum size of a block in bytes.
        int maxSize_ = 1990000;

    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the block and takes no arguments.
*
* Arguments:   None
*
* No Return value
**************************************************/
        Block()
        {
            epoch_ = 0;
            hashedPrefix_ = "";
            notarized_ = false;
            payload_ = {};
            nbOfSignatures_ = 0;
            finalized_ = false;
            std::vector<std::string> valSigs = {};
            std::vector<std::vector<unsigned char >> txids = {};
            header_ = Header(epoch_, valSigs, "", "", txids);
        }

/*************************************************
* Name:        Constructor
*
* Description: Constructs the Block.
*
* Arguments:   -const string hashedPrefix: hash of the previous 
-               block in the chain in which we intend to add this block 
*              -const int epoch: the epoch number in which 
*               the block was created
*              -const std::vector<Transaction>& payload: a sommation of 
*               all the transactions in this block
*              -const std::vector<std::pair<string,unsigned int>>& UTXO :
*               a vector with the balances of each of the wallets.
*
* No Return value
**************************************************/
        Block(const string hashedPrefix, const int epoch, std::vector<Transaction> payload)
            : epoch_(epoch)
            , hashedPrefix_(hashedPrefix)
            , notarized_(false)
            , nbOfSignatures_(0)
            , finalized_(false)
            {   
                int currentBlockSize = sizeof(epoch_) + 2*sizeof(hashedPrefix_) + sizeof(notarized_) + sizeof(nbOfSignatures_) + sizeof(finalized_);
                for (int i=0; i < payload.size(); ++i){
                    if (currentBlockSize + (payload[i]).getSize()> maxSize_){
                        throw "Block size limit exceeded!";
                    }
                    else{
                        payload_.push_back(payload[i]);
                        currentBlockSize = currentBlockSize + (payload[i]).getSize();
                    }
                }
                std::vector<std::vector<unsigned char>> txids = {};
                int payloadSize2 = payload_.size();
                for (int i=0; i<payloadSize2;++i){
                    txids.push_back((payload_[i]).getId());
                }
                std::vector<std::string> valSigs = {};
                std::string MerkelRootHash = CreateMerkelRoot();
                header_ = Header(epoch, valSigs, hashedPrefix, MerkelRootHash, txids);
            }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old block
               in a new block.
*
* Arguments:   -const Block& block: the block
*               we want to copy
*
* No Return value
**************************************************/
        Block(Block const& block)
            : epoch_(block.epoch_)
            , hashedPrefix_(block.hashedPrefix_)
            , notarized_(block.notarized_)
            , payload_(block.payload_)
            , nbOfSignatures_(block.nbOfSignatures_)
            , finalized_(block.finalized_)
            , header_(block.header_)
            {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the block is equal
*              to a given block and otherwise 
*              makes sure they are.
*
* Arguments:   -const Block& block: the block
*               we want to match to
*
* Returns *this (success)
**************************************************/
        inline Block& operator=(Block const& block){
            if (!(*this == block)){
                epoch_ = block.epoch_;
                hashedPrefix_ = block.hashedPrefix_;
                notarized_ = block.notarized_;
                payload_ = block.payload_;
                finalized_ = block.finalized_;
                header_ = block.header_;
                nbOfSignatures_ = block.nbOfSignatures_;
            }
            return *this;
        }

/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the block matches a 
*              given block.
*
* Arguments:   Input const& block: blockobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
    inline bool operator==(Block const& block){
        if (payload_.size() != block.payload_.size()){
            return false;
        }
        else {
            for (int i=0; i<payload_.size(); ++i){
                if (!(payload_[i] == block.payload_[i])){
                    return false;
                }
            }
            if ((epoch_ != block.epoch_) || (hashedPrefix_ != block.hashedPrefix_) || (notarized_ != block.notarized_) || (nbOfSignatures_ != block.nbOfSignatures_) || (finalized_ != block.finalized_) || !(header_ == block.header_) ){
                return false;
            }
            else {
                return true;
            }
        }
    }

/*************************************************
* Name:        getEpoch
*
* Description: Get the epoch number in which the block
*              was created.
*
* Arguments:   None
*
* Returns epoch_
**************************************************/
        inline int getEpoch() const {return epoch_;}

/*************************************************
* Name:        getHashedPrefix
*
* Description: Get the hash of the previous block 
*              in the chain in which we intend to add this block.
*
* Arguments:   None
*
* Returns hashedPrefix_
**************************************************/
        inline string getHashedPrefix() const {return hashedPrefix_;}

/*************************************************
* Name:        getPayload
*
* Description: Get the transactions belonging to this block.
*
* Arguments:   None
*
* Returns payload_
**************************************************/
        inline std::vector<Transaction> getPayload() const {return payload_;} 
        
/*************************************************
* Name:        isNotarized
*
* Description: Function that checks whether a block
*              has been notarized.
*
* Arguments:   None
*
* Returns notarized_
**************************************************/
        inline bool isNotarized() const {return notarized_;}

/*************************************************
* Name:        getNbSignatures
*
* Description: Get the number of signatures on the block.
*
* Arguments:   None
*
* Returns nbOfSignatures_
**************************************************/
        inline int getNbSignatures() const {return nbOfSignatures_;}

/*************************************************
* Name:        isFinalized
*
* Description: Function that checks whether a block
*              has been finalized.
*
* Arguments:   None
*
* Returns hashedPrefix_
**************************************************/
        inline bool isFinalized() const {return finalized_;}
 
/*************************************************
* Name:        getHeader
*
* Description: Get the header of the block.
*
* Arguments:   None
*
* Returns header_
**************************************************/
        inline Header getHeader() const {return header_;}


/*************************************************
* Name:        notarize
*
* Description: Function to notarize a block.
*
* Arguments:   -const int totalValidators: gives the
*               total amount of validators
*
* Returns nothing
**************************************************/
        inline void notarize(const int totalValidators){
            if (3*nbOfSignatures_ >= (2 * totalValidators)){
                notarized_ = true;
            }
        };

/*************************************************
* Name:        addSignatureToBlock
*
* Description: Function to add a signature to a block.
*
* Arguments:   -std::string publicKey: the public key
*               of the person who signed the block
*              -string signature: the signature
*               with which we want to sign the block
*              -const int totalValidators: the total
*               number of validators in the network
*
* Returns nothing
**************************************************/
        inline void addSignatureToBlock(std::string publicKey, std::string signature, const int totalValidators){
            std::string hashedBlock = hashBlock();
            if (verifySignature(signature, hashedBlock, publicKey) == true){
                bool signatureAlreadyAdded = false;
                for(auto& sig : header_.getValidSigs()){
                    if (sig == signature){
                        signatureAlreadyAdded = true;
                    }
                }
                if(!signatureAlreadyAdded){
                    nbOfSignatures_ = nbOfSignatures_ + 1;
                    notarize(totalValidators);
                    header_.addSignature(signature);
                }
                
            }
        };

/*************************************************
* Name:        finalize
*
* Description: Function to finalize a block.
*
* Arguments:   None
*
* Returns nothing
**************************************************/
        inline void finalize(){
            finalized_ = true;
        }

/*************************************************
* Name:        createMerkelRoot
*
* Description: Function to create the Merkel root tree
*              of transactions of the block.
*
* Arguments:   None
*
* Returns hashedTransactions[0]
**************************************************/
        inline std::string CreateMerkelRoot(){
            std::vector<string> hashedTransactions = {};
            int payloadSize = payload_.size();
            for (int i=0; i< payloadSize; ++i){
                hashedTransactions.push_back((payload_[i]).hashTransaction());
            }
            while (hashedTransactions.size() > 1){
                std::vector<string> hashedTransactions2 = hashedTransactions;
                hashedTransactions.clear();
                int hashSize = hashedTransactions2.size();
                for(int i=0;i<hashSize-1; ++i){
                    std::string together = hashedTransactions2[i] + hashedTransactions2[i+1];
                    SHA3_256 hash;
                    string merkelHash;
                    hash.Update((const CryptoPP::byte*)together.data(),together.size());
                    merkelHash.resize(hash.DigestSize());
                    hash.Final((CryptoPP::byte*) &merkelHash[0]);
                    hashedTransactions.push_back(merkelHash);
                    i = i+1;
                }
            }
            if (hashedTransactions.size() ==1){
                std::string merkelRoot = ASCIIToHex(hashedTransactions[0]);
                return merkelRoot;
            }
            return "00000";
        }

/*************************************************
* Name:        transactionInBlock
*
* Description: Function to check whether a certain
*              transaction is in this block.
*
* Arguments:   -const Transaction& transaction: transaction
*               we wish to find
*
* Returns true if the transaction is in the block, else returns false
**************************************************/
    inline bool transactionInBlock(const Transaction& transaction){
        std::vector<Transaction> transactions = getPayload();
        int transactionSize = transactions.size();
        for (int i=0; i < transactionSize; i++){
            if ((transactions[i]).getId() == transaction.getId()){
                return true;
            }
        }
        return false;
    }

/*************************************************
* Name:        hashBlock
*
* Description: Function to hash the arguments of the block.
*
* Arguments:   None
*
* Returns hashedblock
**************************************************/
    inline string hashBlock(){
        string blockString = "";
        blockString = blockString + to_string(epoch_);
        blockString = blockString + hashedPrefix_;
        for (int i= 0; i < payload_.size(); ++i){
            blockString = blockString + (payload_[i]).hashTransaction();
        }
        SHA3_256 hash;
        string hashedBlock;
        hash.Update((const CryptoPP::byte*)blockString.data(),blockString.size());
        hashedBlock.resize(hash.DigestSize());
        hash.Final((CryptoPP::byte*) &hashedBlock[0]);
        std::string hashedblock = ASCIIToHex(hashedBlock);
        return hashedblock;
    }

/*************************************************
* Name:        getSize
*
* Description: Estimates the size of a block.
*
* Arguments:   None
*
* Returns size
**************************************************/
    inline int getSize(){
        int size =0;
        size = size + sizeof(epoch_);
        size = size + sizeof(hashedPrefix_);
        size = size + sizeof(notarized_);
        size = size + sizeof(nbOfSignatures_);
        size = size + sizeof(maxSize_);
        size = size + header_.getSize();
        size  = size + sizeof(std::vector<Transaction>);
        for (int i=0; i < payload_.size(); ++i){
            size = size + (payload_[i]).getSize();
        }
        return size;
    }

};

/*************************************************
* Name:        createGenesisBlock
*
* Description: Function that creates the default 
*              genesis block for our blockchain.
*
* Arguments:   -std::vector<std::pair<std::string,std::string>> defaultWallets:
*               the id and public key of the default wallets
*
* Returns genesisBlock
**************************************************/
inline Block createGenesisBlock(std::vector<std::pair<std::string,std::string>> defaultWallets){
                std::vector<unsigned char> transactionId({'f', 'i', 'r', 's', 't'});
                // We know this is not safe since everyone can sign the genesis block he wants
                // but it is for flexibility and debug reasons, in real we would opt that only the developers have money without signing the 
                const toml::value data2 = toml::parse("developers.toml");
                const auto& table2 = toml::find(data2, "developers");
                const auto key1 = toml::find<std::string>(table2, "public_key");
                std::string developersPublicKey = key1;
                const auto key2 = toml::find<std::string>(table2, "private_key");
                std::string developersPrivateKey = key2;
                // All inputs
                std::vector<Input> inputs;
                Input inputFromDevelopers(std::vector<unsigned char>({'x'}), 0);
                inputs.push_back(inputFromDevelopers);
                // All outputs
                std::vector<Output> outputs;
                // Milan
                Output outputToMilan(1000, defaultWallets[0].second);
                outputs.push_back(outputToMilan);
                // Linde
                Output outputToLinde(1000, defaultWallets[1].second);
                outputs.push_back(outputToLinde);
                // Glenn
                Output outputToGlenn(1000, defaultWallets[2].second);
                outputs.push_back(outputToGlenn);
                // Developers
                Output outputToDevelopers(97000, developersPublicKey);
                outputs.push_back(outputToDevelopers);
                // Making the transaction
                Transaction transactionFromDevelopers(inputs, outputs, "", transactionId);
                /// Signing the transaction with the developers' wallet
                KeyPair devKeyPair(developersPublicKey, developersPrivateKey, "admin");
                std::string message = transactionFromDevelopers.hashTransaction();
                std::string signedMessage = devKeyPair.createSignature(message, "admin");
                // Setting the signature on the transaction
                transactionFromDevelopers.setSenderSig(signedMessage);
                Block genesisBlock("",0, {transactionFromDevelopers});
                genesisBlock.notarize(0);
                return genesisBlock;
}

#endif