#ifndef IO_H
#define IO_H

#include <string>
#include <vector>

extern "C"{
     #include "Signatures/api.h"
    }

class Input{
    private:

// The ID tag of the transaction from which this input comes.
        std::vector<unsigned char> txID_;
// The index of the output in the transaction from which this input comes.
        int outputIndex_;

    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the input.
*
* Arguments:   None
*
* No Return value
**************************************************/
        Input()
        :txID_({})
        , outputIndex_(0)
        {}

/*************************************************
* Name:        Constructor
*
* Description: Constructs the input.
*
* Arguments:   -const std::vector<unsigned char>& txID: ID tag of the transaction 
*               from which this input comes
*              -const unsigned int outputIndex: index of the output in the transaction
*               from which this input comes
*
* No Return value
**************************************************/
        Input(const std::vector<unsigned char>& txID, const int outputIndex)
        : txID_(txID)
        , outputIndex_(outputIndex)
        {
            if (outputIndex < 0){
                outputIndex_ = 0;
            }
            else {
                outputIndex_ = outputIndex;
            }
        }

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old input
               in a new input.
*
* Arguments:   -const Input& input: the input
*               we want to copy
*
* No Return value
**************************************************/
        Input(Input const& input)
        : txID_(input.txID_)
        , outputIndex_(input.outputIndex_)
        {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the input is equal
*              to a given input and otherwise 
*              makes sure they are.
*
* Arguments:   -const Input& input: the input
*               we want to match to
*
* Returns *this (success)
**************************************************/
    Input& operator=(Input const& input){
        if (!(*this == input) ){
                txID_ = input.txID_;
                outputIndex_ = input.outputIndex_;
            }
        return *this;
    }
    
/*************************************************
* Name:        Equality operator
*
* Description: Checks whether the input matches a 
*              given input.
*
* Arguments:   Input const& objInput1: inputobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
    bool operator==(Input const& input){
        if (txID_.size() == input.txID_.size()){
            bool identical = true; 
            for (int i=0; i < txID_.size(); ++i){
                if (txID_[i] != input.txID_[i]){
                    identical = false;
                    break;
                }
            }
            if (identical == true && outputIndex_ == input.outputIndex_){
                return true;
            }
            else {
                return false;
            }   
        }
        else {
            return false;
        }
    }
    
/*************************************************
* Name:        getTxId
*
* Description: Get the id of the previous transaction
*              from which this input comes.
*
* Arguments:   None
*
* Returns txId_
**************************************************/
    inline std::vector<unsigned char> getTxId() const {return txID_;}

/*************************************************
* Name:        getOutputIndex
*
* Description: Get the index of the output of the 
*              previous transaction from which this input comes.
*
* Arguments:   None
*
* Returns outputIndex_
**************************************************/
    inline unsigned int getOutputIndex() const {return outputIndex_;}

/*************************************************
* Name:        inputToString
*
* Description: Generates a string of the input in order
*              to be able to hash it.
*
* Arguments:   None
*
* Returns inputString
**************************************************/
    inline std::string inputToString() {
        std::string inputString =  std::to_string(outputIndex_);
        int txSize = txID_.size();
        for (int i=0;i < txSize;++i){
            inputString.push_back(txID_[i]);
        }
        return inputString;
    }

/*************************************************
* Name:        IDToString
*
* Description: Generates a string of the txID_.
*
* Arguments:   None
*
* Returns idString
**************************************************/
    inline std::string IDToString(){
        std::string idString = "";
        int txSize = txID_.size();
        for (int i=0; i< txSize;++i){
            idString.push_back(txID_[i]);
        }
        return idString;
    }

/*************************************************
* Name:        getSize
*
* Description: Estimates the size of the input.
*
* Arguments:   None
*
* Returns size
**************************************************/
    inline int getSize(){
        int size = 0;
        size = size + sizeof(outputIndex_);
        size = size + sizeof(std::vector<unsigned char>) + (sizeof(unsigned char) * txID_.size());
        return size;
    }

};

class Output{
    private:
// The value that will be transferred.
        unsigned int value_;
// The public key of the person that will receive the output.
        std::string receiverPk_;
        
    public:

/*************************************************
* Name:        Default constructor
*
* Description: Constructs the input.
*
* Arguments:   None
*
* No Return value
**************************************************/
        Output()
        : value_(0)
        , receiverPk_("")
        {}

/*************************************************
* Name:        Constructor
*
* Description: Constructs the output.
*
* Arguments:   -const unsigned int value: The value of the output
*              -const std::string receiverPk: The public key of the person
*               that will receive this output
*
* No Return value
**************************************************/
        Output(const unsigned int value, std::string receiverPk)
        :value_(value)
        ,receiverPk_(receiverPk)
        {}

/*************************************************
* Name:        Copy constructor
*
* Description: Copies the values of an old output
               in a new output.
*
* Arguments:   -const Output& output: the output
*               we want to copy
*
* No Return value
**************************************************/
        Output(Output const& output)
        :value_(output.value_)
        ,receiverPk_(output.receiverPk_)
        {}

/*************************************************
* Name:        Assignment operator
*
* Description: Checks whether the output is equal
*              to a given output and otherwise 
*              makes sure they are.
*
* Arguments:   -const Output& output: the output
*               we want to match to
*
* Returns *this (success)
**************************************************/
        Output& operator=(Output const& output)
        {
            if (value_ != output.value_ || receiverPk_ != output.receiverPk_){
                value_ = output.value_;
                receiverPk_ = output.receiverPk_;
            }
            return *this;
        }

/*************************************************
* Name:        operator==
*
* Description: Checks whether the output matches a 
*              given output.
*
* Arguments:   Output const& objOuput: outputobject
*              to verify the equality with
*
* Returns true if equal, else false
**************************************************/
        bool operator==(Output const& output){
            if (value_ == output.value_ && receiverPk_ == output.receiverPk_){
                return true;
            }
            else{
                return false;
            }
        }

/*************************************************
* Name:        getValue
*
* Description: Get the value of the output.
*
* Arguments:   None
*
* Returns value_
**************************************************/
        inline unsigned int getValue() const {return value_;}

/*************************************************
* Name:        getReceiverPk
*
* Description: Get the public key of the person 
*              to whom the coins will be payed.
*
* Arguments:   None
*
* Returns receiverPk_
**************************************************/
        inline std::string getReceiverPk() const {return receiverPk_;}

/*************************************************
* Name:        outputToString
*
* Description: Generates a string of the output in order
*              to be able to hash it.
*
* Arguments:   None
*
* Returns outputString
**************************************************/ 
        inline std::string outputToString() {
            std::string outputString = std::to_string(value_) + receiverPk_;
            return outputString;
        }

/*************************************************
* Name:        getSize
*
* Description: Estimates the size of the output.
*
* Arguments:   None
*
* Returns size
**************************************************/
        inline int getSize(){
            int size = 0;
            size = size + sizeof(value_);
            size = size + receiverPk_.length() *8;
            return size;
        }

};
#endif