1. **TESTING ON LOCAL MACHINES**

- **For the validators and wallets:**

In the consensus directory you can find a working consensus protocol. You can test this by going to consensus_exercise from the location where you find this readme file.

Before you can run the Cmake you will need to install CRYPTOPP. You can do this using the instructions from the
following link: https://github.com/weidai11/cryptopp/blob/master/Install.txt
After you have done this, you can use the cmake file CMakeLists.txt. Before you use this however you will need to change
line 17: find_path(CRYPTOPP_INCLUDE_DIR NAMES cryptopp-CRYPTOPP_8_5_0/cryptlib.h HINTS "/home/linde/Downloads")
as follows:
    - cryptopp-CRYPTOPP_8_5_0/ to the name of the cryptopp library that you just installed
    - home/linde/Downloads     to the path where the cryptopp library was installed
After that you need to comment lines 89 till the end.
For running it you then run in this order the commands.
cmake ../consensus_exercise
make


- **validator:**

-after you have done this, change the name of setup4.toml to setup.toml. then you open 5 terminal in this directory and run on 4 terminals the command
./Validator 0.0.0.0:5005* 

You should replace the star in the command in the four terminals with a different number ranging from 0 to 3.
In the fifth terminal you enter 
./heartbeatserver A B 
where A is the maximum numbers of epochs you want to run and B is the number of of epoch you want to run at one time. After B epochs the server will ask you again how many epochs you want to run and this repeats itself until you reach A epochs.
This command starts the heartbeatserver and starts the protocol so run this after you started the 4 validators.

If you want to add or remove a number of validators or change the listening ports you can do this by changing the setup.toml file.



- **wallet:**

there are three types of wallets:
		1) the default wallets (Milan, Linde and Glenn) who are able to send only to each other
		2) the generic wallets who run a full interface and can send and receive to any other wallet (as long as the public keys of receiver wallets are known by the user controlling the wallet, since it should be pasted in the file (see below))
		3) the "developer wallet" which is property from the developers and have all the coins in the system, it is a non-default wallet having all coins apart from the default wallets (see remark)

Remark: - by definition of the genesis block in the class "BlockChain" 1000 coins are transferred to each of the default wallets
	- as a result the "developer wallet" has 100 000 - 3000 = 97 000 coins
				
Running a wallet: 1) type in the terminal ./wallet <wallet_name>
                  2) if <wallet_name> is Milan, Linde or Glenn (case-sensitive!) the corresponding default wallet is generated (with 1000 coins)
                     if <wallet_name> is dev, the "developer" wallet is generated (with 97 000 coins)
		     if <wallet_name> is an integer greather than 3, a generic wallet is generated (without any coins) --> transfer coins from developers' wallet (dev) to the generic wallet by pasting the public key (find in your new wallet by doing 4) option (3)) of the receiving wallet in the interface of the developer wallet (see -4))
		  3) an interface will run asking for your new password (don't forget it, it will be needed to make a transaction!)
                  4) if you run a default wallet, you can send a transaction to any other default wallet (1), synchronize the balance from a validator (2), ask for the configuration (3) or quit the wallet (4) by typing the number in parenthesis
                      - 4) if you don't run a default wallet, you can do the same things as with default wallets, only it is possible to send to other wallets
				--> ----KNOWN RECEIVERS---- contain all receiver public keys you previously have send to (so in the beginning it will be empty, but try to add a public key as explained in the line below and see if it is added to ---KNOWN RECEIVERS--- afterwards)
                                --> ----NEW RECEIVERS---- here it is possible to add new receivers by pasting their public key in the file 'publicKey.txt' (type "y"(es) if the file contains the key) which will ask for the amount to transfer if the public key in the file is a valid one
                                --> ----TRANSACTION ID---- asks for a transaction id (only positive integers are allowed and checked!), when blank a random number will be generated
                                --> ----COMMIT---- to commit type "y"(es), to wait type "n"(o), to abort the transaction type "a"(bort) 

Remark: since the terminal is not able to receive more than 4095 characters, it is obted to read the public key of the receiver from the file 'publicKey.txt'.



- **For the testsuite:**

Before you can run the testsuite you will need to install CRYPTOPP. You can do this using the instructions from the
following link: https://github.com/weidai11/cryptopp/blob/master/Install.txt
After you have done this, you can use the cmake file CMakeLists.txt. Before you use this however you will need to change
line 91: find_path(CRYPTOPP_INCLUDE_DIR NAMES cryptopp-CRYPTOPP_8_5_0/cryptlib.h HINTS "/home/linde/Downloads")
as follows:
    - cryptopp-CRYPTOPP_8_5_0/ to the name of the cryptopp library that you just installed
    - home/linde/Downloads     to the path where the cryptopp library was installed
After that you need to comment lines 6-90, uncomment the others and run: cmake ../consensus_exercise.
Now you can compile the project by running: make
Finally you can run the unit test using the command: ./tests



2. **DEMO ON ESAT MACHINES**

- **For the validators and wallets:**

If you want to run the complete cosicoin on the ESAT machines, we have prepared a ready to launch demo. You can find this demo in the directory named esat_demo which you find in the same directory as this README. 

Before you can run the Cmake you will need to install CRYPTOPP. You can do this using the instructions from the
following link: https://github.com/weidai11/cryptopp/blob/master/Install.txt
One important remark is that you don't use the command:
sudo make install 
but 
make install PREFIX=users/*. 
replace the star with the location where you're home directory is located. For a student is this students/"r-number".

After you have done this, you can use the cmake file CMakeLists.txt. Before you use this however you will need to change
line 17: find_path(CRYPTOPP_INCLUDE_DIR NAMES cryptopp-CRYPTOPP_8_5_0/cryptlib.h HINTS "/home/linde/Downloads")
as follows:
    - cryptopp-CRYPTOPP_8_5_0/ to the name of the cryptopp library that you just installed
    - home/linde/Downloads     to the path where the cryptopp library was installed this will be the same path which you wrote after PREFIX 
After that you need to comment lines 89 till the end.
For running it you then run in this order the commands.
cmake ../esat_demo
make


- **validators:**

-after you have done this, open 5 terminals in this directory. 
Because we're running the validator on four different machines so you have to ssh to four different machines. The setup.toml file is prepared to run on machines with ip-address, pc-klas1-{13-16}. On these machines you go again to the directory esat_demo and run the command
./Validator *:9999 

You should replace the star in the command in the four terminals with the ip-address of your machine that the terminal is connected to.
In the fifth terminal you enter 
./heartbeatserver A B 
where A is the maximum numbers of epochs you want to run and B is the number of of epoch you want to run at one time. After B epochs the server will ask you again how many epochs you want to run and this repeats itself until you reach A epochs.
This command starts the heartbeatserver and starts the protocol so run this after you started the 4 validators.

If you want to add or remove a number of validators or change the listening ports you can do this by changing the setup.toml file.


- **wallet:**

there are three types of wallets:
		1) the default wallets (Milan, Linde and Glenn) who are able to send only to each other
		2) the generic wallets who run a full interface and can send and receive to any other wallet (as long as the public keys of receiver wallets are known by the user controlling the wallet, since it should be pasted in the file (see below))
		3) the "developer wallet" which is property from the developers and have all the coins in the system, it is a non-default wallet having all coins apart from the default wallets (see remark)

Remark: - by definition of the genesis block in the class "BlockChain" 1000 coins are transferred to each of the default wallets
	- as a result the "developer wallet" has 100 000 - 3000 = 97 000 coins
				
Running a wallet: 1) type in the terminal ./wallet <wallet_name>
                  2) if <wallet_name> is Milan, Linde or Glenn (case-sensitive!) the corresponding default wallet is generated (with 1000 coins)
                     if <wallet_name> is dev, the "developer" wallet is generated (with 97 000 coins)
		     if <wallet_name> is an integer greather than 3, a generic wallet is generated (without any coins) --> transfer coins from developers' wallet (dev) to the generic wallet by pasting the public key (find in your new wallet by doing 4) option (3)) of the receiving wallet in the interface of the developer wallet (see -4))
		  3) an interface will run asking for your new password (don't forget it, it will be needed to make a transaction!)
                  4) if you run a default wallet, you can send a transaction to any other default wallet (1), synchronize the balance from a validator (2), ask for the configuration (3) or quit the wallet (4) by typing the number in parenthesis
                      - 4) if you don't run a default wallet, you can do the same things as with default wallets, only it is possible to send to other wallets
				--> ----KNOWN RECEIVERS---- contain all receiver public keys you previously have send to (so in the beginning it will be empty, but try to add a public key as explained in the line below and see if it is added to ---KNOWN RECEIVERS--- afterwards)
                                --> ----NEW RECEIVERS---- here it is possible to add new receivers by pasting their public key in the file 'publicKey.txt' (type "y"(es) if the file contains the key) which will ask for the amount to transfer if the public key in the file is a valid one
                                --> ----TRANSACTION ID---- asks for a transaction id (only positive integers are allowed and checked!), when blank a random number will be generated
                                --> ----COMMIT---- to commit type "y"(es), to wait type "n"(o), to abort the transaction type "a"(bort) 

Remark: since the terminal is not able to receive more than 4095 characters, it is obted to read the public key of the receiver from the file 'publicKey.txt'.



3. **USED LIBRARIES**

This section enumerates all used libraries in the CosiCoin project and discusses why a particular library is used, as opposed to others,
or coding it ourselves.

- **Cryptopp:**

The reason why the Cryptopp library was included in the beginning of the project is the fact that it was recommended by the assistents and it fitted the need 
for having cryptographic primitives in the project with saving valuable time for other parts in the project. 
However, the decision for using this library was taken before the post quantum signature scheme was chosen. 
This can be important because, in later stages of
the project, this library can be replaced by the built-in hash function in the CRYSTALS-Dilithium library.
However, since we can use the cryptopp library free of charge, it does not matter a lot that it is still
included in the project. Altough, migrating to the CRYSTALS-Dilithium library makes it possible to (partly) hardware accelerate the SHA3-256 and reuse code. 


- **CRYSTALS-Dilithium:**

CRYSTALS-Dilithium is one of the tree in the running for become standardized as a post-quantum signature scheme. As a result, not many libraries for this signature scheme are availlabe which means
that the used library was the only candidate. As this is also an ellaborate library, it was not possible to code it from scratch.
Here, only a brief introduction for selection of Dilithium5-AES will be given, for more ellaboration see the report. First, the the generation speed of keypairs of Dilithium5-AES is only beaten by other Dilithium implementations and by the PICNIC signature scheme which is significantly faster.
The other implementations have a speed that is significantly lower than the speed of Dilithium5-AES. Second, for the signing speed the same figure holds. However, outside the Dilithium class of signature schemes no one performes in this case better at signing speed.
Third, as verifications are done much by the validators, the Dilithium5-AES implementation is picked since it is faster than every 
other scheme, although FALCON comes close. Lastly, Dilithium5-AES has the highest security level regarding other Dilithium variants and its memory requirements are acceptable.
We copied the whole Signatures directory right from their git repository.


- **Toml:**

As in case of the Cryptopp library toml extension was chosen by recommendation of the assistents. After reading the documentation it was 
clear that this library was easy to useand seamlessly integrable with the rest of the code. 
The library doesn't provide that much of functionality, but all required options were available (e.g. only reading to C++ is provided).
Futhermore, the the sofware follows the MIT license which doesn't impose any restriction on the use of toml, except for including a license and copyright notice. 
The files that we copied from the github is the directory toml and also the toml.hpp file.

