\babel@toc {english}{}
\beamer@sectionintoc {1}{Theme options}{3}{0}{1}
\beamer@sectionintoc {2}{Frames and text}{7}{0}{2}
\beamer@sectionintoc {3}{Itemization and enumeration}{21}{0}{3}
\beamer@sectionintoc {4}{Blocks and other environments}{35}{0}{4}
